:orphan:

.. warning:: **SPOILERS!**

   **If you are a player** planning to play the tutorial module or any related
   content, then you should avoid this section!

   The information here contains some back-story and details that *will* give
   away plot-lines, motives and secrets.

   Only continue reading this section *if you are a GM* planning to run the
   tutorial for a group of players.
