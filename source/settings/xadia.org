#+title: Adventures in Xadia

* Adventures in Xadia
These are some rules that you can use to run a game set in the Dragon Prince
setting. I created this so that I can run games for my daughter who loves the
series.

** Primal Sources
All primal sources are *Domains*.

[[https://dragonprince.fandom.com/wiki/Magic#Primal_Sources][See the wiki for more details]].

*** Sun
TBD…

*** Moon
TBD…

Abilities

Boons
   Invisible

*** Stars
TBD…

*** Earth
TBD…

*** Sky
TBD…

*** Ocean
TBD…

** Races

*** Humans
Refer to the *generic human frame*. We don’t need to customize anything here.

*** Elves
Refer to the *generic elf frame*. Customize as needed to customize it for the
different Elven sub-frames (sub-species).

** Magic
TBD…

** Dark Magic
TBD…

** Bestiary
TBD…
