.. -*- coding: utf-8 -*-

:orphan:

.. |st| replace:: Simple Tales

.. title:: Simple Tales

.. include:: /global-message.rst

.. _setting-st:

.. todo:: *Better name*

   Come up with a better name for this setting. “Simple Tales” is currently just
   a placeholder name.

############
Simple Tales
############
.. topic:: Overview

   This is a simplification of the core system, to make it suitable for younger
   players. I originally created this to run a couple of quick games for my 5yr
   old daughter. We both enjoyed the experience so I decided to expand on this a
   bit more and create a more “official” version of it.

   :Date: |today|
   :Author: Andre Engelbrecht
