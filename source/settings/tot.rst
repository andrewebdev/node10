.. -*- coding: utf-8 -*-

:orphan:

.. |tot| replace:: Tales of Tyria

.. _setting-tot:

============
Inscriptions
============
In |tot| items can have inscriptions crafted into them. These inscriptions
imbue the item with spells and effects, accessible to the user.

Inscriptions is a way to add :ref:`equipment-abilities` to a weapon or item in
|tot|. The maximum number of inscriptions depend on the weapon rank.

.. math::

   {NumAbilities} = {WeaponLevel} \times 2

|

Known Inscriptions
==================
.. node:def:: ItemRecall
   :uid: tot.ItemRecall
   :type: Ability, Active

   Activate this ability to command the item to return to your hands.
   The maximum range at which this is effective is the
   :ref:`fixed maximum <fixed-med-max>` (in meters) for this node.

   If using this ability offensively, then this will count as a
   ``Control › Marksmanship › Weapon`` ranged attack roll.


===========
Professions
===========
.. todo:: *Custom professions that make sense*

   Create custom professions that makes sense in the world of Tyria. In the GW2
   MMO, professions are essentially your Character Class and is very limited.

   I a more open setting, hundreds of professions could potentially exist each
   with their own skill requirements and training.

   See :ref:`custom-backgrounds` for more information on creating custom
   backgrounds.

===========================
Disciplines and Skill Nodes
===========================
.. note:: *Characters can train in multiple disciplines*.

   Unlike the actual GW2 MMO, skills in this system are generally available to
   all characters, provided they put the effort in to train it.

   Professions will provide a default set of trained skill, should a player opt
   for this but it’s not a requirement.


.. _tot-skill-domains:

Skill Domains
=============
.. node:def:: Alchemy
   :uid: tot.Domain.Alchemy
   :type: A

   You have knowledge and experience in alchemy. Using this skill you can
   create, identify and use alchemical potions and components.

.. node:def:: Virtues
   :uid: tot.Domain.Virtues
   :type: P

   You have a virtuous spirit that burns with fiery justice, rejuvenating
   resolve, and courageous light.

   :Damage:

      * :node:ref:`Generic.Damage.Heat`

   :Support:

      * :node:ref:`Generic.Ability.Heal`

   :Boons:

      * :node:ref:`Generic.Boon.Fury`
      * :node:ref:`Generic.Boon.Regeneration`
      * :node:ref:`Generic.Boon.Resilience`

   :Conditions:

      * :node:ref:`Generic.Condition.Burning`

.. node:def:: Curses
   :uid: tot.Curses
   :type: Active

   *TODO*

.. node:def:: Death Magic
   :uid: tot.DeathMagic
   :type: Active

   *TODO*

.. node:def:: Blood Magic
   :uid: tot.BloodMagic
   :type: Active

   *TODO*

.. node:def:: Soul Reaping
   :uid: tot.SoulReaping

   *TODO*


Setting Specific Templates
==========================
.. todo::

   Create some base templates that suit the setting.

   A *Well Template*, for example, would consist of:

   .. mermaid::
      :align: center

      flowchart LR

         [/Channel/] --> [/Pulse/] --> |inside| [/Sphere/]
