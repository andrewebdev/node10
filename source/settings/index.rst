.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. title:: Custom Settings

.. include:: /global-message.rst

.. _custom-settings:

########################
Creating Custom Settings
########################
.. topic:: Overview

   This section will explain how you can adapt any milieu or genre to work with
   the |nem| System.

   For the most part you can just use the rules as normal, but the places where
   you may need to customize the rules are covered in more detail below.

   :Date: |today|
   :Author: Andre Engelbrecht


===========
Skill Nodes
===========
.. todo:: Creating Custom Skill Nodes


==============
Races (Frames)
==============
Custom frames should follow the following rules. Feel free to break the rules
here if you feel that it suits the setting.

Frame Attribute Maximums
========================
These are the maximum rank that player will be able to level up a attribute on
their chosen frame/race.

* Every attribute maximum on the frame will start with a baseline of 2.
* A single, attribute on the frame will have a 1 point maximum increase,
* … now you can freely increase any attribute by further points, but for the
  extra increase, you need to balance it out with an equivalent decrease
  somewhere else.

.. _custom-backgrounds:

===========================
Backgrounds and Professions
===========================
It’s not hard to create a custom background or profession. Just make sure that
it fits within the narrative and setting.

The background or profession will give the character 7 Skills on rank 1. These
are skills that the character needs to successfully work in that profession.

The background or profession will, however, limit the skill choices.

Sometimes, the character will also get some basic equipment and items, provided
by their chosen background or profession.

=========
Equipment
=========
.. todo:: Creating Custom Equipment


===================================
Specialized Skill Nodes (eg. Magic)
===================================
.. todo:: Creating special skill nodes for things like magic


================
Example Settings
================

:doc:`Tales of Tyria </settings/tot>`
=========================================
This is an custom setting for running games in the Guild Wars 2 setting. I play
the game often, and I how magic and technology is merged in this game world.
This world has such a good use case for RPG's.


:doc:`Simple Tales </settings/simple-tales>`
================================================
.. todo:: *Better name*

   Come up with a better name for this setting. “Simple Tales” is currently just
   a placeholder name.


This is a simplification of the core system, to make it suitable for younger
players. I originally created this to run a couple of quick games for my 5yr old
daughter. We both enjoyed the experience so I decided to expand on this a bit
more and create a more “official” version of it.
