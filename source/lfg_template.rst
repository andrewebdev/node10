.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. title:: Looking for Group - Silent Passenger

:orphan:

LFG - The Silent Passenger (18+)
================================
*Free*

A diverse group of stranger embark on an impromptu science expedition into the
unforgiving Tannup jungle, where they uncover something sinister.

:Slots Available:

   5 Players (Pre-made Characters: 2 Female, 3 Male)

:System Used:

   |nem| (Home-brew system that focus on fair outcomes, deadly combat, and a
   sprinkle of Drama with use of a drama dice)

:Style:

   Voice and Video (Video optional for players)

:Session Duration:

   Max 2 hours per session.
   Around 2-3 sessions to finish the adventure, depending on distractions etc.

   Session and total adventure time depends on time players spend on role-play
   and combat. Play time will be faster for seasoned players.

   I'm always willing to consider running longer sessions if players are
   enjoying the moment and wish to push for a bit more time.

:Schedule:

   I would prefer to run on a Friday or Saturday evenings (GMT).

:Requirements:

   * Discord
   * Microphone (optional webcam)
   * WebGL capable browser (Brave, Chrome or Firefox preferably).

I've put a lot of effort into this short adventure and would love feedback on
the story/plot, as well as the core system. Effort include adding custom maps,
sound tracks, lots of lore and back-story.

The game runs on Foundry Virtual Tabletop, and I spent a lot of time learning
the software and coding my core system for Foundry. For this reason I ask that
you only sign up if you know you'll be able to attend and finish the session(s).

Markdown Template For Discord
=============================
.. code::

   **LFG - The Silent Passenger (18+)**
   *Free*

   A diverse group of stranger embark on an impromptu science expedition into the unforgiving Tannup jungle, where they uncover something sinister.

   **Slots Available:** 5 Players (Pre-made Characters: 2 Female, 3 Male)

   **System Used:** |nem| (Home-brew system that focus on fair outcomes, deadly combat, and a sprinkle of Drama with use of a drama dice)

   **Style:** Voice and Video (Video optional for players)

   **Session Duration**

   Max 2 hours per session.
   Around 2-3 sessions to finish the adventure, depending on distractions etc.

   Session and total adventure time depends on how much time players spend on role-play and combat. Play time will be faster for seasoned players.

   I'm always willing to consider running longer sessions if players are enjoying the moment and wish to push for a bit more time.

   **Schedule:** I would prefer to run on a Friday or Saturday evenings (GMT).

   **Requirements:**

   * Discord
   * Microphone (optional webcam)
   * WebGL capable browser (Brave, Chrome or Firefox preferably).

   **Notes:**

   I've put a lot of effort into this short adventure and would love feedback on the story/plot, as well as the core system. Effort include adding custom maps, sound tracks, lots of lore and back-story.

   The game runs on Foundry Virtual Tabletop, and I spent a lot of time learning the software and coding my core system for Foundry. For this reason I ask that you only sign up if you know you'll be able to attend and finish the session(s).
