.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. image:: /branding/nemron-rpg-logo.png
   :width: 100%
   :align: center

|

.. topic:: Overview

   |nem| is a game system used for running Tabletop RPG’s. I developed it
   because I could never find a generic system that suited what I wanted in a
   system.

   This manual cover the core rules for the core |nem| system, along with some
   guidelines on how to use it with other settings, worlds or genres.

   :Author: **Andre Engelbrecht**
   :Licenses:

      * Nemron RPG System — Creative Commons Attribution-ShareAlike 4.0

      See, :doc:`/license` for details.

.. tip::

   If you are new to this game system, we suggest skimming through the
   :doc:`/system/rules`, and then play through one of the testing modules.

   This will give you a feel for the system and it’s capabilities, before
   committing to it for a larger adventure or campaign.

===============
Why Use Nemron?
===============
There are plenty great systems out there. I once considered running my
sci-fi campaign on two of them (Cypher and Savage Worlds). That said there was
always one or two things that I could not get in any other system or that would
prevent me from finishing.

Meanwhile, I’ve been home-brewing a system on-and-off since 2004, which have
always remained on the shelf as a hobby project. In 2020, I decided I should
just take that and make it more official. This is what became Nemron.

Simple Difficulty Scale
   Nemron aims to use a difficulty scale that is easy and feels natural.
   Difficulty in Nemron is a simple ``1-10`` for basic difficulties. We do have
   a higher difficulty scale (``11-20``), reserved for heroic difficulties.

   This allows the GM to quickly make up realistic difficulties for rolls,
   especially when players decide to do something unexpected.

No Classes
   For me, classes in RPG’s also “feels” unnatural and restrictive. Looking at
   the most popular games on the market right now, if players want something
   specific and thematically different to a existing class, they have to resort
   to either multi-classing, or creating custom subclasses to shoe-horn the
   flavour into the game.

   This adds extra complexity, rules and wasted time just to create something
   that might only exist in one campaign.

   In Nemron, we allow any character to train in *any available skill*. The only
   restriction is those constraints within the fantasy world or setting that the
   character is in.

Every Skill is Useful
   I never like the “closed” nature of skills in other games, where you either
   use this skill or that skill. In reality your knowledge and experience in one
   domain might cross over into another, and this is rarely reflected in game
   systems.

   In the Nemron system we try to promote blending and mixing skills together,
   so that a player don’t end up with some skills which they feel is useless.

   The dancer can choose to add their dancing skill to a combat stance. An
   acrobat can add their acrobatics skill to an evasion action. A physicist can
   add their knowledge on thermodynamics or plasma to their fire magic rolls.

Skill Blending
   Players can dynamically combine their skills with different
   :doc:`/system/intentions` to create a specific effect *on-the-fly*.

   The sword wielder can use his sword to do slashing damage, cause bleeding or
   stun the target by bashing with the pommel. Each effect is a
   :doc:`intention </system/intentions>`.

   PC’s can then blend skills, abilities and other intentions together to create
   a variety of custom effects.

Drama
   Many game systems have either bad or good rolls (natural 1 or natural 20, for
   example). I wanted a system where a *good* roll could, potentially, have an
   good or bad outcome. In the same way a *bad* roll could have a bad, or good
   outcome.

   I wanted a way to express this “random dramatic nature” in the system, and we
   do this using a stand-alone drama dice, that can randomly inject drama into
   any scene.

   *See:* :ref:`rules-drama` and :ref:`rules-drama-example`.

==============
System Details
==============
.. toctree::
   :maxdepth: 1
   :caption: Contents:

   /system/index
   /settings/index
   /contributions/index
   /colophon
   /license

.. include:: /content-footer.rst
