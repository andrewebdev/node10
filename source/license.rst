.. -*- coding: utf-8 -*-

.. title:: License

#######################################
Nemron RPG and Nemron Universe Licences
#######################################
.. topic:: Overview

   This page covers the different licenses for the system, and what you as a GM,
   player or content creator can do with it all.

   :Date: |today|
   :Author: Andre Engelbrecht


.. _license-nemron-rpg:

Nemron RPG Core License
=======================
.. note::

   The license below only applies to the core rules as outlined in
   :doc:`system/index`.

Nemron RPG System (c) by Andre Engelbrecht

Nemron RPG System is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see http://creativecommons.org/licenses/by-sa/4.0/.

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png


Important, notes regarding third party content
==============================================
While the Nemron RPG System can be used for any game world and setting, there
are some limitations to what you as a content creator can do. This is due to the
restrictions in the third party licensing and legal trademarks.

If you create content based on licensed trademarks or copyrighted content, then
the Nemron License *only applies to the Nemron RPG rules* as used in your
custom content.

The original owner still maintain their original trademark and copyright.
