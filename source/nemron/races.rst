.. -*- coding: utf-8 -*-

.. title:: Frames

======
Frames
======
The following is a list of known frame and races in the Nemron Universe. But the
principle for using them applies to any game world or genre. You can use them as
is, or adapt them to your needs. For more “common” frames and races to use in
most genre’s and settings, see :doc:`/system/frames`.

.. _nemron-races-solborn-human:

Solborn Human
=============
These are the humans born in the Sol system. Humans are quick to adapt to change
and quickly pick up new skills.

A starting human character may pick:

* Any 3 skill nodes…
* … or 2 skill nodes and 1 *Trait (boon)* talent your character might have.

Human Attribute Nodes Maximums
------------------------------
:Body: Power (3), Agility (3), Constitution (5)
:Mind: Intellect (4), Awareness (3), Willpower (3)
:Spirit: Control (4), Sense (3), Focus (4)


.. _nemron-races-solborn-hybrid:

Solborn Hybrid
==============
The hybrid are the offspring of genetically modified humans from thousands of
years ago. This variant of the Solborn looks a lot like their human cousins,
except that some of their features are a bit more exaggerated than others.

Starting hybrid characters get the following:

* Pick any 1 *active or passive* skill node.
* Pick any 2 *hybrid* skill nodes of any type, so long as they are available to
  the chosen hybrid group.

Canine Hybrid Maximums
----------------------
:Body: Power (3), Agility (3), Constitution (5)
:Mind: Intellect (4), Awareness (4), Willpower (2)
:Spirit: Control (4), Sense (4), Focus (3)

Feline Hybrid Maximums
----------------------
:Body: Power (3), Agility (5), Constitution (3)
:Mind: Intellect (3), Awareness (4), Willpower (3)
:Spirit: Control (4), Sense (4), Focus (3)

Ursine Hybrid Maximums
----------------------
:Body: Power (5), Agility (3), Constitution (3)
:Mind: Intellect (3), Awareness (4), Willpower (3)
:Spirit: Control (4), Sense (4), Focus (3)

Raven Hybrid Maximums
---------------------
:Body: Power (3), Agility (4), Constitution (3)
:Mind: Intellect (4), Awareness (4), Willpower (3)
:Spirit: Control (3), Sense (4), Focus (4)

Hybrid Skill Nodes
------------------
* :node:ref:`Generic.Boon.KeenSense`
* :node:ref:`Roar, Growl or Snarl <Generic.Ability.Shout>`

.. todo:: Add some more hybrid skill Nodes

.. _nemron-races-constructs:

Constructs
==========


Dravoranians
============
..  todo::

    We will have 4 different casts of Dravorian. Each will have their own
    features, but since they all share evolutionary ancestry, they will have
    similar features.

    Ancestor was a creature with 6 arms/legs, tail and long neck with head.
    The different Dravorian casts all have same base structure, but some will
    have different minor features, some stronger and more muscular, some longer
    necks, other shorter tails, etc. etc.

.. include:: /content-footer.rst
