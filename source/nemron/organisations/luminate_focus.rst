.. -*- coding: utf-8 -*-

.. title:: Luminate Focus

==============
Luminate Focus
==============
The Luminate Focus is a secret organisation that values truth, fact. They work
from the shadows to uncover conspiracies, corporate wrongdoing, corruption and
anything they deem dangerous to civil society.

They accept that conflict cannot always be avoide, and that everyone in society
cannot ever be on equal social standing. But they do believe that every member
of society deserves the right to live with dignity and opportunity.

Their activities include:

* Investigate and uncover corruption or dangerous activities.
* Fact check any information gathered by other agents, and make sure that it's
  accurate, and that there is enough evidence.
* Leak the details of the information gathered to sources that can disseminate
  it into the population and media.
* See what government and public reaction is to this. If public opinion or
  government action isn't moving in the right direction, they may take a more
  direct approach.
* Where appropriate, take a more direct approach, by forcefully stopping or
  dismantling dangerous plots, before they have a opportunity to take hold.


Recruitment
===========

A existing member may make a recommendation for a new recruit, but the recruit
cannot know anything about the organisation beforehand.

Once a recommendation was sent, the Luminate Focus will start background checks
and investigations into the candidate for more detailed info.

At the same time a elite member of the Luminate Focus will start testing the
candidate without their knowledge. Typically they will place certain items and
clues in their path. Some items truthful, some fictional. They will observe how
they resolve issues and discover the truth.

They will also test candidates allegiances by seeing what they will do with
when presented with damaging information, sometimes even personal information.

All this will be done without the player realising they are being tested.


.. include:: /content-footer.rst
