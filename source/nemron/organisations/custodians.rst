.. -*- coding: utf-8 -*-

.. |TC| replace:: The Custodians

.. title:: The Custodians

##############
The Custodians
##############
.. topic:: Author(s)

   * Andre Engelbrecht

.. note:: **WORK IN PROGRESS**

.. todo::

   Need a new section talking about DAIO’s and their roles within the Nemron
   Setting and Universe.

The custodians is a *Democratic Artificial Intelligence Organization (DAIO)*.
The different AI’s grouped together within this organisation with the aim to
protect organic life *and* civilization.

|TC| know that they “organics” will always judge any AI, and that mistrust can
quickly creep into society. For this reason they always remain as neutral as
possible. |TC| will also publish their meetings and decision trees in open
access journals, for public scrutiny.

========
Services
========

Protective Custody
==================
.. todo:: **Expand on this, and do a proper edit pass.**

Individuals can request protective custody. In extreme cases they can sign a
contract where the persons’ consciousness will be transferred into a android
frame.

The android itself is controlled by both the AI and the protected individual.
They become a partnership. The individual lives a new life, with a new
identity. The AI will passively observe surroundings and determine any risks
or harm that may affect the individual.

This partnership is temporary, until any threat to the individual passed, or
until the contract ends.


.. include:: /content-footer.rst
