.. -*- coding: utf-8 -*-

.. title:: Organisations

=============
Organisations
=============
Below is a list of some organisations within the Nemron universe. Each
organisation will have a history and hierarchy. Some will also have pre-defined
character backgrounds that players can use to base their characters on.

.. toctree::
   :maxdepth: 1
   :caption: Important Organisations

   arcgen
   custodians
   luminate_focus
   ysc

.. include:: /content-footer.rst
