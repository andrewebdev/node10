.. -*- coding: utf-8 -*-

.. title:: Ynglrost Sustainability Council

=====================================
Ynglrost Sustainability Council (YSC)
=====================================
One of the most important organisations that forms part of the Tannup Jungle
government. They have authority to investigate, detain and charge anyone
breaking conservation laws within the Tannup region.

.. include:: /content-footer.rst
