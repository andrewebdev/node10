.. -*- coding: utf-8 -*-

.. title:: Arcgen

======
Arcgen
======
Arcgen is a company dedicated to genetic research and development. Their
research have made strong advances in curing many genetic diseases and
conditions.

They recently started researching the Chask symbiote in the Tannup jungle.
The primary research goals for Arcgen, is to understand how the Chask is able to
repair nerve damage. They hope to develop treatments for spinal and nerve
damage, without a patient needing to fully bind to the symbiote.

This would make healing nerve damage more cost effective for the general
populace, since the current cybernetic implants and solutions are expensive.
There are also many people that don’t like the idea of having to host a
symbiote.

For this reason Arcgen offices are located in
:doc:`/nemron/starsystem_01/terra_verda/tannup/ynglrost`. From here they can
send for samples in the jungle and have research facilities close by.

Arcgen’s new CEO, :doc:`/nemron/npcs/handouts/archibald_simmons`, vowed to make
the company stronger and more relevant in this age.

.. include:: /content-footer.rst
