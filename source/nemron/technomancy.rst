.. -*- coding: utf-8 -*-

.. title:: Technomancy

.. include:: /global-message.rst

###########
Technomancy
###########
Technomancy is a special domain of academic research and advanced
engineering. Engineers create special interfaces, modules and functions which
is then used by someone with enough knowledge and experience.

.. note::

   In the Nemron Universe, Technomancers *cannot* use Biomancy and visa versa.
   This is because the interface communicate through nerve endings in the skin,
   or via cybernetic implants.

   Since the Chask Symbiote (required for Biomancy) fuses with the nervous
   system, it will actively attempt to block what it perceives as an attack on
   the nervous system.

==================
Technomancy Basics
==================
The interfaces will emit a swarm of nano-machines, to create the intended
effects. Each nanite-swarm behave like an insect in a swarm or colony. By
itself, a nanite’s behaviour might seem random and out of place, but when an
entire swarm of nano-machines perform their instructions the effects are
impressive.

Nano-machine swarms is visible to the trained eye. It looks like a misty vapour
or cloud. Depending on the :ref:`template <ability-templates>` used by the
interface and Technomancer, the nanite-swarm will take on the shape of the
template, right before the actual effect activates.

Once the nano-machines completed their functions, they “die”, and the spent
husks fall to the ground.

Tracking and Policing Technomancy
=================================
Due to laws and regulations, every Technomancy user must have a licence. Owners
of Technomancy interfaces must register their interfaces with authorities, and
they are only allowed to purchase or use interfaces with a valid licence.

When used, the licence number, current date/time and nanite-instructions, is
logged into both the swarm-capsule and interface that released the
nanite-swarm. This allows investigators and authorities to read the interface or
empty capsule logs during crime investigations.

Core Technomancy Skills
=======================
.. node:def:: Technomancy
   :uid: Nemron.Skill.Technomancy
   :type: A

   *Required by anyone to use Technomancy interfaces.*

   The core skill that enables the practitioner to use Technomancy interfaces
   and codices.

   Damage, Support, Boons and Conditions all depend on the interface and
   associate skills.

   This skill also covers knowledge about Technomancy in the Nemron Universe
   in general.

.. _nemron-techno-energy:

Batteries, Energy and Swarm Capsules
====================================
Technomancy functions and features require energy and nano-machines.

The energy can come from any of the following sources:

* An Energy module plugged into the interface.
* Hard-wired power cable running from a generator or mains power from a
  building.
* The character energy pool

.. item:def:: Swarm Capsule
   :uid: Nemron.Techno.SwarmCapsule

   | **Level:** 1
   | **RRP:** 200

   Regulated companies produce the nano-machines and store them in single-use
   :item:ref:`“Swarm Capsules” <Nemron.Techno.SwarmCapsule>`. When using
   Technomancy, the interface will send instructions to the nanites inside the
   capsule, after which the capsule release the nanite-swarm to execute their
   instructions.

   The interface will expel the empty capsule in the same way a gun might expel
   an empty cartridge.

.. _nemron-techno-interfaces:

Technomancy Interfaces
======================
Technomancers need to have an active link with a Technomancy interface. Some
common interfaces include, clothing, weapons, tools and workbenches.

The main requirement is that the user must have a linked connection with the
interface, and they need to have the :node:ref:`Nemron.Skill.Technomancy` skill.

Physical contact is the usual way to establish a link with an interface, but
some interfaces can be wireless (like a remote control drone owned by a
Technomancer).

General Interfaces
------------------
.. item:def:: Technomancy Gloves
   :uid: Nemron.Interface.Gloves

   | **Level:** 1
   | **RRP:** 5,000
   | **Modules:** 1 per glove
   | **Capsules:** Maximum 2 per glove, depending on what else you have in your
     hand.

   **Included Templates:**
   :node:ref:`Generic.Ability.Touch`

   Fingerless gloves that allows the user to just use a
   :item:ref:`Nemron.Techno.SwarmCapsule` or module by just holding it in their
   hand(s).

   This has the unique benefit that the user don’t need to load a capsule into
   an interface, they can just grab it and use it immediately.

.. item:def:: Technomancy PDA
   :uid: Nemron.Interface.PDA

   | **Level:** 1
   | **RRP:** 10,000
   | **Modules:** Up to 2
   | **Capsules:** Up to 5
   | **Battery:** 10 Energy
   | **Included Templates:**
     :node:ref:`Generic.Ability.Beam`,
     :node:ref:`Generic.Ability.Pulse`

   A Technomancy PDA that will allow the user to interface with up to two
   different Codices.

   This interface also count as a programming interface. It can copy, delete or
   edit Technomancy skills on the codex.

   You can also use it to create new skills to save them into the codex.

   .. todo::

      We need to expand on the programming rules for codices and link to them
      here.

.. item:def:: Technomancy Pistol
   :uid: Nemron.Interface.Pistol

   | **Level:** 1
   | **RRP:** 24,000
   | **Modules:** Maximum 1
   | **Range:** 40m
   | **Capsules:** 8 per magazine
   | **Battery:** 20 Energy
   | **Included Templates:**
     :node:ref:`Generic.Ability.Beam`,
     :node:ref:`Generic.Ability.Pulse`,
     :node:ref:`Generic.Ability.Touch`

   When fired, a small concentrated bead of nanites launch towards the target.
   Each bead will automatically activate on :node:ref:`Generic.Ability.Touch`.

   Once the nanite bead makes impact with something, the pre-programmed effect
   will invoke as though it originated from the *impact point* and not the user.

   The impact from the bead itself don’t do damage, since the nano-machines
   instantly disperse on impact to perform the programmed tasks.

      **Example Narration**

      Yanet takes a moment to focus her knowledge of biology into the pistol.

      When the next nanite-swarm hit the target she wants the nano-machines
      to over-stimulate the nerves and stun the target.

   In the above example Yanet’s player rolls
   ``Control › Technomancy › Biology » Condition: Stun = 3 AP``.

   Every shot fired will now spend ``1 Energy`` (for the :node:ref:`Touch
   Template <Generic.Ability.Touch>`) and, if it hits a biological target, will
   :node:ref:`Stun <Generic.Condition.Stunned>` the target if they fail their
   resistance roll.

.. _nemron-interface-modules:

Interface Modules
=================
Most interfaces support extra functionality through the use of modules.

These modules can provide extra pre-programmed functions, energy and other
features to the interface.

A module **cannot** work with a interface that is a lower rank than the module
itself.

Common Modules
--------------
.. item:def:: Battery
   :uid: Nemron.Module.Battery

   | **Level:** 1
   | **RRP:** 5,000

   Provides extra power to the interface based on the module rank.

   | **Level 1:** 5 Energy
   | **Level 2:** 8 Energy
   | **Level 3:** 10 Energy
   | **Level 4:** 15 Energy

Codex Modules
-------------
Codices are modules that provide expanded storage for Technomancy Interfaces.

In addition to storing :ref:`ability-templates` on a codex, engineers with
enough theoretical and technomancy knowledge can create custom recipes and store
them on a codex.

.. item:def:: Student Codex
   :uid: Nemron.Module.StudentCodex

   | **Level:** 1
   | **RRP:** 50,000
   | **Max Functions:** 2

   A codex given to governments, higher education and commercial companies, to
   assist in training students and workers.

.. item:def:: Common Codex
   :uid: Nemron.Module.CommonCodex

   | **Level:** 2
   | **RRP:** 150,000

   These codices must have valid documentation and licences to be able to use
   them.

   Common codex levels can be anywhere between *ranks 2 to 4*.

   Max Functions by Rank:
      | **Rank 2:** 3 functions max
      | **Rank 3:** 4 functions max
      | **Rank 4:** 5 functions max

   RRP By Rank:
      | **Rank 2:** 150,000
      | **Rank 3:** 300,000
      | **Rank 4:** 450,000

Commercial and Industrial Interfaces and Modules
================================================
These are some special interfaces that make use of technomancy for various
industries.

.. item:def:: Welding Interface
   :uid: Nemron.Interface.Welder

   | **Level:** 2
   | **RRP:** 10,000
   | **Codex Slots:** 1
   | **Capsules:**
     Uses a small “swarm tank” rather than capsule, which contain the
     nano-machines equivalent of
     :item:ref:`100 swarm capsules <Nemron.Techno.SwarmCapsule>`.
   | **Pre-Programmed Templates:**
     :node:ref:`Generic.Ability.Touch`,
     :node:ref:`Generic.Ability.Beam`

   This is a machine that has built in welding programs and functions.

   **Welding Codex:**
   A Level 2 Common Codex with functions dedicated to manipulating and
   welding steel alloys.

.. item:def:: Medical Bed
   :uid: Nemron.Interface.MedicalBed

   | **Level:** 4
   | **RRP:** 200,000
   | **Codex Slots:** 4
   | **Capsules:**
     Uses a medium “swarm tank” rather than capsule, which contain the
     nano-machines equivalent of
     :item:ref:`500 swarm capsules <Nemron.Techno.SwarmCapsule>`.

   In hospitals it is common to have 1 main tank and 2 backups connected to
   each medical bed.

   **Pre-Programmed Templates:**
   :node:ref:`Generic.Ability.Touch`,
   :node:ref:`Generic.Ability.Shape`,
   :node:ref:`Generic.Ability.Beam`,
   :node:ref:`Generic.Ability.Pulse`,

   This is a medical bed used for diagnosis and treatment of patients.
   This interface can be used without a codex, but does have 4 available codex
   slots so that specific functions can be provided, based on treatment required
   by the patient.

   A technomancer can interface with the bed through touching the control
   interface.

   Holographic projectors can emit visual graphs and feedback where required.

.. item:def:: Bio-Chemix Station
   :uid: Nemron.Interface.BioChemix

   | **Level:** 4
   | **RRP:** 1 mil
   | **Codex Slots:** 4
   | **Capsules:**
     Uses a medium “swarm tank” rather than capsule, which contain the
     nano-machines equivalent of
     :item:ref:`500 swarm capsules <Nemron.Techno.SwarmCapsule>`.
   | **Included Templates:**
     :node:ref:`Generic.Ability.Shape`,

   This is a large machine and technomancy interface that can mix and manipulate
   chemicals. This allows for creating compounds and can even manipulate the DNA
   of organisms placed inside etc.

   The interface release nano-machines into the soup of chemicals within the
   large glass chamber, where they will execute the commands and functions
   entered by the operator.

.. Author(s)

   * Andre Engelbrecht

.. include:: /content-footer.rst
