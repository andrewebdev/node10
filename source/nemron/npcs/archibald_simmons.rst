.. -*- coding: utf-8 -*-

:orphan:

.. title:: Archibald Simmons

=================
Archibald Simmons
=================
.. include:: /spoilerwarn.rst

.. note::

   **TODO**

   * Talk about how he's actually an honest business person. Sure he skits
     around the edges what is legal, but contrary to rumours and reporting, he
     does actually care about well-being of staff and communities. It’s had to
     find a balance between doing what’s good for everybody, and because of this
     his intentions are often misinterpreted.
   * Talk about how he's trying to provide opportunity for his son, but is
     finding it hard because his son, Jordan, doesn't seem to have similar
     ambitions


.. include:: /content-footer.rst
