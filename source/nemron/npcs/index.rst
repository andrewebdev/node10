.. -*- coding: utf-8 -*-

.. title:: Notable NPC’s

==============
Notabale NPC’s
==============
.. include:: /spoilerwarn.rst

This section contains a list of notable NPC’s in the wider Nemron Universe in
general. These are important figures in society, or individuals that have a key
influence, public or private, within the universe.

.. toctree::
   :maxdepth: 1

   archibald_simmons
   jordan_simmons
   alex_simmons

.. include:: /content-footer.rst
