.. -*- coding: utf-8 -*-

.. |,| unicode:: , 0xA0

:orphan:

.. title:: Jordan Simmons

==============
Jordan Simmons
==============
.. include:: /spoilerwarn.rst

Bio
===
Jordan Simmons, son of :doc:`/nemron/npcs/archibald_simmons`, have long lived
in the shadow of his dad’s success. Everyone always said that he’s only
successful because his dad helped him and gave him the opportunity to do so.

Every job he’s always had, was because he was appointed by his dad.

When Archibald bought majority shares in ArcGen, a company that specializes in
genetic research and development, with a primary focus on the Chask symbiote and
it’s potential applications, Jordan saw an opportunity.

He thought that he could potentially control the R&D departments and develop
he’s own species of the Chask Symbiote through Gene Editing, and then create his
own company that would specialize in creating this variant for Military
purposes.

He then manipulated his dad to get him employed at ArcGen as Head of Research
and Development.

Secret Project - Gene Edited Chask
==================================
Jordan is attempting to create a gene edited version of Chask symbiote that can
be used to manipulate a infected individual pre-programmed commands.

A code-word, phrase or image could be used to activate the Chask, and then the
host would complete the command as instructed. This can be used by military,
governments or corporate spies, to have someone be a sleeper agent without the
host knowing.

He codenamed this new version of the organism:
``GEC-CE (Gene Edited Chask for Command Execution) Symbiote``

Encounter Stats
===============

.. _actor-jordan-simmons-pre-gech:

Jordan Simmons (Pre-GECH)
-------------------------
Frame:
   :ref:`nemron-races-solborn-human`

Attributes:
   | Power [1], Agility [2], Constitution [2]
   | Intellect [4], Awareness [3], Willpower [2]
   | Control [4], Sense [2], Focus [2]

Skills:
   Fencing [4],
   Biology [1],
   Chemistry [3],
   Law [2],
   Economics [2],
   Insight [3],
   Investigation [3],
   Persuasion [3]
   Intimidation [2]

Equipment
---------
* item.L2 Chask
* item.L2 Oral Symbiote

:item:ref:`Level 2 Chask Abilities <Nemron.Chask>`
--------------------------------------------------
:Templates:
   :node:ref:`Generic.Ability.Amplify` allows you to efficiently keep some
   biomancy effects active for longer periods.

:Support:
   * :node:ref:`Generic.Ability.Cleanse`

:Boons:
   * :node:ref:`Generic.Boon.Alert`
   * :node:ref:`Generic.Boon.Regeneration`
   * :node:ref:`Generic.Boon.Immunity`

:item:ref:`Level 2 Oral Symbiote Abilities <Nemron.OralSymbiote>`
-----------------------------------------------------------------
:Damage:
   * :node:ref:`Generic.Damage.Corrosive`

:Support:
   * :node:ref:`Generic.Ability.Detection` via taste.

:Conditions:
   * :node:ref:`Generic.Condition.Corrosion`
   * :node:ref:`Blind <Generic.Condition.Dull>`

.. _actor-jordan-simmons-gech:

Jordan Simmons (GECH)
---------------------
Karma finally caught up with Jordan and he was turned into a GECH by none other
than his adopted :ref:`Alex <nemron-npc-alex-simmons>`. Skills are the same as
:ref:`pre-gech <actor-jordan-simmons-pre-gech>` since he only became a host
recently.

Just rename the “Chask” symbiote to “Gech” (it stays the same rank) and he can
now :node:ref:`infect <Nemron.Gech.Infection>` the target with a successful
bite.

.. include:: /content-footer.rst
