.. -*- coding: utf-8 -*-

.. title:: Biomancy

========
Biomancy
========
.. todo::

   Move this to Nemron section. It doesn’t really belong in the core mechanics.
   Core mechanics should have some example skill domains which can be mentioned
   and referenced with links in the mechanics section, but we do need to keep
   the core mechanics area clean and tidy for quick and easy rule reference.

Biomancy is a domain of skills that are *only available to Chask hosts*. The
skills are used to enhance the individual's body in various ways, or to instruct
other symbiotes attached to the host to activate their skills/effects.

Every symbiote will provide a unique set of skills or abilities. All symbiote
skill nodes should be treated as *Fixed Nodes* (see:
:ref:`rules-node-types`), and will always be the level of their related
symbiote.

This means that, if the symbiote evolves to a new level, then all the skills
that it provides will automatically rank up alongside the symbiote.


For a list of symbiotes and their related skills, see
:doc:`/nemron/equipment/biomancy`.


Extra Biomancy related skills
=============================
.. node:def:: Biomancy Theory
   :uid: Nemron.Skill.BiomancyTheory

   You have theoretical and practical understanding of biomancy and how it
   works. You also have knowledge on the history and academic research in this
   field.

.. include:: /content-footer.rst
