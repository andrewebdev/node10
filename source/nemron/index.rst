.. -*- coding: utf-8 -*-

.. title:: Nemron Universe

===============
Nemron Universe
===============
.. topic:: Overview

   :Author: **Andre Engelbrecht**

License
=======
The Nemron Universe (c) by Andre Engelbrecht

The Nemron Universe is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see http://creativecommons.org/licenses/by-sa/4.0/.

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png

Important, notes regarding third party content
----------------------------------------------
While the Nemron RPG System can be used for any game world and setting, there
are some limitations to what you as a content creator can do. This is due to the
restrictions in the third party licensing and legal trademarks.

If you create content based on licensed trademarks or copyrighted content, then
the Nemron License *only applies to the Nemron RPG rules* as used in your
custom content.

The original owner still maintain their original trademark and copyright.

Introduction
============
The Nemron Universe is the Sci-Fi setting for which we originally designed
the system for.

The universe takes place far in our future timeline. Humanity slowly spread
to 3 different star systems, and only made contact with one new alien
species.

.. toctree::
   :maxdepth: 2
   :caption: Nemron Universe

   races
   technomancy
   biomancy
   equipment/index
   npcs/index
   organisations/index
   starsystem_01/index


.. include:: /content-footer.rst
