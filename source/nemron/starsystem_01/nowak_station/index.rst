.. -*- coding: utf-8 -*-

:orphan:

.. title:: Nowak Station


.. _nemron-nowak-station:

#############
Nowak Station
#############
.. topic:: Overview

   .. include:: _overview.rst

   :Date: |today|
   :Author: Andre Engelbrecht


=======
History
=======

.. todo:: TBD [#kaz_nowak]_


===============
Modular Station
===============
.. todo:: TBD

==================
Places of Interest
==================

Station Command
===============
.. todo:: TBD

Barracks
========
.. todo:: TBD

Commerce Hub
============
.. todo:: TBD

Research and Development
========================
.. todo:: TBD

Residence Hub
=============
.. todo:: TBD

Upper Residence Hub
===================
.. todo:: TBD

Civil Services Hub
==================
.. todo:: TBD


----

.. rubric:: Footnotes

.. [#kaz_nowak]

   Shortened version of *Kazimierz Nowak*, a famous explorer in the history of
   the Sol star-system.


.. include:: /content-footer.rst
