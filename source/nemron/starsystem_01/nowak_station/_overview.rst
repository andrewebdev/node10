The Nowak Space Station is named after the *Kaz Nowak* science vessel that first
came to inspect the planet :ref:`nemron-terra-verda`.
