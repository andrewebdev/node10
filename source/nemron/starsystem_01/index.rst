.. -*- coding: utf-8 -*-

.. title:: Starsystem 01


#############
Starsystem 01
#############
.. topic:: Overview

   :Date: |today|
   :Author: Andre Engelbrecht

.. todo::

   Need a better name for the star system.
   Current shortlist:

   * NuSol
   * solbeta
   * betasol
   * echosol

==================
Places of Interest
==================
The following is a list of *known* planets, asteroids, space stations and other
places within the star system.

Planetary Bodies
================

:doc:`terra_verda/index`
------------------------
.. include:: terra_verda/_overview.rst

Space Stations and Facilities
=============================

:doc:`nowak_station/index` [#kaz_nowak]_
----------------------------------------
.. include:: nowak_station/_overview.rst

----

.. rubric:: Footnotes

.. [#kaz_nowak]

   Shortened version of *Kazimierz Nowak*, a famous explorer in the history of
   the Sol star-system.

.. include:: /content-footer.rst
