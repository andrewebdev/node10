.. -*- coding: utf-8 -*-

.. title:: Tannup Bestiary

###############
Tannup Bestiary
###############
:Author(s):

   * Andre Engelbrecht

.. _tannup-random-encounters:

=================
Random Encounters
=================
Here are some quick random encounters that you can use in your games. They are
just a guide so feel free to change, remove or add your own.

.. csv-table:: Random encounter roll table
   :header: "Roll", "Encounter", "Description"

   "1", "No Encounter", ""
   "2", "Vinebear Family", "Vinebear Family, consisting of 2 cubs and Mum"
   "3", "Trapped Animal", "Animal Trapped in illegal snare"
   "4", "Poachers", "Party stumbles on a group of 3 poachers"
   "5", "Leech Fall", "Leeches drop on the party"
   "6", "Trapped Vinebear Cub", "Party finds a cub stuck in a sinkhole"
   "7", "Sinkhole", "Party potentially fall into a sinkhole"
   "8", "Rachni Ambush", "Rachni ambush consisting of 1d2 adults, and 1d4 juveniles"
   "9", "Shoreline Ambush", "Party get too close to the shoreline where lashers are waiting for prey in the water."
   "10", "Jungle fever", "1 or more party members were bitten by insects that causes an infection"


==========
Encounters
==========

Lasher
======
.. todo:: **Write description of this creature properly**

   2 front legs, claw like hands (similar to a chameleon).
   It has a torso that gradually narrow into a very long tail.

   Tail can be controlled like a snake, to provide grip, movement or to attack
   with.

   The entire body has got the scaly texture similar to an anaconda.

   Long protruding jaw (Imagine a *slightly* elongated Jaquar skull).

.. actor:def:: Lasher
   :uid: Nemron.Tannup.Lasher
   :frame: Lasher
   :body: 3,3,2
   :mind: 1,4,1
   :spirit: 1,1,1

Skills:
   Brawling [2],
   Stealth [2],
   Survival [3],
   JungleBiome [3]

Items
-----
* L2 Claws: Power, Brawling
* L2 Teeth: Power, Brawling
* L1 Tail: Agility, Brawling

Lasher Tactics
--------------
Lashers prefer to ambush their prey from water. They will hide just below the
water’s surface, and wait for prey to drop their guard so that they can strike
out from there.

The usual tactic used by a Lasher is to grapple and drag prey under water and
wait for them to drown, but they also like to use their strong snake-like bodies
to wrap around and constrict their prey.

.. note:: *Escaping the Grapple*

   The normal :ref:`grappling rules <rules-grappling>` apply here but the
   rules change slightly when the prey is *grappled/constricted with the tail*:

   * The Lasher gets a *Lesser Penalty* instead of the normal *Greater Penalty*
     for any actions that don’t apply to the grapple.
   * The Lasher doesn’t get any penalty for *Mind and Spirit* rolls.

   The reason for this is that the Lasher wraps their prey in the tail, and
   can still move the upper body semi-freely.


Leech Fall
==========
Leeches in Tannup are harmless by themselves. They have evolved a toxin that
numb the area around the bite, preventing detection. The real problem with these
leeches is that they evolved a nasty ambush tactic that cause them to group in
large swarms.

They attach themselves on the bottom of large trees. The swarm  can range from
10 to hundreds of individual leeches.

Once a prey animal walks below the leeches, the entire swarm drop onto the
animal at once and, immediately attach and start feeding on the animal.

Due to the large number of leeches, the amount of numbing toxin increase to
dangerous levels, usually causing the animal to become drowsy and fall asleep.

The leeches will stay attached to their prey until removed, or the animal dies.
For this reason they evolved to inject just enough of their toxin to keep the
animal alive as long as possible, but weak and sleepy enough not to immediately
remove all the leeches.

Once the prey animal dies, they will head back up to the canopy’s for the next
ambush.

Running the encounter
---------------------
To passively spot a leech swarm require an awareness roll of ``7``. Actively
scanning for dangers while walking reduces the difficulty to ``5``.

The leeches are tiny, so even if the character notice the dark, “dotted” pattern
on the bottom of tree branches or leaves, they won’t necessarily know what it is
without an intelligence check of ``4`` (jungle or survival related skill nodes
can be used) or closer inspection.

If someone failed to spot the leeches and walks underneath, the leeches will
just drop, all at once, onto the target. They make no sound when this happens,
and the character will feel like some water droplets fell from the canopy.

GM rolls :roll:`1d10` to determine the *toxin damage per hour*. To determine the
number of leeches that do that amount of damage, just multiply the roll by `10`.

Every hour, the victims need to make a *constitution resistance roll* vs. the
toxin damage roll. This roll must be made *every hour*, until they removed
enough leeches to not be affected by the toxin any more.

.. math::

   {ToxinDamage} = {ToxinRoll} - {ConstitutionResistanceRoll}

|

Failing the roll they will take damage to the *energy pool*. When the energy
pool is empty, two things happen:

* Toxin damage now goes to the Body HP.
* The target needs to make a willpower roll vs. the toxin damage, to remain
  awake. If they fail this resistance roll, they will fall asleep till the next
  roll is required.

.. note::

   If for some reason only *some* of the leeches are removed, then the GM will
   need to adjust the damage for the subsequent hours. Remember that the leeches
   will do ``1 point`` of energy damage for *every 10 leeches* attached to the
   target.

.. _tannup-vinebear:

Vinebear
========
Vinebears are bear-like creatures that roam the jungle. They are omnivores that
generally feed on small animals and certain plants and fruits.

Vinebears get their name from the :item:ref:`Nemron.VineSymbiote` which they
commonly host on their bodies.

.. actor:def:: Vinebear
   :uid: Nemron.Tannup.Vinebear
   :frame: Vinebear
   :body: 3,1,3
   :mind: 1,3,1
   :spirit: 3,1,1

.. actor:def:: Vinebear Cub
   :uid: Nemron.Tannup.VinebearCub
   :frame: Vinebear
   :body: 1,1,2
   :mind: 1,2,1
   :spirit: 2,1,1

Skills:
   Evade (1),
   Brawling (3),
   Stealth (1),
   Survival (3),
   Jungle (3),

Equipment
---------
* 1 Claws: Pwr, Brawling
* L1 Teeth: Pwr, Brawling
* :item:ref:`L2 Vine Symbiote <Nemron.VineSymbiote>`

Abilities
---------
:Templates:
   * :node:ref:`Generic.Ability.Amplify` (Chask)

:Damage:
   * :node:ref:`Generic.Damage.Crushing` (Vines)
   * :node:ref:`Generic.Damage.Piercing` (Bite, Vines)
   * :node:ref:`Generic.Damage.Slashing` (Claws, Vines)

   *Note:* 3 or more damage with the vines, will apply the
   :node:ref:`Generic.Condition.Paralysis` condition, *at the end of the round*.

:Support:
   * :node:ref:`Generic.Ability.Cleanse` (Chask)
   * :node:ref:`Nemron.VineSymbiote.FlailingVines` (Vines)

:Boons:
   * :node:ref:`Generic.Boon.Alert` (Chask)
   * :node:ref:`Generic.Boon.Regeneration` (Chask)
   * :node:ref:`Generic.Boon.Immunity` (Chask)

----

.. include:: /content-footer.rst
