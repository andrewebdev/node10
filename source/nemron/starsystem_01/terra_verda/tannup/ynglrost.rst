.. -*- coding: utf-8 -*-

.. title:: City of Ynglrost

========
Ynglrost
========
.. topic:: Author(s)

   * Andre Engelbrecht

The city was built on top of 3 large mesas that rise up from the :doc:`index`.

Many such geological protrusions exist in the jungle and there is a lot of plant
life that is able to grow up on these spires and mesas.  Most of the other mesas
and protrusions are too small to support larger buildings.

The city planners decided to divide the city districts in such a way that all
the mesas can function somewhat independently, but each mesa should still have
an overarching theme.

Industry Mesa
=============
This is the largest of the three mesas, and planners decided to dedicate this
mesa to business industries.

The resources that the jungle provide consist of:

* **Industry** — Oils, gum, waxes, flavourings, and dyes are all found in rain
  forests. Rubber, is also produced, but is minimal because synthetic
  alternatives are far cheaper and more versatile.
* **Food** — Many grains and fruits grow in rain forests.
* **Medicine** — Certain plant and animal extracts serve as sources for chemicals
  used in medical research.
* **Other** — Symbiont Research and Development.

Commons Mesa
============
This mesa predominantly serves as the commercial hub for the city, providing the
following services:

* Government and regulatory head-offices.
* Shops and businesses that serves the general needs of the city.
* The largest residential district of the 3 mesas

Travel Mesa
===========
This Mesa is home to the Ynglrost Star Port. Large warehouses store goods
destined for shipment by medium-sized haulers.

This is also where people can rent jungle terrain vehicles for travel into the
jungle.

Jungle Conservation
===================
Officials take conservation of the jungle seriously. There is regulation for
every kind of industry in order to maintain balance and ensure that both jungle
and citizens are healthy and vibrant.

Toponymy
========
Historians agree that there are two origins for the toponymy of Ynglrost, “The
Jungle’s Rest” or “The Jungle’s Roost”.

It is believed the original settlers used both names because, this was one place
to escape and rest high up out of the harsh jungle in the valleys below. There
were also a large amount of bird species that used to nest up on the mesa,
which is what led to the second name “The Jungle’s Roost”.

Over many thousands of years and name iterations, the city just became known as,
*Ynglrost*.

.. include:: /content-footer.rst
