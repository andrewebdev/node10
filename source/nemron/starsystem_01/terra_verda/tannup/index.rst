.. -*- coding: utf-8 -*-

.. title:: Tannup Jungle

.. _nemron-tannup-jungle:

#############
Tannup Jungle
#############

:Author(s):

   * Andre Engelbrecht

The Tannup Jungle consist of lush jungle valleys, naturally carved over
thousands of years, from a large rock plateau.

The dense jungle and the numerous mesas that block your path, makes travel
difficult. You cannot, for example, travel in a straight line from one point to
another, unless you are flying (which is illegal).

.. todo::

   Discuss the areas of the jungle, ie.

   * the city
   * north of city for industry and tourism
   * West of city is protected, no access for anyone but conservation and
     research.
   * East eventually meets the ocean
   * South has access to limited tourism and scientific expeditions
   * Far southern region of the jungle consist of marshlands that eventually
     merge into the ocean.

   Also, talk about a note on illegal hunting of exotic animals, but also point
   out that most of the animals are illegally hunted because they are the only
   ones that can mature certain symbiotes.

Toponymy
========
When the jungle was first discovered locals and settlers named it, the
Entangled Uplands. Over time it became known as Tannup Jungle.

Jurisdiction
============
The Tannup Jungle is a protected area watched over by the
:doc:`/nemron/organisations/ysc`. Any activity in the jungle need to pass strict
regulations.

Northern parts of the jungle are open to industry, so long as they do everything
sustainably, while the southern parts of the jungle are only available for
scientific research and limited tourism.

Restricted Fly Zone
===================
Due to the conservation laws, low level flying over the jungle is not permitted,
except by emergency services such as police and search and rescue.

The only areas where flight is permitted, is over the Ynglrost City Mesas. This
is to allow transport in and out of the city.

Communication
=============
Due to the nature of the jungle terrain, long range communications do not work
well. Communication relays on top of some smaller mesas provide limited signal
to anyone in the jungle. Smaller villages in the jungle do have stronger
communication relays.

Chask Symbiote
==============
This symbiote is a microscopic organism found in rivers and streams within the
jungle. For more information on the Chask Symbiote and how it fits into the
world, see: :doc:`/nemron/biomancy`.

Places of Interest
==================
.. toctree::
   :maxdepth: 2

   ynglrost
   chaskriven
   glowing_caverns

Notable NPC’s
=============
.. toctree::
   :maxdepth: 2

   npcs

Dangers in the Jungle
=====================
.. toctree::
   :maxdepth: 1
   :caption: Tannup Bestiary

   bestiary
