.. -*- coding: utf-8 -*-

.. title:: Tannup NPC’s

########################
Notabale NPC’s in Tannup
########################
.. include:: /spoilerwarn.rst

.. topic:: Overview

   This is a list of notable and important NPC’s within the Tannup Region.

   :Author(s):

      * Andre Engelbrecht

.. _nemron-npc-chaskriven-security:

===================
Chaskriven Security
===================
.. topic:: Credits

   * Andre Engelbrecht

You can mix and match the nodes using the following guidelines:

**Attributes Nodes**

* 3 Attributes on rank 2
* 1 Attribute on rank 3
* All other attributes on Rank 1

Core Skills:
   All Chaskriven security will have basic training in the following skills (All
   on rank 1):

   Insight,
   FirstAid,
   UnarmedCombat,
   Marksmanship,
   JungleBiome,
   Evade

Of those skills, at least 3 of them will be on ``rank 2``.

After that you can give them any 2 other skills, which reflect on their hobbies
(if applicable). *One* of those two skills will be on ``rank 2``.

.. _nemron-npc-chaskriven-medical:

==================
Chaskriven Medical
==================
.. topic:: Credits

   * Andre Engelbrecht

You can mix and match the nodes using the following guidelines:

**Attributes Nodes**

* 3 Attributes on ``rank 2``
* 1 Attribute on ``rank 3``
* All other attributes on ``rank 1``

Core Skills:
   All Chaskriven security will have basic training in the following skills (All
   on ``rank 2``):

   Biology,
   FirstAid,
   Medicine,
   :node:ref:`Nemron.Skill.Technomancy`

Of those skills, at least 1 of them will be on ``rank 3``.

After that you can give them any 3 other skills, which reflect on their hobbies
(if applicable). *One* of those bonus skills will be on ``rank 2``.

.. _nemron-npc-noel:

===========
Noel Pastor
===========
.. topic:: Credits

   * Andre Engelbrecht

Old Noel Paster is one of the most important characters located in Chaskriven.
On the surface he comes across as old and almost-senile, speaking in riddles and
questions and rambling to himself on many occasions.

This is all an act. Noel is highly intelligent and actually much younger
than how he comes across. He only puts on an act of being old and somewhat
senile, so that everyone he encounters can drop their guard, and sometimes give
him valuable information.

He will always try to gather information, but he will do so through casual
conversation, ramblings, and pretending not to listen. He will try to sell
useless trinkets, to players, but always do so with the primary goal of making
conversation and gain information.

Noel Bio
========
As a younger man, *Noel Pastor* was an actor. He appeared in stage shows and
films and was a talented individual. While on a film shoot deep within the
jungle, Noel got lost and fell into the river. He woke up a day later in a small
camp.

A woman rescued him, gave him food and tended his wounds. Her name was “Amara”.
He couldn’t understand how or why she would be alone in such a treacherous
jungle. He didn’t have to wait long for the answer.

Predators later attacked Noel, but the animals froze in place the moment they
saw Amara. They whimpered and fled back into the depths of the jungle.

With Amara’s help, Noel reached the search parties looking for him. But no-one
believed him about the Amara.

Tannup Wardens
--------------
Noel grew obsessed with his experience and, unable to focus on work, his acting
career ended. He obsessively joined any jungle expedition that he could to keep
going back into the jungle with the goal of finding Amara again.

He eventually did. On one of the expeditions, the crew was ambushed by Rachni.
In the chaos Noel fled deeper into the jungle where, he fell down into a burrow.

   “You are the clumsiest individual I’ve ever met…”, were the first words he
   heard as he woke. It was the Amara.

After long talks he convinced her that he doesn’t care for the acting world
any more and, even though his original quest was to find her, he’s grown fond of
the jungle, and would rather stay close to it.

Amara told Noel that she lives in the jungle and helps protect it. Cynically
Noel mentioned that one person cannot possibly protect the jungle alone. This is
the point where he discovered that Amara belongs to a secret group called the
“Tannup Wardens”.

Becoming a Warden
-----------------
Noel eventually joined the wardens and thus found his calling in life. Over the
years, he gained respect within the wardens.

On his 40th, Noel went back to Chaskriven Village, to become the eyes and ears
of the wardens within the village.

Noel, decided to use his experience in acting to pretend that he somehow
survived in the jungle for 13 years, and made it back to Chaskriven. But he
would also pretend to not remember much of his acting career, and the jungle
drove him to be mentally unstable.

This would be his cover story.

Noel’s Curiosities
------------------
Noel’s story of survival became popular news. After the media attention died
down, people felt sorry for his “tragic” story, and he was given a small house
in Chaskriven.

As part of his cover, he started a shop that sells all manner of things from the
jungle. Things like hand-made necklaces, various figurines carved by locals,
soaps made from some tree saps and plants in the jungle etc.

Some of the trinkets, are actually secret messages to members of the Wardens.

Stats
=====
.. actor:def:: Noel Pastor
   :uid: Nemron.Tannup.NoelPastor
   :frame: :ref:`nemron-races-solborn-human`
   :body: 1,3,2
   :mind: 3,4,3
   :spirit: 3,1,1

Attributes:
   | Power [1], Agility [3], Constitution [2]
   | Intellect [3], Awareness [4], Willpower [3]
   | Control [3], Sense [1], Focus [1]

Skills:
   :node:ref:`Charming (trait) [3] <Generic.Boon.Charming>`,
   :node:ref:`Insightful (trait) [3] <Generic.Boon.Talented>`,
   Drama (4),
   Crafting (2),
   Survival (1),
   JungleBiome (3),
   Foraging (2),
   Investigation (2),
   Insight (4),
   Persuasion (2),
   Deception (2),
   Stealth (2)

Chask Symbiote [3]:
   :node:ref:`Generic.Ability.Amplify`,
   :node:ref:`Generic.Ability.Cleanse` (self only),
   :node:ref:`Generic.Boon.Alert`,
   :node:ref:`Generic.Boon.Regeneration`,
   :node:ref:`Generic.Boon.Immunity`

Other Symbiotes [2]:
   :node:ref:`Generic.Boon.DarkSight` (Ocular Symbiote),
   :node:ref:`Generic.Ability.Detection` (Oral Symbiote, by taste on the tongue)

Equipment
---------
.. item:def:: Jungle Totem
   :uid: Nemron.JungleTotem

   **Level:** 3

   This is a Technomancy interface with 2 codex slots. The idol is a hand-carved
   object of a mythological forest spirit. The 2 codex slots are hidden inside
   the idol. Only Noel knows how to open the idol.

   All this means that the to the outside observer the idol looks like nothing
   more than a traditional carving.

   Codex Skills:
      :node:ref:`Detect Life <Generic.Recipe.DetectLife>`,
      :node:ref:`Heal Wounds <Generic.Recipe.Regenerate>`,
      :node:ref:`Cure Condition <Generic.Recipe.CureCondition>`

----

.. _nemron-npc-john-rocksen:

============
John Rocksen
============
John is the police chief in Chaskriven village. This ursine hybrid’s size and
power is easy to miss behind he’s cheerful personality and big smile.

He’s got a large diagonal scar running above the left eye, which cause  him to
have occasional, involuntary twitches raising the eyebrow every now and then.

This is due to nerve damage received when he once wrestled and evicted a
:ref:`tannup-vinebear` from the village.

Stats
=====
.. actor:def:: John Rocksen
   :uid: Nemron.JohnRocksen
   :frame: :ref:`Ursine Hybrid <nemron-races-solborn-hybrid>`
   :body: 4,2,3
   :mind: 2,4,2
   :spirit: 2,2,1

Skills:
   :node:ref:`Powerful (trait) [3] <Generic.Boon.Talented>`,
   :node:ref:`Endurance (trait) [2] <Generic.Boon.Endurance>`,
   :node:ref:`Generic.Ability.Shout` (3),
   Inspiration (2),
   Insight (4),
   Brawling (4),
   Evade (2),
   Marksmanship (3),
   Survival (2),
   JungleBiome (2),
   Carpentry (2)

Equipment
---------
* Credits: 10,311 Cr
* :item:ref:`Generic.Weapon.HuntingRifle`
* :item:ref:`Generic.Weapon.CombatPistol`
* :item:ref:`Generic.Weapon.Dagger`
* :item:ref:`Nemron.Medkit`
* General survival kit

----

.. _nemron-npc-kesser-garcia:

=============
Kesser Garcia
=============
.. topic:: Credits

   * Andre Engelbrecht

Mayor of :doc:`/nemron/starsystem_01/terra_verda/tannup/chaskriven`. He’s been
running the town for years and outwardly, leads a generally comfortable life.

His wife died to animal attack before they had any children, which caused Kesser
a lot of distress, resentment and sometimes even hatred towards creatures in the
jungle.

Bodyguards
==========
These are two mercenaries that Kesser pays for protection, but also to assist
with tasks or activities that could lead to exposure.

.. todo:: Add stats for the two guards

Smuggling
=========
In secret he and his gang heads a smuggling ring that capture and smuggle
exotic, protected animals out of Tannup. His influence extends deep into the
city and he even has some connections within the
:doc:`/nemron/organisations/ysc`.

He pretends to assist the YSC with information on possible smuggling activities,
but these are red herrings most of the time. Every now and then, just to try and
keep up the appearance that he can be trusted, he will leak some information for
legitimate smuggling that he organised.

No-one, other than his two body guards know of his activities.

Secret Ledger
=============
He keeps a :doc:`/tutorial/handouts/kesser_secret_ledger` secret ledger of his
sales and purchases in a secret, hidden office in the basement of his home.

Kesser Stats
============
.. actor:def:: Kesser Garcia
   :uid: Nemron.KesserGarcia
   :frame: :ref:`Human <nemron-races-solborn-human>`
   :body: 1,1,1
   :mind: 4,3,1
   :spirit: 1,1,2

Skills:
   Deception (4),
   :node:ref:`Talented Deceiver (trait) [3] <Generic.Boon.Talented>`,
   Law (3),
   Management (3),
   Insight (3),
   Tactics (3),
   Music (2),
   Investigation (2),
   Literature (2),
   Marksmanship (1)

----

.. _nemron-npc-morag-dunn:

==========
Morag Dunn
==========
.. topic:: Credits

   * Andre Engelbrecht

Runs The Mossy Mug pub in
:doc:`/nemron/starsystem_01/terra_verda/tannup/chaskriven`.

Morag is fond of the mayor, :ref:`nemron-npc-kesser-garcia`, and is regularly
tasked to procure a rare alcohol for him. He usually gets 2 bottles, and he and
the mayor will then enjoy a bit from one bottle, while the other goes into
storage in the mayor’s house.
