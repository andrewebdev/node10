.. -*- coding: utf-8 -*-

:orphan:

.. title:: GECH

====
GECH
====
.. include:: /spoilerwarn.rst

The GECH are good stalkers and can be dangerous. Their primary drive is to find
nourishment and spread the infection to new hosts.

The GECH cannot infect a species of a different frame or species than itself.
This is because the parasite will have a blueprint of the original host species,
and passes down to each larva. So a human GECH Host can only infect another
human, and a Hybrid can only infect other Hybrids.

Because of this the GECH will target its host species and bite the victim, to
infect them with the larvae. The GECH may on occasion capture and entomb the
victim in some way to keep it safe while the parasite matures, almost like a
mother caring after her young. It takes around ``4 weeks`` for a GECH parasite
to mature.

GECH will not usually attack another species, except for defence or nourishment,
and for this reason they will normally just hide, sneak and observe.

Once the GECH parasite has fully matured, the hosts have no control over their
own body, speech or actions. The GEC-CE parasite has full control. The Host
may still be awake and experience everything that they are doing. To the
host this would feel like a nightmare where they cannot act or do anything, only
passively observe.

Cured Hosts and Recurring Nightmares
====================================
If the host is ever cured, they will only remember fragments of the things
they've done. The recovering host will need to make willpower resistance rolls
every sleep cycle, at a difficulty of ``2`` for *every 5 victims* they attacked
while they were GECH.

Failing the roll the characters will have nightmares and suffer
``1 Mental Damage``.

Infecting characters with cybernetic augments
=============================================
A particularly nasty consequence of the GECH parasite is how aggressively it
rewrites the nervous system. It's especially bad for characters that have
cybernetic and Technomancy augmentations.

As the GECH parasite matures, any augmentations will slowly stop working,
starting with augments on the extremities (The furthest away from the spinal
column).

Once the parasite fully matures, no augments will be functional. A cybernetic arm
for example, will be useless and will just dangle freely from the GECH.

GECH Origins
============
Play or read through the :doc:`/tutorial/index` Module to learn about the GECH
origins.

GECH Stats and Abilities
========================
Since the GECH parasite was originally generically engineered from the Chask
Symbiote, it will also give the infected individual access to biomancy. All
other biomancy based symbiotes and parasites will also work for GECH.

.. item:def:: Gech Parasite
   :uid: Nemron.Gech

   **Level:** 1

   This is the Gech parasite that was originally a genetically modified version
   of the Chask symbiote. Since it’s based on the Chask symbiote, it may also
   function as a core requirement for having any other biomancy
   symbiotes (or parasites).

   Templates:
      :node:ref:`Generic.Ability.Amplify` allows you to efficiently keep some
      biomancy effects active for longer periods.

   Conditions:
      :node:ref:`Nemron.Gech.Infection` (with a successful “bite”).

   Boons:
      :node:ref:`Generic.Boon.Alert`,
      :node:ref:`Generic.Boon.Regeneration`,
      :node:ref:`Generic.Boon.Immunity`

.. node:def:: GECH Infection
   :uid: Nemron.Gech.Infection
   :type: E

   Resistance Pool:
      ``Control``
   Growth:
      Once infected this condition rank will increase by ``1 rank per week``.

   Decay:
      This condition never decays, unless the host received the special
      treatment protocol.
      Once treated, the individual will regain control over their own mind and
      body and this condition will start to decay by ``1 rank per week``.

   Individual GECH hosts don’t ever lose full control. They are in a constant
   wrestling match trying to gain control over their own mind and body.

   Whenever the host does anything that would go against the Gech survival
   instincts or wishes, then both the parasite and host will fight for control:

   #. Host makes a **Willpower Resistance roll** vs. the
      :ref:`fixed-medium <fixed-med-max>` of this condition.
   #. Failing the roll, the host will lose control over their **body and mind**
      for 1 hour.
   #. If this happen when this condition is **Rank 4**, the GECH will be in full
      control.

   If at any point the host rolled **Good Drama** on their **Willpower
   Resistance roll**, then they will remain in full control of the parasite, and
   won’t ever need to make another resistance roll.

.. include:: /content-footer.rst
