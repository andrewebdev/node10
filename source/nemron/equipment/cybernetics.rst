.. -*- coding: utf-8 -*-

.. title:: Cybernetics

===========
Cybernetics
===========
.. topic:: Overview

   This section covers the various cybernetic augments that individuals can get
   within the Nemron Universe. Cybernetic augments all need to be connected to
   the nervous system and as such *cannot* be used by anyone that has the Chask
   symbiote.

Cybernetic Energy Use
=====================
Cybernetic augments need energy to operate. Smaller augments can use energy from
the character energy pool directly. Larger augments like limbs requires more
energy, and may include their own energy source like a battery.

Sharing Energy
--------------
Cybernetic augments can share *some* of its energy with the operator and
visa-versa. There is a limit to how much energy can be shared this way.

The maximum energy transferred *per round*, is equal to the *Focus*
attribute.

Requirements To Use
===================
Does not work if you have biomancy symbiotes.

Cybertek Components
===================
.. todo::

   Expand on the Cyberteck company a little bit. Maybe create their own document
   and move the items below into that document.

.. Cybertek is the apple of the cybernetics world, in that the different
   cybernetic limbs can only share energy with other Cybertek components.
   They cannot share energy with third party components.

.. does not have it's own energy source, need to use energy from other
   cybernetics, or from the charcter energy pool.

Cybertek Equipment
------------------
.. item:def:: Cybertek Hub
   :uid: Nemron.Cybertek.Hub

   | **RRP:** 10,000
   | **Battery Capacity:** 20 Energy

   This is the core energy hub for all Cybertek components. This cybernetic
   implant is usually implanted within the chest cavity, where it’s protected by
   the rib-cage.

   Template(s):
      :node:ref:`Generic.Ability.Amplify`

   Boon(s):
      Roll ``Focus › Cybertek Hub » 1 AP, 1 Energy`` to gain
      :node:ref:`stacks of fury <Generic.Boon.Fury>` equal to the amount rolled.

.. item:def:: Cybertek Skirmish
   :uid: Nemron.Cybertek.Skirmish

   | **Level:** 1
   | **RRP:** 120,000

   A single cybernetic arm, optimized for entry level private security
   services. During standard use, the arm provides the same sensitivity and
   feedback to the user, as a normal biological arm would.

   The individual arm features may be activated however to get extra effects.

   Features:
      Multi purpose IO socket to update the cybernetic software, or recharge the
      :item:ref:`Nemron.Cybertek.Hub` via the *gateway*.

   Boon(s):
      Roll ``Control › Cybertek Skirmish » 1 AP, 2 Energy`` to empower your
      arms.

      Gain :node:ref:`empower stacks <Generic.Boon.Empowered>` equal to the
      amount rolled.

   Condition(s):
      Roll ``Control › Cybertek Skirmish » 1 AP, 2 Energy``. Unleash a static
      discharge to apply :node:ref:`stunned stacks <Generic.Condition.Stunned>`
      equal to the amount rolled.

.. item:def:: Cybertek Optic One
   :uid: Nemron.Cybertek.OpticOne

   | **Level:** 1
   | **RRP:** 50,000
   | **Attribute:** Sense

   Boons:
      Roll ``Sense › Optic One » 1 AP, 2 Energy`` to get boon stacks equal to
      the amount rolled. You may only have one of the following boons active at
      any one time.

      * :node:ref:`Generic.Boon.DarkSight`
      * :node:ref:`Generic.Boon.Deadeye`
      * :node:ref:`Generic.Boon.Farsight`

   Cybertek developed this ocular implant to predominantly aid in scouting and
   surveying.


.. include:: /content-footer.rst
