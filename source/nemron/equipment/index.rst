.. -*- coding: utf-8 -*-

.. title:: Equipment

=========
Equipment
=========

.. toctree::
   :maxdepth: 1
   :caption: Equipment

   ./armor
   ./weapons
   ./drones
   ./cybernetics
   ./biomancy
   ./misc

.. include:: /content-footer.rst
