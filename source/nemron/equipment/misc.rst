.. -*- coding: utf-8 -*-

.. title:: Miscellaneous Equipment

=======================
Miscellaneous Equipment
=======================

Medical
=======
.. item:def:: Healing Gel
   :uid: Nemron.HealingGel

   | **Level:** 1
   | **RRP:** 1,000

   A special gel that contain chemical compounds and organisms, which will aid
   in healing open wounds and burns. The gel disinfect the wound, while also
   assisting the body to heal the wounded area much faster.

   Quantity:
      This comes as a small tub filled with *1000 cubic cm* of the gel.

   Boons:
      The gel will give the patient
      :node:ref:`1 stack of regeneration <Generic.Boon.Regeneration>` for
      ``2 hours``. After 2 hours, the regeneration will decay as normal, unless
      the gel is re-applied.

.. item:def:: Healing Patches
   :uid: Nemron.HealingPatch

   | **Level:** 1
   | **RRP:** 100

   A pack of 5 square patches. Stick a patch over an open wound, to disinfect
   and accelerate healing.

   Each patch contains :item:ref:`Nemron.HealingGel`.

   The gel will slowly be released into the wound and surrounding area, helping
   to disinfect it, as well helping to heal it faster.

   The entire patch will slowly be absorbed by the body as the chemicals and
   organism fuse with and heal the body.

   Size:
      10cm x 10cm x 0.5cm (50 cubic cm)

   Multi Pack:
      The 100 Cr price is for a pack of **5 patches**.

   Boons:
      The patch will give the patient
      :node:ref:`1 stack of regeneration <Generic.Boon.Regeneration>` for
      ``2 hours``. After 2 hours, the regeneration will decay as normal, unless
      the gel is re-applied.

.. item:def:: General Antidote
   :uid: Nemron.Antidote

   | **Level:** 1
   | **RRP:** 50

   A special antidote that was initially developed to cure infections, poison
   and venom. This is a general cure all medication and can treat fever and
   related conditions. It’s fast acting and have become standard in every
   emergency services kit.

   Using it will :node:ref:`Generic.Ability.Cleanse` the patient, while also
   granting :node:ref:`1 rank of immunity <Generic.Boon.Immunity>`.

.. item:def:: Diagnostic Wand
   :uid: Nemron.DiagnosticWand

   | **Level:** 1
   | **RRP:** 10
   | **Requires:** Biology, Medicine or First-Aid

   These diagnostic probes were originally patented and created by Arcgen. They
   have been specifically designed to do quick, in-the-field diagnostics of
   wounds. They can also detect poisons, toxins and infections.

   They are single use items to reduce the risk spreading infections.

   Removing the cap from the end of the probe will activate it. There is a 5
   minute window to probe the area you wish to test. The probe will connect to
   any valid technomancy interface to report the results.

   Once the 5 minutes are up the probe will release nannites, which will break
   apart the molecular bonds of the probe, and it will dissolve away in the air.

   Support:
      :node:ref:`Generic.Ability.Detection`

.. item:def:: Medical Kit
   :uid: Nemron.Medkit

   | **Level:** 1
   | **RRP:** 3,215

   Contains:
      #. :item:ref:`20 x Healing Patches <Nemron.HealingPatch>`
      #. :item:ref:`20 x General Antidote <Nemron.Antidote>`
      #. :item:ref:`20 x Diagnostic Sticks <Nemron.DiagnosticWand>`
      #. 5 x Medical Gloves
      #. Scissors

Engineering Related
===================
.. item:def:: Systems ToolKit
   :uid: Nemron.SystemsToolkit

   | **Level:** 1
   | **RRP:** 750

   This kit is required for doing systems diagnostics and software security
   penetration etc.

   .. todo:: Expand a bit more on usage of the kit etc.

Outdoor Activities
==================
.. item:def:: Auto-Tent
   :uid: Nemron.AutoTent

   | **Level:** 1
   | **RRP:** 200
   | **Armor Level:** 3 (Will absorb up to 3 points of damage)

   A 6 man tent packed into a flat box, with a handle like a suitcase.

   The tent will unfold and assemble itself in a matter of seconds. It’s made of
   composite materials that proves good protection from the elements and certain
   predators. This tent can sleep up to 6 people.

.. item:def:: Micro-Reactor
   :uid: Nemron.MicroReactor

   | **Level:** 1
   | **RRP:** 10,000
   | **Weight:** 20 Kg

   A small and compact fusion reactor that can generate quite a bit of energy
   once it's been supplied by a bit of organic matter. The reactor has 2 power
   outlets. And is about the size of a knee-high barrel.

   This can supply steady 2 Energy Points every 10 minutes. Once filled with
   organic matter it will be able to provide up to 100 pips of energy.

.. item:def:: Camp Light
   :uid: Nemron.CampLight

   | **Level:** 0
   | **RRP:** 70
   | **Max Energy:** 10
   | **Energy:** 1 Energy per hour.

   Has its own battery that will last for 10 hours.

   Uses a common connector to get power from a :item:ref:`Nemron.MicroReactor`.
   Recharges in 10 minutes when connected to the reactor.

.. item:def:: Camp Stove
   :uid: Nemron.CampStove

   | **Level:** 1
   | **RRP:** 40

   A small powered stove.

   Uses a common connector to get power from a :item:ref:`Nemron.MicroReactor`.

.. item:def:: Water Purifier
   :uid: Nemron.WaterPurifier

   | **Level:** 1
   | **RRP:** 500

   A device that is about the size of a human hand. One purifier can purify
   *5 litres of water per minute*.

   It works by using nanotechnology to scan for and remove dangerous organisms
   or heavy metals from the water.

   Uses a common connector to get power from a :item:ref:`Nemron.MicroReactor`.

.. include:: /content-footer.rst
