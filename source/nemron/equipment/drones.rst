.. -*- coding: utf-8 -*-

.. title:: Drones

######
Drones
######
.. todo:: *Using a drone as proxy for Technomancy*

   Talk about the :node:ref:`Nemron.Techno.Conduit` node, and how the
   technomancer can channel their skills through the drone.

Recon Drone
===========
.. item:def:: Recon Drone
   :uid: Nemron.ReconDrone

   | **Level:** 0
   | **RRP:** 5,000

   A small drone that fits in the hand, meant for scouting and recognisance. The
   4 propellers on this drone is silent.

   The difficulty to hear this drone if it’s within 5 meters is ``DC5``.

   Difficulty goes up by ``1 point`` for every *1 meter* over the initial 5
   meters.

   * Drone has a light that can toggle on or off for illumination.
   * The operator can view the drone camera feed using **Sense**.

Seeker Drone
============
.. item:def:: Seeker Drone
   :uid: Nemron.SeekerDrone

   | **Level:** 1
   | **RRP:** 250,000

   .. todo:: Add information on seeker drones

   Seeker Drones have the following abilities:

   Condition:
      :node:ref:`Stun Bolt <Generic.Condition.Stunned>`,
      :node:ref:`Daze Bolt <Generic.Condition.Dazed>`,
      :node:ref:`Tracker <Generic.Condition.Marked>` (see below).

   Tracker:
      Fires a small tracker that sticks to and
      :node:ref:`Marks <Generic.Condition.Marked>` the target. It deals no
      damage and is hard to detect or feel.

      Difficulty to feel the impact is the :ref:`fixed-maximum <fixed-med-max>`
      of the drone level.

      The tracker emit a signal that reach up to ``2km``.

.. _nemron-drone-mule:

Mule Drone
==========
Standing 1.4 meters tall, this is a drone that can carry a large weight through
difficult terrain. It has 8 legs in total but it normally folds them into paired
groups, so that it only uses 4 legs in easy terrain. It will start using all 8
legs when the terrain gets harder to traverse.

The mule has a “follow-mode” where it will make use of AI to always follow a
specific individual. It normally only takes commands from the operator, but the
owner can give temporary command to someone else.

The AI is smart enough to follow simple instructions and commands.

Using it’s array of sensors, the drone can determine the best way to traverse
difficult terrain. In some cases it can even detect rotten wood or surfaces that
may break under its weight.

The drone also has a voicebox that it can use to communicate with the operator
or anyone with permission.

.. item:def:: Mule Frame
   :uid: Nemron.MuleFrame

   | **Level:** 3
   | **RRP:** 50,000
   | **Abilities:** :node:ref:`Nemron.Techno.Conduit`

   Attributes:
      | Power [3], Agility [1], Constitution [1]
      | Intellect [1], Awareness [0], Willpower [0]
      | Control [1], Sense [2], Focus [1]

   Swarm Capsules:
      The drone has space for
      :item:ref:`5 Swarm capsules <Nemron.Techno.SwarmCapsule>`.

      If the operator has :node:ref:`Nemron.Skill.Technomancy`, they can use the
      drone as a conduit for technomancy abilities, provided it has swarm
      capsules loaded.

----

.. Author(s)

   * Andre Engelbrecht

.. include:: /content-footer.rst
