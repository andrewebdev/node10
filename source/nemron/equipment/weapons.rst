.. -*- coding: utf-8 -*-

.. title:: Weapons

#######
Weapons
#######
.. note::

   We will eventually expand this section with more weapon types specific to the
   Nemron universe.

   In the meantime just use the generic weapons provided:

   **Melee Weapons**

   * :item:ref:`Generic.Weapon.Dagger`
   * :item:ref:`Generic.Weapon.Machete`

   **Ranged Weapons**

   * :item:ref:`Generic.Weapon.BasicPistol`
   * :item:ref:`Generic.Weapon.CombatPistol`
   * :item:ref:`Generic.Weapon.HuntingRifle`
   * :item:ref:`Generic.Weapon.AssaultRifle`


.. include:: /content-footer.rst
