.. -*- coding: utf-8 -*-

.. title:: Biomancy Modules

################
Biomancy Modules
################

==============
Chask Symbiote
==============
This symbiote is a microscopic organism that is rarely found in rivers and
streams within the :doc:`/nemron/starsystem_01/terra_verda/tannup/index`.

People will collect Chask from the river for use in research, and medical
procedures.

Player Characters with knowledge in Biology can roll to determine how much they
know about it. Any other field of biological study may also apply, like
specialisation in micro-biology etc.

:Difficulty 3: Function and Purpose
:Difficulty 6: Discovery, History
:Difficulty 7: Life cycle, Military Interests

.. item:def:: Chask
   :uid: Nemron.Chask

   This is the core symbiote that every character will need if they wish to use
   biomancy. It binds and grows onto the entire nervous system, which allows
   other symbiotes (or parasites) to interface with the user.

   *Note: All intentions are only usable on the host.*

   Traits
      Being host to the Chask Symbiote will automatically give the player the
      trait :node:ref:`Generic.Boon.Resolve`.

   Abilities
      :node:ref:`Generic.Ability.Amplify` allows you to efficiently keep some
      biomancy effects active for longer periods.

   Support
      :node:ref:`Generic.Ability.Cleanse`

   Boons
      :node:ref:`Generic.Boon.Alert`,
      :node:ref:`Generic.Boon.Regeneration`,
      :node:ref:`Generic.Boon.Immunity`

Function and Purpose
====================
Chask symbiote binds to the nervous system and enhances the senses for the
character with it.

It is popular to use Chask for certain medical procedures. Once fused to the
nervous system the symbiote will repair it from damage. It is often used in to
repair spinal cord injuries etc.

The symbiote also cure diseases related to nervous system damage.

Hosting other symbiotes
-----------------------
Another feature of the Chask symbiote is, because of the shared biological
evolution, that it allows the individual to also host other symbiotes.

These secondary symbiotes all communicate and work with the host, through the
Chask symbiote.

Discovery
=========
To date, Tannup Jungle is the only known location where this symbiote lives.
For this reason the jungle is a protected area, enforced by strict conservation
laws.

.. todo::

   Change this... The chask symbiote can be found in other areas of the planet,
   but is almost extinct in those areas because it was never protected. Tannup
   Jungle is the last place where the symbiote actually thrives.

History
=======
The name derived from an ancient civilization, the Inca, that once lived on the
planet Earth in the Sol system.

The original Inca name, “Chasqui”, referred to people that used to carry
messages on common roads and paths, similar to how the nervous system is a
network of message paths.

Life cycle
==========
When a mammal drinks water with a Chask larvae within it will then find the
nearest nerve inside the digestive system and attach itself to it.

Over the next three months, the symbiote will grow and develop further, thinly
spreading and merging with the entire nervous system. The Chask will get
nutrients and resources from the host body, as well as a safe environment to
grow. The host also benefits by having their natural senses enhanced, increasing
chances of survival.

The Chask will, under the right circumstances re-produce by releasing hundreds
tiny microscopic eggs into the bladders of the creatures. Larvae will hatch when
the eggs get into contact with the waters in the jungle, where the cycle starts
again.

There hasn’t been any success in producing them in controlled environments,
because;

#. There seems to be an unknown condition for a fully mature Chask to actually
   produce eggs, and;
#. Even when an egg is exposed, it seems that it has to be the water from this
   jungle, and even then the larvae don’t always hatch.

No one knows why this is the case yet, but research is ongoing to determine
why.

Military Interests
==================
There is minimal military interests, but is generally seen as an “interesting
alternative”, since the military applications of cybernetic and Technomancy are
better.

Because of the Chask life-cycle, it’s not possible to mass produce chask, making
most military organisations and research just avoid biomancy all-together.

.. _biomancy-other-symbiotes:

===============
Other Symbiotes
===============
.. todo::

   Need to expand on where the symbiotes are located, their names, applications
   etc. Right now I’m adding them ad-hoc as required in the games I run.

   Add some more abilities to our existing symbiotes, get some inspiration from
   the animal kingdom.

Adrenal Symbiote (Name TBD)
===========================
.. item:def:: Adrenal Symbiote
   :uid: Nemron.Symbiote.Adrenal

   :Requires: :item:ref:`Level 1 Chask Symbiote <Nemron.Chask>`

   This symbiote assists with regulating adrenaline in the body. It gives the
   host complete control over when to flood the system with adrenaline, or to
   remove excess adrenaline from the body.

   :Boons:
      * :node:ref:`Generic.Boon.Ferocity`
      * :node:ref:`Generic.Boon.Fury`
      * :node:ref:`Generic.Boon.Resilience`

Ocular Symbiote (Name TBD)
==========================
A symbiote that embeds itself within the eyes of the host, enhancing their sight
in various ways.

.. item:def:: Ocular Symbiote
   :uid: Nemron.OcularSymbiote

   :Requires: :item:ref:`Level 1 Chask Symbiote <Nemron.Chask>`

   ⚠ Notes TDB...

   Traits
      :node:ref:`Generic.Boon.DarkSight`

Oral Symbiote (Name TBD)
=========================
A symbiote that lives inside the mouth of the host, usually under the tongue.
Oral symbiotes only mature in the mouth of a Rachni. Because of this, poachers
will seek out Rachni purely because they grow this symbiote.

.. item:def:: Oral Symbiote
   :uid: Nemron.OralSymbiote

   :Requires: :item:ref:`Level 1 Chask Symbiote <Nemron.Chask>`
   :Template(s):
      :node:ref:`Touch (1 Energy) <Generic.Ability.Touch>`, by licking or
      touching something to the tongue.

      :node:ref:`Cone Shape (3 Energy) <Generic.Ability.Shape>`, by squeezing
      the spit glands and spraying vapour in front of you.

   :Damage:
      * :node:ref:`Generic.Damage.Corrosive`

   :Support:
      * :node:ref:`Generic.Ability.Detection` via taste

   :Conditions:
      * :node:ref:`Generic.Condition.Corrosion`
      * :node:ref:`Blind <Generic.Condition.Dull>`

Vine Symbiote (Name TBD)
========================
.. item:def:: Vine Symbiote
   :uid: Nemron.VineSymbiote

   :Range: Up to 3 Meters

   A symbiote that can attach anywhere on the outer skin of a Chask host, so
   long as it’s not too close to another symbiote.

   This symbiote grows up to 3 vines that can be independently controlled by the
   host. They can also do damage by whipping out towards an enemy.

   :Damage:
      *Note:* If you do 3 or more damage with any of the damage intentions
      below, you automatically apply the :node:ref:`Generic.Condition.Paralysis`
      condition, *at the end of the round*.

      * :node:ref:`Generic.Damage.Crushing` — By wrapping the vines around the
        target and squeezing.
      * :node:ref:`Generic.Damage.Piercing` — The thorns of the vines, or by
        using the sharp tip of the vine.
      * :node:ref:`Generic.Damage.Slashing`

   :Support:
      * :node:ref:`Generic.Ability.Disarm`
      * :node:ref:`Nemron.VineSymbiote.FlailingVines`

Vine Symbiote Skills
--------------------
.. node:def:: Flailing Vines
   :uid: Nemron.VineSymbiote.FlailingVines
   :type: P

   **Attribute(s):** Control

   In this stance, the vines whip, flail and move continuously, threatening to
   hit anyone within 3 meters of the host. When entering the stance, roll
   ``Control › Vine Symbiote » 1 AP``

   The resulting roll count as an automatic attack against anyone coming within
   3 meters of the host.

   The stance will end the moment you use your vines for any other action.

   .. warning::

      While in this stance you also gain :node:ref:`1 stack of Slow
      <Generic.Condition.Slow>`. This *does not decay as normal*. Remove the
      condition the moment you exit the stance.

.. include:: /content-footer.rst
