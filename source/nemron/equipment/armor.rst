.. -*- coding: utf-8 -*-

.. title:: Armour

######
Armour
######

.. note::

   We will eventually expand this section with more armour types specific to the
   Nemron universe.

   In the meantime for amour, just refer to the generic armour provided:

   * :item:ref:`Generic.Armour.TacticalVest`
   * :item:ref:`Generic.Armour.Light`
   * :item:ref:`Generic.Armour.Medium`
   * :item:ref:`Generic.Armour.Heavy`

========================
Shields & Shield Bracers
========================
.. item:def:: Shield Bracer
   :uid: Nemron.ShieldBracer

   | **Level:** 1
   | **RRP:** 5,000
   | **Attribute(s):** Agility
   | **Damage:** :node:ref:`Generic.Damage.Kinetic`
   | **Support:**
     :node:ref:`Generic.Ability.Disarm`,

   A special bracer that can un-fold into a small personal shield, and to
   provide the wearer with an additional physical barrier against attacks.

   *The folding plates use clever origami to collapse back into a bracer around
   the wrist.*

.. include:: /content-footer.rst
