.. -*- coding: utf-8 -*-

:orphan:

.. title:: Patreon Content

##################
Patreon Scratchpad
##################

==========
About Page
==========
I’m in the process of developing a generic tabletop game system that can be
translated to many genres. The system is open and under a creative commons
licence, so I encourage others to use, expand, and hopefully contribute any
enhancements.

The system focuses on fair roll outcomes, deadly combat, and a sprinkle of drama
with the use of a drama dice.

To make sure I don’t step on any intellectual property, I’ve built my own
science fiction world for use of this system to begin with.

If there is enough interest, I may post examples of how other types of worlds
or genres can make use of this system.


============
Welcome Post
============
