Players find themselves trapped in a sci-fi nightmare, drifting in space. All
manner of terrifying creatures are hunting them. With only their whit,
experience and determination, they need to survive and escape.

:Document: :doc:`testing-scenario2`

:Setting: Science Fiction Horror

:Players: 3-4

:Experience: Intermediate

:Testing Goals:
   Will test the sanity and torment rules, sprinkled with investigation,
   survival and a tiny bit of combat.

:Feedback:
   See :ref:`the contributions section <contribute>` if you have any
   feedback, questions or ideas.
