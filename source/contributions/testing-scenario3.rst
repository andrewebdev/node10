.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: Scenario 3 (Modern Fantasy) — |nem|

.. _testing-scenario3:

###########################
Scenario 3 — Modern Fantasy
###########################
.. include:: testing-scenario3-overview.rst

.. note::

   This module is currently being written in a different format. We most likely
   provide it as a PDF for download.

Geologist

.. include:: /global-message.rst
