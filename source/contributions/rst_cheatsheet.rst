.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. |longtext| replace:: this is a long text to include.

.. |logo| image:: /branding/nemron-rpg-logo.png
   :width: 65pt

.. title:: ReStructuredText CheetSheet

.. include:: /global-message.rst

:orphan:

.. _rst-tutorial:

##############################################
Restructured Text (reST) and Sphinx CheatSheet
##############################################

.. topic:: Overview

   This page targets editors and contributors that are actively working on the
   documentation. If this is not you, then you can ignore this section entirely
   (Although this does give an insight into how we work internally on the
   system).

   This page describes some of the RST and Sphinx syntax. It is based on
   `Docutils <http://docutils.sourceforge.net/rst.html>`_ and more generally
   software documentation written with Sphinx.

   This is not an exhaustive description but it should allow you to start and
   contribute to the system documentation.

   :Date: |today|
   :Author: **Andre Engelbrecht**


============
Introduction
============
The reStructuredText (RST) syntax provides an easy-to-read,
what-you-see-is-what-you-get plaintext markup syntax and parser system. However,
you need to be precise and stick to some strict rules:

   * RST syntax is sensitive to indentation!
   * RST requires blank lines between paragraphs

This entire document is written with the RST syntax. In the page footer, you
should find a link **show source**, which shows the RST source code.

===============
Text Formatting
===============

Inline markup and special characters (e.g., bold, italic, verbatim)
===================================================================
There are a few special characters used to format text. The special character
``*`` is used to defined bold and italic text as shown in the table below. The
backquote character ````` is another special character used to create links to
internal or external web pages as you will see in section
`Internal and External Links`_.

=========== ================================== ==============================
usage          syntax                           HTML rendering
=========== ================================== ==============================
italic      `*italic*`                         *italic*
bold        `**bold**`                         **bold**
link        ```Nemron <www.nemron.org>`_``     `Nemron <www.nemron.org>`_
verbatim    ````*````                          ``*``
=========== ================================== ==============================

The double backquote is used to enter in verbatim mode, which can be used as the
escaping character. There are some restrictions about the ``*`` and ``````
syntax. They

   * cannot not be nested,
   * content may not start or end with whitespace: ``* text*`` is wrong,
   * it must be separated from surrounding text by non-word characters like a
     space.

The use of backslash is a work around to second previous restrictions about
whitespaces in the following case:

   * ``this is a *longish* paragraph`` is correct and gives *longish*.
   * ``this is a long*ish* paragraph`` is not interpreted as expected. You
     should use ``this is a long\ *ish* paragraph`` to obtain long\ *ish*
     paragraph

Special Characters and Unicode Support
======================================
We try to use typographically accurate and unicode characters where appropriate.
So make sure that the text editor of your choice has unicode support.

Add the following to the first line of every RST file where unicode characters
appear

.. code-block:: rst

   .. -*- coding: utf-8 -*-

Headings
========
In order to write a title, you can either underline it or under and over-line
it. The following examples are correct titles.

.. code-block:: rst

   =====
   Title
   =====

   subtitle
   ========

   subsubtitle
   -----------
   and so on

Two rules:

   * When using under- and over-line, their length must be identical
   * The length of the underline must be at least as long as the title itself

Normally, there are no heading levels assigned to certain characters as the
structure is determined from the succession of headings. However, it is better
to stick to the same convention throughout a project. For instance:

* `#` with overline, for parts
* `=` with overline, for chapters
* `=`, for sections
* `-`, for subsections
* `^`, for subsubsections

Internal and External Links
===========================
In Sphinx, you have 3 type of links:

   #. External links (http-like)
   #. Implicit links to title
   #. Explicit links to user-defined label (e.g., to refer to external titles).

External links
--------------
If you want to create a link to a website, the syntax is

.. code-block:: rst

   `<http://duckduckgo.com/>`_

which appear as `<http://duckduckgo.com/>`_ . Note the underscore after the
final single quote. Since the full name of the link is not always simple or
meaningful, you can specify a label (note the space between the label and link
name)

.. code-block:: rst

   `DuckDuckGo <http://duckduckgo.com/>`_

The rendering is now: `DuckDuckGo <http://duckduckgo.com/>`_.

.. note::

   If you have an underscore within the label/name, you got to escape it with a
   '\\' character.


.. _rst-implicit:

Implicit Links to Titles
------------------------
All titles are considered as hyperlinks. A link to a title is just its name
within quotes and a final underscore

.. code-block:: rst

   `Internal and External links`_

This syntax works only if the title and link are within the same RST file.
If this is not the case, then you need to create a label before the title and
refer to this new link explicitly, as explained in `Explicit Links`_ section.

Explicit Links
--------------
You can create explicit links within your RST files. For instance, this document
has a label at the top called ``rst-tutorial``, which is specified by typing

.. code-block:: rst

   .. _rst-tutorial:

You can refer to this label using

.. code-block:: rst

   :ref:`rst-tutorial`

This method will use the first title’s name found after the link. Here, the
second method would appear as :ref:`rst-tutorial`.

You can use custom text for the link

.. code-block:: rst

   :ref:`Custom Link Text <rst-tutorial>`

Which will result in, :ref:`Custom Link Text <rst-tutorial>`.

List and bullets
================
The following code

.. code-block:: rst

   * This is a bulleted list.
   * It has two items, the second
     item uses two lines. (note the indentation)

   1. This is a numbered list.
   2. It has two items too.

   #. This is a numbered list.
   #. It has two items too.

gives:

* This is a bulleted list.
* It has two items, the second
  item uses two lines. (note the indentation)

1. This is a numbered list.
2. It has two items too.

#. This is a numbered list.
#. It has two items too.

.. note::

   if two lists are separated by a blank line only, then the two lists are not
   differentiated as you can see above.


===================
What are directives
===================
Sphinx and the RST syntax provides directives to include formatted text. As an
example, let us consider the **code-block** syntax. It allows to insert code
(here HTML) within your document::

   .. code-block:: html
      :linenos:

      <h1>code block example</h1>

Its rendering is:

.. code-block:: html
   :linenos:

   <h1>code block example</h1>

Here, **code-block** is the name of the directive. **html** is an argument
telling that the code is in HTML format, **lineos** is an option telling to
insert line number and finally after a blank line is the text to include.

=================================
Inserting code and Literal blocks
=================================

How to include simple code
==========================
This easiest way to insert literal code blocks is to end a paragraph with the
special marker made of a double coulumn `::`. Then, the literal block must be
indented::

   This is a simple example::

      import math
      print 'import done'

or::

   This is a simple example:
   ::

      import math
      print 'import done'

gives:

This is a simple example::

   import math
   print 'import done'


code-block directive
====================
By default the syntax of the language is Python, but you can specify the
language using the **code-block** directive as follows::


   .. code-block:: js
      :linenos:

      game.nemron.N10Roll.action({token: token});

produces

.. code-block:: js
   :linenos:

   game.nemron.N10Roll.action({token: token});

======
Tables
======
Standard reStructuredText tables as explained here.

The rendering of the table depends on the CSS/HTML style, not on sphinx itself.

Simple tables
=============
Simple tables can be written as follows::

   +---------+---------+-----------+
   | 1       |  2      |  3        |
   +---------+---------+-----------+

which gives:

+---------+---------+-----------+
| 1       | 2       | 3         |
+---------+---------+-----------+

Size of the cells can be adjusted as follows::

   +---------------------+---------+---+
   |1                    |        2| 3 |
   +---------------------+---------+---+

renders as follows:

+---------------------+---------+---+
|1                    |        2| 3 |
+---------------------+---------+---+

This syntax is quite limited, especially for multi cells/columns.


Multicells tables, first method
===============================
A first method is the following syntax::

   +------------+------------+-----------+
   | Header 1   | Header 2   | Header 3  |
   +============+============+===========+
   | body row 1 | column 2   | column 3  |
   +------------+------------+-----------+
   | body row 2 | Cells may span columns.|
   +------------+------------+-----------+
   | body row 3 | Cells may  | - Cells   |
   +------------+ span rows. | - contain |
   | body row 4 |            | - blocks. |
   +------------+------------+-----------+

gives:

+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+
| body row 3 | Cells may  | - Cells   |
+------------+ span rows. | - contain |
| body row 4 |            | - blocks. |
+------------+------------+-----------+

Multicells table, second method
===============================
The previous syntax can be simplified::

   =====  =====  ======
      Inputs     Output
   ------------  ------
     A      B    A or B
   =====  =====  ======
   False  False  False
   True   False  True
   =====  =====  ======

gives:


=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
=====  =====  ======


The csv-table directive
=======================
Finally, a convenient way to create table is the usage of CSV-like syntax

.. code-block:: rst

   .. csv-table:: Title for the Table
      :header: "name", "firstname", "age"
      :widths: 20, 20, 10

      "Smith", "John", 40
      "Smith", "John, Junior", 20

which render as follows:

.. csv-table:: a title
   :header: "name", "firstname", "age"
   :widths: 20, 20, 10

   "Smith", "John", 40
   "Smith", "John, Junior", 20

==================================================
Include other RST files with the toctree directive
==================================================
Sooner or later you will want to structure your project documentation by having
several RST files. The **toctree** directive allows you to insert other files
within a RST file. The reason to use this directive is that RST does not have
facilities to interconnect several documents, or split documents into multiple
output files. The **toctree** directive looks like

.. code-block:: rst

   .. toctree::
      :maxdepth: 2
      :numbered:
      :titlesonly:
      :glob:
      :hidden:

      intro.rst
      chapter1.rst
      chapter2.rst

It includes 3 RST files and shows a TOC that includes the title found in the RST
documents.

Here are some notes about the different options

* **maxdepth** is used to indicates the depth of the tree.
* **numbered** adds relevant section numbers.
* **titlesonly** adds only the main title of each document
* **glob** can be used to indicate that * and ? characters are used to indicate
  patterns.
* **hidden** hides the toctree. It can be used to include files that do not need
  to be shown (e.g. a bibliography).


The glob option works as follows:

.. code-block:: rst

   .. toctree::
      :glob:

      intro*
      recipe/*
      *

Note also that the title that appear in the toctree are the file’s title. You
may want to change this behaviour by changing the toctree as follows:

.. code-block:: rst

   .. toctree::
      :glob:

      Chapter1 description <chapter1>

So that the title of this section is more meaningful.

==================
Images and figures
==================

Include Images
===============
Use

.. code-block:: rst

   .. image:: /branding/nemron-rpg-logo.png
      :width: 200px
      :align: center
      :height: 100px
      :alt: alternate text

to put an image

.. image:: /branding/nemron-rpg-logo.png
   :width: 200px
   :align: center
   :height: 100px
   :alt: alternate text

Include a Figure
=================
.. code-block:: rst

   .. figure:: /branding/nemron-rpg-logo.png
      :width: 200px
      :align: center
      :height: 100px
      :alt: alternate text
      :figclass: align-center

      figure are like images but with a caption and whatever else you wish to
      add

      .. code-block:: js

         game.nemron.N10Roll.action({token: token});

gives

.. figure:: /branding/nemron-rpg-logo.png
   :width: 200px
   :align: center
   :height: 100px
   :alt: alternate text
   :figclass: align-center

   figure are like images but with a caption and whatever else you wish to
   add

   .. code-block:: js

      game.nemron.N10Roll.action({token: token});

The option **figclass** is a CSS class that can be tuned for the final HTML
rendering.


=====
Boxes
=====

Notices: note, seealso, todo and warnings
=========================================
There are simple directives that creates nice coloured notices:

.. rst:directive:: .. seealso:: text

   .. code-block:: rst

      .. seealso:: This is a simple **seealso** note.

   .. seealso:: This is a simple **seealso** note.


.. rst:directive:: .. note:: text

   .. code-block:: rst

      .. note:: This is a **note** box.

   .. note:: This is a **note** box.


.. rst:directive:: .. warning:: text

   .. code-block:: rst

      .. warning:: note the space between the directive and the text

   .. warning:: note the space between the directive and the text


.. rst:directive:: .. todo:: text

   Lastly we also use a **todo** directive throughout the docs as a quick way to
   let any reader or reviewer know that something needs to be done.

   .. code-block:: rst

      .. todo:: Please review this feature

   .. todo:: Please review this feature


Topic directive
===============
A **Topic** directive allows to write a title and a text together within a box
similarly to the **note** directive.

This code

.. code-block:: rst

   .. topic:: Your Topic Title

      Subsequent indented lines comprise the body of the topic, interpreted as
      body elements.

gives

.. topic:: Your Topic Title

   Subsequent indented lines comprise the body of the topic, interpreted as
   body elements.


Sidebar directive
=================
It is possible to create sidebar using the following code

.. sidebar:: Sidebar Title
   :subtitle: Optional Sidebar Subtitle

   Subsequent indented lines comprise the body of the sidebar, interpreted as
   body elements.


.. code-block:: rst

   .. sidebar:: Sidebar Title
      :subtitle: Optional Sidebar Subtitle

      Subsequent indented lines comprise the body of the sidebar, interpreted
      as body elements.

|

|

======
Others
======

Comments
========
Comments can be made by adding two dots at the beginning of a line as follows

.. code-block:: rst

   .. comments


Substitutions
=============
Define a substitution as follows

.. code-block:: rst

    .. _nemron: http://www.nemron.org/

and to refer to it, use the same syntax as for the internal links: just insert
the alias in the text (e.g., ``nemron_``, which appears as nemron_ ).

A second method is as follows

.. code-block:: rst

   .. |longtext| replace:: this is a long text to include.

and then insert  ``|longtext|`` wherever required.


glossary, centered, download and fields
=======================================

Fields
------
.. code-block:: rst

   :Field Label: this is handy to create new field

:Field Label: this is handy to create new field

Glossary
--------
.. rst:directive:: .. glossary::

   .. code-block:: rst

      .. glossary::

         apical
            at the top of the plant.

   .. glossary::

      apical
         at the top of the plant.

Download
--------
.. rst:directive:: :download:`text <file>`

   .. code-block:: rst

      :download:`download character sheet </Downloads/character-sheet.pdf>`

   :download:`download character sheet </Downloads/character-sheet.pdf>`

HList
-----
.. rst:directive:: .. hlist::

   hlist can be use to set a list on several columns.

   .. code-block:: rst

      .. hlist::
         :columns: 3

         * first item
         * second item
         * 3d item
         * 4th item
         * 5th item

   .. hlist::
      :columns: 3

      * first item
      * second item
      * 3d item
      * 4th item
      * 5th item

Footnote
========
For footnotes, use ``[#name]_`` to mark the footnote location, and add the
footnote body at the bottom of the document after a “Footnotes” rubric
heading, like so

.. code-block:: rst

   Some text that requires a footnote [#f1]_ .

   .. rubric:: Footnotes

   .. [#f1] Text of the first footnote.

You can also explicitly number the footnotes (``[1]_``) or use auto-numbered
footnotes without names (``[#]_``). Here is an example [#footnote1]_.

Citations
=========
Citation references, like [CIT2002]_ may be defined at the bottom of the page

.. code-block:: rst

   .. [CIT2002] A citation
         (as often used in journals).

and called as follows

.. code-block:: rst

   [CIT2002]_

More about aliases
==================
Directives can be used within aliases

.. code-block:: rst

   .. |logo| image:: /branding/nemron-rpg-logo.png
      :width: 65pt

Using this image alias, you can insert it easily in the text `|logo|`, like this
|logo|. This is especially useful when dealing with complicated code. For
instance, in order to include 2 images within a table do as follows

.. code-block:: rst

   +---------+---------+-----------+
   | |logo|  | |logo|  | |longtext||
   +---------+---------+-----------+

+---------+---------+-----------+
| |logo|  | |logo|  | |longtext||
+---------+---------+-----------+

.. note:: Not easy to get exactly what you want though.


Metadata and Information
========================
.. code-block:: rst

   :orphan:

   Use **:orphan:** towards the top of the file (without any other text) to
   suppress warnings for RST files that are not included in table of contents.

.. rst:directive:: .. sectionauthor:: name <email>

   Specifies the author of the current section.

   .. code-block:: rst

      .. sectionauthor:: John Smith <email>

   By default, this markup isn’t reflected in the output in any way, but you
   can set the configuration value **show_authors** to True to make them
   produce a paragraph in the output.

|

----

|

.. rubric:: Footnotes

.. [#footnote1] this is a footnote aimed at illustrating the footnote capability.

.. rubric:: Bibliography

.. [CIT2002] A citation
      (as often used in journals).
