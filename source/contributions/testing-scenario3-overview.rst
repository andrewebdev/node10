Groups of unlikely allies band together to confront a common enemy. A demon set
loose in the South-African city of Johannesburg is causing chaos and descent,
in an attempt to cause divisions and civil unrest in an already tense political
climate.

:Document: :doc:`testing-scenario3`

:Setting:
   Modern Fantasy (Werewolves, Vampires, Ghosts, Witchcraft, Magic and Occult)

:Players: 4 pre-made

:Experience: Intermediate to Advanced

:Testing Goals:
   This one-shot aims to test the balance between a mix of creature types and
   their abilities.

   This will also test another core pillar of the system, which involves
   blending skills and effect templates together to dynamically “craft” magical
   spell effects.

:Feedback:
   See :ref:`the contributions section <contribute>` if you have any
   feedback, questions or ideas.
