.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: Scenario 1 (Poachers) — |nem|

.. _testing-scenario1:

##################################
Scenario 1 — Apprehending Poachers
##################################
.. include:: testing-scenario1-overview.rst

|

   **Read aloud**

   You are volunteers of the Earth Wardens, a charitable organisation that aim
   to protect natural habitat and exotic animals.

   You spent the last 2 days tracking poachers in the jungle. These poachers are
   known to illegally trap and capture exotic and endangered animal species to
   sell on the black market.

   Your goal is to capture the poachers, free any animals they may have captured
   and find evidence about their employers. Even though your task is to
   *capture*, and not kill, the poachers, you are well aware that the poachers
   will not hesitate to kill you, to avoid capture.

   You finally stop on the edge of a clearing. On the other side a towering
   cliff face stretch high above the jungle canopy. A small cavern nestles
   within the imposing rock surface, providing the perfect shelter from weather,
   and a likely base-of-operations for poachers.


.. include:: /global-message.rst
