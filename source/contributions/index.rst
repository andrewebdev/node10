.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. title:: Contributions, Feedback and Support — |nem|

.. include:: /global-message.rst

.. _contribute:

===================================
Contributions, Feedback and Support
===================================
If you like the project and you wish to contribute in any way, then this page
is a good place to start.

Testing
=======
You can help by :ref:`running some test games <contribute-testing>` and giving
feedback using any of the methods mentioned below.

Patreon
=======
You can support the project by
`becoming a patron <https://www.patreon.com/nemron>`_.

Right now we only have one tier, but if there is enough interest and support for
the project, then we will update the Patreon tiers to add more benefits and
exclusive content for patrons.

Discord
=======
You can chat to us on `Our Discord Server <https://discord.gg/zAsM7zNNrb>`_.
The community is small at the moment, but someone is sure to answer any
questions you may have.

You can also use ``#ideas-and-feedback`` channel on that server to give us your
thoughts.

On occasion, we may use that server to run games in the voice channels.

Service Desk
============
You can send an
`email to our service desk <mailto:incoming+andrewebdev-node10-15019777-issue-@incoming.gitlab.com>`_.

.. code::

   incoming+andrewebdev-node10-15019777-issue-@incoming.gitlab.com

This will automatically create an issue on our GitLab project page (see below).

GitLab
======
You can submit issues and comments on various issues and merge requests
`On our GitLab Project <https://gitlab.com/andrewebdev/node10>`_.

This is where we store all the source documents and code that we write for the
system. If you ever worked with code repositories, or are a programmer, then you
will no doubt be familiar with this.

Gitlab workflow
---------------
We use the `issue tracker <https://gitlab.com/andrewebdev/node10/-/issues>`_
to log any issues or feature requests.

`Merge requests <https://gitlab.com/andrewebdev/node10/-/merge_requests>`_
are issues, edits and documentation that are actively worked on. These are
usually related to one or more issues.

* :ref:`rst-tutorial`

BAT Tips
========
Another way you can contribute is to just use the
`Brave Browser <https://brave.com/>`_ when browsing the site.

If you use the brave browser on this site, you will notice that we are a brave
verified publisher. This means that your attention on the site can, if you
choose to, contribute BAT tokens to the project.

The browser also allows you to tip directly, or set up a monthly tip amount.
