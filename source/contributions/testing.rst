.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: Testing — |nem|

.. include:: /global-message.rst

.. _contribute-testing:

##########################
Testing and Test Scenarios
##########################
This section has information and sample scenarios that we will use to test the
rules and systems in the |nem| System.

In addition to helping with the system, the test games will give you some
fundamental understanding of some of the core rules, since each scenario is
uniquely tailored to a specific set of challenges (with some overlap where
applicable).

==================================
Scenario 1 — Apprehending Poachers
==================================
.. include:: testing-scenario1-overview.rst

====================================
Scenario 2 — Horror and Sanity Rules
====================================
.. include:: testing-scenario2-overview.rst

========================================
Scenario 3 — Modern Day, Mythical Beasts
========================================
.. include:: testing-scenario3-overview.rst
