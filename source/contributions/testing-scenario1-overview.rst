Players will track down and apprehend poachers. The poachers are trapping
exotic animals in the jungle, for the illegal animal trade. The goal for the
players is to track down poachers and apprehend them, preferably alive so that
they can be questioned by authorities.

:Document: :doc:`testing-scenario1`

:Setting: Modern Day
:Players: 2 pre-made
:Experience: Novice
:Testing Goals:
   Basic adventuring, investigation, tracking and some combat rules. This
   also has the chance of testing some item and resistance rules.

:Feedback:
   See :ref:`the contributions section <contribute>` if you have any
   feedback, questions or ideas.
