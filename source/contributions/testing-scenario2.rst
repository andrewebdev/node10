.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. |cruiser| replace:: **The Roving Star**

.. |AI| replace:: *Artificial Information Desk and Assistant (AIDA)*

.. |ai| replace:: *AIDA*

.. |planet| replace:: Mizunas

.. title:: Scenario 2 (Horror and Sanity) – Nemron RPG

.. _testing-scenario2:

####################################
Scenario 2 — Horror and Sanity Rules
####################################
.. include:: testing-scenario2-overview.rst

.. todo::

   | [x] Cruiser and planet names
   | [ ] Cruiser floor plan. Just as a flow chart to keep things simple and
         capable of using theatre of the mind
   | [x] Stats for Crim Gang
   | [ ] Roll table of equipment
   | [ ] Random encounters roll table
   | [ ] Research on what how to make TTRPG horror work

.. todo::

   Add notes for the GM to mention how to adapt the module to make certain
   character backgrounds feel useful.

========
Synopsis
========
|planet| is an oceanic planet with scattered sandy islands and shallow
reefs. |cruiser| is the only vessel to orbit the planet, and the
planet is 5 days travel away from the nearest civilisation. This makes it an
ideal tourist destination to get away from the chaos of daily life and relax a
bit.

On the surface of the planet, it’s coming up to evening and a small group of
tourists are heading back on their shuttle to go back up to the |cruiser|
for the evening.

While the PC’s were relaxing on the planet…

A criminal gang infiltrated the cruiser in an attempt to steal credits from the
casino financial servers. The gang bought a VR virus from the black market,
which should distract security by copying VR Characters and objects into the
real world via nano-technology.

This plan initially worked for the gang, with the chaos allowing them to get
access to restricted areas, while the security teams tried to deal with the VR
Leak. However, this quickly backfired on them, because the leaked VR entities
came from the Horror catalogue of programs. The entities started attacking
passengers and security staff.

They are now trapped.

Module Goals
============
The PC’s will have limited supplies at hand. They need to navigate the cruiser,
avoiding the dangerous entities. On the way they may gather more supplies,
rescue passengers (and potentially the criminals). They win if they either
escape using one of the long distance shuttles, or fix the VR leak.

.. note::

   Players must not know that it’s a VR leak. When describing the creatures and
   entities, make them believable, like creatures that invaded the vessle.

   The player characters should discover that it’s a VR Leak through
   investigation and eventual realization.

=================
Taking in the sun
=================
.. todo::

   Do we want the players to use this opportunity to get to know each other, or
   do we want to skip this bit and just start them on the shuttle on the way
   back?

=======
Shuttle
=======
All shuttles connect remotely with the cruiser, and |AI| can fly the shuttles to
and from the planet. |ai| also has a manifest of who went down to the planet
surface, and who needs to return.

* Medkit
* Beach Towels
* Fold-able Beach Chairs

.. note::

   Shuttles cannot travel long distances. They only serve to facilitate travel
   to the planet and space cruiser or, any other vessel that is close by.

===============
Common Features
===============
Maintenance Terminals:
   These terminals exist throughout the cruiser, locked behind marked wall panels.
   The wall panels require an electronic maintenance key to open. The key can also
   unlock the terminal.

   Players can pry/break open the maintenance panel with ``Power » DC4``. The lock
   cannot be picked since it’s electronic, but it can be hacked with a
   ``Systems Engineering » DC3`` check.

   Hacking the terminal require ``Systems Engineering » DC6``.

   Diagnostics will show:

      * The ship is running in lockdown-mode.
      * Escape pods have been system-locked, and cannot launch.
      * Potentially dangerous pathogen detected on-board.
      * Power runs as normal.
      * No critical damage to the ship.
      * AI in auto-pilot mode.
      * All visitors were asked to isolate in their rooms.
      * Security also reported unknown hostile actors attacking tourists and
        staff.

|AI|:
   The ship has an artificial intelligence that can opperate majority ship. It
   will follow the rules as set up by the ship captain and management. So
   getting permissions and access from the AI will be difficult.

   With the encounters, make the AI feel that the AI might be the cause of the
   issues.

   Hacking the AI would require a ``Systems Engineering » DC12`` check. These
   kinds of AI have strict security protocols due to the nature of access they
   provide. In the case of |ai|, it cannot be hacked without direct access to
   the security office where the mainframe is.

   .. todo:: explain this better

.. _test2-transport-deck:

==============
Transport Deck
==============
.. mermaid::
   :align: center

   journey
      title Overview
      section Transport Deck
         Arrival: 3: PCs
         Hook: 4: PCs, NPCs
         Investigation: 3: PCs
         Diagnostics: 3: PCs

This is where the shuttles dock with |cruiser|. The cruiser has 4 shuttles
available, and when players arrive here from the planet, the other 3 will be
here.

The AI is rudimentary and is only capable of basic communication, and limited to
specific ship functions. Only authorised personnel can make requests of the AI.

This area is deserted. Players will know that this is un-common for this deck.
There is always staff on-hand when shuttles are arriving or departing. The staff
is in charge of cleaning and prepping the shuttles for their next departure.

   Stepping off the shuttle, you immediately notice the dimmed lights, with
   emergency lights illuminating the path to the exits.

   You hear a message over the intercom: “This is |ai|. Please remain
   quarantined in your rooms while we assess the situation. The emergency
   services have been notified and we expect their arrival in approximately 1
   day.”

   The message stops, and the transport deck once again is silent… Or it would
   be, if the unusual rattling sound from behind the cafeteria desk would stop.

.. note:: |ai| will repeat the message above every 10 minutes.

Tormented NPC:
   One of the criminal gang members managed to escape all the way here. He was
   hoping to take a shuttle off ship. He is cowering in the locker.

   Completely overwhelmed with stress and torment, from escaping both the
   Xenomorph, Cultists and Haze, this individual is completely over the edge.
   He’s permanently quivering and shacking and unable to form sentences.

   When players encounter him, he will constantly try to hide his face, like a
   child thinking you cannot see them if they cover their eyes.

* Medkit (On the wall, illuminated by the emergency lighting)
* Food & drinks dispensers (Vending machines)
* Small cafeteria
* Shelves with general beach holiday provisions, like towels, swimming gear and
  water safety equipment, etc.
* Lockers where people can lock up things like valuables etc.
* Cleaning Cupboard with cleaning equipment (and chemicals) for the shuttles.
* Maintenance Console which provide diagnostics and information on the ship.

.. _test2-viewing-deck:

=====================
Pool and Viewing Deck
=====================
Large open space with domed glass stretching from one side of the deck all the
way to the other. This deck provide gorgeous views of open space, and is the
perfect place for people to come and relax, in a more chilled, quiet atmosphere.

There is also restaurant tables and seating available for diners, as well as a
impressive, circular bar in the center of the deck.

On the outer edges, there are comfortable seating areas where people can sit
back, and relax to read a book or watch the stars.

Entities
========

VR Limbo
--------
These are people that were in the VR while the virus took over the system. It
breaks the individuals out of VR, but their minds are trapped in the VR
simulation they took part in before the virus attacked.

stats etc...

C'thulu Cultist
---------------
Attributes:
   | **Body:**
     ``Power (1)``,
     ``Agility (1)``,
     ``Constitution (2)``
   | **Mind**
     ``Intellect (3)``,
     ``Awareness (2)``,
     ``Willpower (3)``
   | **Spirit**
     ``Control (3)``,
     ``Sense (1)``,
     ``Focus (2)``

Skills:
   ``Power › Brawling (1)``,
   ``Power › Grappling (2)``,
   ``Agility › Evasion (1)``,

Items:
   ``Ceremonial Knife (2)``

C-thulu Mutant
--------------
Attributes:
   | **Body:**
     ``Power (2)``,
     ``Agility (1)``,
     ``Constitution (2)``
   | **Mind**
     ``Intellect (3)``,
     ``Awareness (3)``,
     ``Willpower (1)``
   | **Spirit**
     ``Control (3)``,
     ``Sense (2)``,
     ``Focus (1)``

Skills:
   ``Power › Brawling (2)``,
   ``Power › Grappling (3)``,
   ``Agility › Evade (1)``,

Items:
   ``Claws (1)``,
   ``Bite (3)``,
   ``Tentacle (1)``

Combat:
   Claws do little damage, but the mutant prefer to use the arm with the
   tentacle to grapple their opponent, after which it will bite their victim
   to inflict a deadly damage.

.. _test2-accommodation-deck:

==================
Accommodation Deck
==================
The accomodation deck contain all the rooms where visitors stay. This deck
consist of 4 levels.

.. todo::

   * Room types.
   * Lowest level is for the workers and staff, also cheapest accomodation.
   * Every room has en-suite nano-tech shower and toilet.
   * Every room also serve as it's own escape pod. When there is an emergency,
     and people are trapped here, the rooms can eject. Each room has enough
     emergency supplies for 4 weeks, and emits an emergency beacon once
     launched.

.. _entertainment-hall:

==================
Entertainment Hall
==================
.. todo::

   * VR Rooms
   * Casino
   * Band Area
   * Simple bar and bar seating

Neural Dive Rooms
=================
This is where people come to experience any number of lucid reality
experiences.

.. note::

   This is also the primary issues for this module originates. The gang uploaded
   their VR virus here, and the programs started leaking into the real world.
   Using the large amount of nanites in the Cruizer to manifest the entities
   into the real world.

================
Engineering Deck
================
The server room is here. It’s behind a sealed security door. The goal in this
deck is for players to avoid the Xenomorph stalking the area, and make their way
to the server room.

Server Room:
   This is where the currency keys are stored. The gang already hacked and stole
   the crypto keys from the servers, but on their way out, the Xenomorph
   ambushed them.

   Two members managed to lock themselves in the server room. One barely making
   it with severe injuries (3 wounds).

   The other 5 members got split during the Xeno attack; Over the course of the
   day, another 2 died to Xeno, C'thulu cultists captured and sacrificed *one*
   member in their ritual circle.

   Only one managed to escape all the way to the Transport Deck.

Xenomorph
=========
Attributes:
   | **Body:**
     ``Power (3)``,
     ``Agility (4)``,
     ``Constitution (2)``
   | **Mind**
     ``Intellect (3)``,
     ``Awareness (4)``,
     ``Willpower (1)``
   | **Spirit**
     ``Control (2)``,
     ``Sense (3)``,
     ``Focus (1)``

Skills:
   ``Power › Brawling (4)``,
   ``Power/Agility › Climbing (4+3)``,
   ``Awareness/Agility › Stealth (4+3)``,
   ``Agility › Evade (1)``

Items:
   ``Claws/Tail/Bite (2)``

   Claws count as :ref:`combat-dual-wielding`.

===================
Security and Bridge
===================
.. todo::

   The security office is sealed due to a hull breach. A small explosion ripped
   a hole a window panel, but the damage prevented the blast shields from
   closing.

   Players need to get into this room to retrieve a security key, granting them
   access to the engineering deck.

   .. todo?? What created this explosion, why.

========
Appendix
========

|cruiser| Decks and Connected Hallways
======================================
.. mermaid::
   :align: center

   flowchart LR
      B(Bridge and Security) --- ED
      B --- AD
      AD(Accomodation Deck) --- HD
      HD(Hospitality Deck) --- EH
      HD --- VD
      EH(Entertainment Hall) --- AD
      VD(Viewing Dome) --- EH
      VD --- ED
      ED(Engineering Deck)
      SD(Shuttle Deck) --- VD

Entities
========

.. _nemron-entity-nanite-haze:

Nanite Haze
-----------
Simulates a creature that will constantly stalk the PC’s. PC’s will never have
an actual combat encounter with this creature, since it’s not real. It’s a cloud
of nanites that stimulate the senses, creating slight halucinations and events
that will make the PC’s constantly feel like someone is watching or stalking
them.

When things are quiet and PC’s are moving around the station, ask them to make
``Sense » DC3`` resistance roll.

**Failing** the resistance roll, they will hear a noise around a corner,
footsteps, or notice a shadow. They might notice someone or something disappear
behind a corner. Now they have to make a ``Willpower » DC4`` resistance roll.

**Failing the Willpower** resistance roll, characters will get
:node:ref:`1 stack of stress <Generic.Condition.Stress>`.

Once stressed by the haze, future failures of the ``Sense`` resistance roll,
will cause :node:ref:`1 Stack of Torment <Generic.Condition.Torment>`.

Security Guard
--------------
Attributes:
   | **Body:**
     ``Power (2)``,
     ``Agility (2)``,
     ``Constitution (2)``
   | **Mind**
     ``Intellect (1)``,
     ``Awareness (3)``,
     ``Willpower (2)``
   | **Spirit**
     ``Control (2)``,
     ``Sense (2)``,
     ``Focus (1)``

Skills:
   ``Power › Martial Weapons (2)``
   ``Awareness › Marksmanship (3)``,
   ``Power › Grappling (2)``,
   ``Awareness/Sense › Insight (2)``,
   ``Agility › Evade (1)``

Items:
   :item:ref:`Generic.Weapon.BasicPistol`,
   Rank 1 Technomancy Pistol,
   :item:ref:`Generic.Weapon.Dagger`,
   Swarm Capsule x 16,
   Restraints

   The Pistol is pre-programmed to inflict :node:ref:`Generic.Condition.Stunned`

Generic Bandit
--------------
Attributes:
   | **Body:**
     ``Power (1)``,
     ``Agility (2)``,
     ``Constitution (2)``
   | **Mind**
     ``Intellect (3)``,
     ``Awareness (3)``,
     ``Willpower (1)``
   | **Spirit**
     ``Control (2)``,
     ``Sense (1)``,
     ``Focus (1)``

Skills:
   ``Awareness › Marksmanship (3)``,
   ``Power › Brawling (2)``,
   ``Agility/Awareness › Stealth (2)``,
   ``Agility › Evade (2)``

Items:
   :item:ref:`Generic.Weapon.BasicPistol`,
   :item:ref:`Generic.Weapon.Dagger`

.. include:: /global-message.rst
