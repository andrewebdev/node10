.. -*- coding: utf-8 -*-

:orphan:

.. todo::

   Everything here is still to be expanded on into proper pages.

   For the time being I'm using this as a scratchpad to quickly jot down GM tips
   for later expansion.

======================
GM or Storyteller Tips
======================
.. todo:: **GM or Storyteller Tips**

   * Consider all text to contain spoilers and are for the GM only.
   * Block quotes are safe to read out to players. GM should read these to the
     players when appropriate or required.
   * Use only the player handouts to give to players. Any other documents
     regarding the world may contain spoilers for this adventure.

   For this adventure, we will use the “Theatre of the Mind” for maps and
   scenes. So when it comes to maps for scenes, we will only provide
   “flow-charts” that describe how rooms are connected, with some information on
   what you can find in the rooms.

   This approach has 2 benefits for us:

   #. We save a lot of time and resources in that we don’t have to create maps
      that would fit in print media.
   #. We can quickly review and change maps by just updating the flow charts,
      rather than having graphics files that would need a lot more time for
      updates.
   #. You, the GM, have more freedom. Other than the key features in each room,
      you can extend them however you wish, without having to justify those
      changes against limitations on graphical maps.

   Here are some resources discussing *Theatre of the Mind* that may be useful.

   * `Abyssalbrews post on TOTM <https://abyssalbrews.com/hobby-dm-theatre-of-the-mind/>`_
   * `Theater of the Mind - The Limitless Tabletop! <https://www.youtube.com/watch?v=Wf1AEmkJpeg>`_

Tracking Player Reputation
==========================
During their travels and adventuring, players may encounter different factions.
GM’s can use our “node” system to track the party (or individual) reputation
with the different factions and/or organizations.

.. note::

   While it is nice to have a mechanical rule for reputation like this, it does
   “gamify” it a bit more and players might not like it.

   For this reason we should emphasise that this is an optional rule. The GM
   can always lower difficulties based on their own knowledge about the party
   reputation, so a node like this isn’t strictly required.

   It’s merely a nice way to quantify it.

Example
-------
GM can track player reputation with a faction using the following
:ref:`Resource (R) Node <node-type-resource>`.

.. node:def:: Faction Reputation
   :uid: faction-rep
   :type: E,R

   You have good reputation with the a local faction. Reduce the difficulty for
   any social rolls with this faction by the number of ranks in this node.

   * This node cannot go higher than ``Rank 4``.
