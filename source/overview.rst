.. -*- coding: utf-8 -*-

:orphan:

.. title:: Nemron — Documentation Review and Overview

.. todo::

   We are currently populating this section by hand. I manually run wordcount
   commands over the source file and add the info here.

   We should create some sphinx directives that will automatically do this for
   us.

=====
Stats
=====

Tutorial
========
Note that the word counts listed here are for the *source* files, which includes
a lot of directives that generate the final documentation. Because of this the
word counts will be *slightly* inflated.

:Total Word Count: ``20061`` Words

What follows is a breakdown of the word counts in each section of the module
sources:

Parts Wordcount
---------------
.. code-block::

   342 index.rst
   2833 part_one.rst
   3673 part_three.rst
   3384 part_two.rst

   10257 total

Players Wordcount
-----------------
Wordcount for the pre-made player characters.

.. code-block::

   81 index.rst
   647 brandon.rst
   883 darren.rst
   835 orrian.rst
   921 rhalf.rst
   1130 yanet.rst

   4497 total

Encounters Wordcount
--------------------
.. code-block::

   61 index.rst
   219 aiden_martins.rst
   202 cs_four.rst
   676 facility_generic.rst
   484 glowing_caverns.rst
   281 kiera_wood.rst
   258 poachers.rst
   438 shane_mcnally.rst
   456 tree_burrow.rst

   3075 total

Handounts Wordcount
-------------------
.. code-block::

   34 index.rst
   74 arcgen_id_card.rst
   372 expedition_rental.rst
   24 kesser_secret_ledger.rst
   1382 kiera_personal_log.rst
   74 travel_manifest.rst
   272 yanet_research_notes.rst

   2232 total
