.. -*- coding: utf-8 -*-

.. title:: Colophon

########
Colophon
########


:Authors:

  Andre Engelbrecht

:Date: |today|

:Revision: |version|
