.. -*- coding: utf-8 -*-

.. title:: Equipment and Items

.. include:: /global-message.rst

###################
Equipment and Items
###################
This section covers the rules on using and creating items and equipment for
your games.

=========================
Equipment and Item Basics
=========================
The rank of an item reflects on the quality of the item.

.. tip:: **Equipment upgrades**

   Smiths and engineers, or anyone with the professional skill, can  upgrade or
   improve equipment. This would be the natural way to increase the rank of an
   item.

.. _equipment-intentions:

Item Intentions
===============
Some items will list one ore more :doc:`intentions` under the categories of
*damage, support, boons or conditions*.

This means that the item give the wielder access to those intended outcomes.
This give players and GM’s an easy way to recognize (and customize) what the
players can do with the items they obtain and wield.

.. _equipment-abilities:

Item Abilities
==============
Some game settings can give items special abilities beyond the obvious. This
can be enchantments or modifications. For example, a magical sword might have an
ability that will deal extra fire damage if it strikes un-dead creatures.

All abilities embedded or enchanted on an item, will have **a fixed rank**
equal to the rank of the item they are embedded onto.

Ammo, Charges and Energy
========================
Many items have their own ammo, charges or energy.

If an item has its own *energy* source, then any skills or abilities on the item
will use the item energy.

Items with depleted energy or charges can re-charge, if the game setting or item
description allows for this.

Ammunition vs. Weapon Rank
--------------------------
Usually a weapon can only use ammunition types of the same rank or lower as the
weapon.

.. tip:: **Using higher ranked ammo**

   *Advanced Mechanic, can ignore. Usage also depend on setting and narrative.*

   It is possible to use a higher ammunition rank.

   In this case the initial attack will go through, but there is a chance that
   the ammunition will damage the weapon.

   Roll the weapon rank vs. the ammunition *fixed-medium* difficulty (based on
   ammunition rank).

   Failing the roll the weapon will receive damage and drop in quality. Reduce the
   weapon level by the difference between the ammunition and weapon levels.

   .. math::

      {DamageTaken} = {AmmunitionLevel} - {WeaponLevel}

.. _equipment-generic-items:

=============
Generic Items
=============
What follows in this section is a list of *generic* equipment that can form the
basis for most game settings. Use what makes sense for your world, and feel free
to modify or adapt them as required.

.. _equipment-generic-weapons:

Weapons
=======

Melee Weapons
-------------
.. item:def:: Machete
   :uid: Generic.Weapon.Machete

   | **Level:** 1
   | **RRP:** 100
   | **Attribute(s):** Power
   | **Range:** :ref:`Melee <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Slashing`,
     :node:ref:`Generic.Damage.Kinetic` (If bashing with handle, spine or
     flat edge of the blade).

   A blade that meant for cutting down jungle undergrowth.

.. item:def:: Combat Knife
   :uid: Generic.Weapon.Dagger

   | **Level:** 1
   | **Attribute(s):** Agility
   | **Range:** :ref:`Melee <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Piercing`,
     :node:ref:`Generic.Damage.Slashing`,
     :node:ref:`Generic.Damage.Kinetic` (if bashing with the handle).

   A strong combat knife. A common, standard issue weapon for most armed forces.

.. item:def:: Short Sword
   :uid: Generic.Weapon.ShortSword

   | **Level:** 2
   | **Attribute(s):** Power
   | **Range:** :ref:`Melee <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Slashing`,
     :node:ref:`Generic.Damage.Piercing`,
     :node:ref:`Generic.Damage.Kinetic` (if bashing with pommel, spine or
     flat edge of the blade).

.. item:def:: Rapier
   :uid: Generic.Weapon.Rapier

   | **Level:** 2
   | **Attribute(s):** Agility
   | **Range:** :ref:`Melee <rules-range-indicators>`
   | **Damage:** :node:ref:`Generic.Damage.Piercing`

.. item:def:: Great Sword
   :uid: Generic.Weapon.GreatSword

   | **Level:** 3
   | **Requires:** Power rank 2
   | **Attribute(s):** Power
   | **Range:** :ref:`Melee <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Slashing`,
     :node:ref:`Generic.Damage.Piercing`,
     :node:ref:`Generic.Damage.Kinetic` (if bashing with pommel, spine or
     flat edge of the blade)

   Great swords can deal a lot of damage, and are often used to not only
   damage the opponent, but also make them stagger etc. They do require more
   power to use, since these weapons are usually heavy.

Shields
-------
While shields usually provide defence and cover, characters can also use them
offensively. For this reason we do list damage types with the shields.

.. item:def:: Buckler
   :uid: Generic.Weapon.Buckler

   | **Level:** 1
   | **Attribute(s):** Agility
   | **Damage:** :node:ref:`Generic.Damage.Kinetic`
   | **Support:** :node:ref:`Generic.Ability.Disarm`

.. item:def:: Medium Shield
   :uid: Generic.Weapon.MediumShield

   | **Level:** 2
   | **Attribute(s):** Agility, Power
   | **Damage:** :node:ref:`Generic.Damage.Kinetic`

.. item:def:: Spiked Shield
   :uid: Generic.Weapon.SpikedShield

   | **Level:** 2
   | **Attribute(s):** Agility, Power
   | **Damage:**
     :node:ref:`Generic.Damage.Kinetic`,
     :node:ref:`Generic.Damage.Piercing`

.. item:def:: Tower Shield
   :uid: Generic.Weapon.TowerShield

   | **Level:** 2
   | **Requirements:** Power rank 2
   | **Attribute(s):** Power
   | **Damage:**
     :node:ref:`Generic.Damage.Crushing`,
     :node:ref:`Generic.Damage.Kinetic`

Bows
----
.. item:def:: Short Bow
   :uid: Generic.Weapon.ShortBow

   | **Level:** 1
   | **Attribute(s):** Agility
   | **Range:** :ref:`Near <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Piercing`.
     (Depending on the ammo used, other types may also be available.)

   A normal short bow.

.. item:def:: Long Bow
   :uid: Generic.Weapon.LongBow

   | **Level:** 2
   | **Requires:** Power rank 2
   | **Attribute(s):** Power
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Piercing`.
     (Depending on the ammo used, other types may also be available.)

   A long bow that has a thick and strong spine. Because of the difficulty to
   draw the bow, it requires a lot of power to use.

.. item:def:: Compound Bow
   :uid: Generic.Weapon.CompoundBow

   | **Level:** 3
   | **Requires:** Power rank 2
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Piercing`.
     (Depending on the ammo used, other types may also be available.)

   A compound bow made from a combination of materials to provide maximum power
   behind the arrow, still being easy to aim. The balance on this bow allows for
   aiming using awareness rather than power or agility.

   It still has a minimum power requirement to use since the bow is still heavy
   to make the initial draw.

Modern Weapons
--------------
.. item:def:: Basic Pistol
   :uid: Generic.Weapon.BasicPistol

   | **Level:** 1
   | **RRP:** 1,000
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Ballistic`.
     (Depending on the ammo used, other types may also be available.)

   A common pistol. Default Clip can take up to 8 bullets.

.. item:def:: Police Pistol
   :uid: Generic.Weapon.CombatPistol

   | **Level:** 2
   | **RRP:** 1,400
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Ballistic`.
     (Depending on the ammo used, other types may also be available.)

   A more advanced pistol that is commonly used by security forces.

   * 12 Bullet Mag
   * 1 x Accessory Attachment
   * 1 x Combat Knife Attachment

.. item:def:: Shotgun
   :uid: Generic.Weapon.Shotgun

   | **Level:** 3
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Near <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Ballistic`,
     :node:ref:`Generic.Damage.Kinetic`.
     (Depending on the ammo used, other types may also be available.)

.. item:def:: Hunting Rifle
   :uid: Generic.Weapon.HuntingRifle

   | **Level:** 2
   | **RRP:** 3,000
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Ballistic`.
     (Depending on the ammo used, other types may also be available.)

   Preferred by explorers and hunters, this rifle is handy in most scenarios.

   * Default Clip Capacity: 10 Bullets

.. item:def:: Assault Rifle
   :uid: Generic.Weapon.AssaultRifle

   | **Level:** 3
   | **RRP:** 10,000
   | **Attribute(s):** Awareness
   | **Range:** :ref:`Far <rules-range-indicators>`
   | **Damage:**
     :node:ref:`Generic.Damage.Ballistic`.
     (Depending on the ammo used, other types may also be available.)
   | **Abilities:**
     :node:ref:`Generic.WeaponMod.BurstFire`,
     :node:ref:`Generic.WeaponMod.SpreadFire`

   * Two-handed
   * Default Clip Capacity: 50 Bullets
   * 2 x Type 2 Accessory Attachment
   * 1 x Combat Knife Attachment

Modern Weapon Abilities
~~~~~~~~~~~~~~~~~~~~~~~
.. todo:: Would these work better as custom intentions for these weapons?

.. node:def:: Burst-Fire
   :uid: Generic.WeaponMod.BurstFire
   :type: Ab

   The weapon has the ability to fire number of bullets equal to the *rank of
   this ability*. Each bullet will hit with less precision than the previous.

   On successful attack:

   #. The first bullet will do full damage.
   #. Each successive bullet damage is reduced by half (rounded down) until the
      damage is zero, or the maximum bullets fired (based on ability rank).

   .. note:: **Example**

      Brandon Ross uses the burst fire ability on his assault rifle. He rolls
      ``6`` for his attack. If the target doesn’t defend, he will get the
      following damage:

      #. 6 Damage
      #. 3 Damage
      #. 1 Damage

      Assuming the target tried to dive behind cover, and rolled ``4`` on their
      evasion roll. They will take the following damage:

      #. 2 Damage
      #. 1 Damage
      #. 0 Damage

.. node:def:: SpreadFire
   :uid: Generic.WeaponMod.SpreadFire
   :type: Ab

   Pull back the trigger and let the weapon unleash havoc. When using this
   ability you can hit multiple targets, depending on where you start and end
   your spread.

   | **AP Cost:**
     When first activating this ability, you spend the normal AP.
     Then for every extra enemy that you target with the spread, you need to
     spend ``1 additional AP``. This is because you are physically strafing
     your weapon towards those targets.
   | **Bullet Cost:** Equal to the ability rank, per target.
   | **Damage:**
     Use the :node:ref:`Generic.WeaponMod.BurstFire` rules for each target. You
     only have to roll your spread-fire once. The same roll apply to each
     target, so long as you have enough ammunition and AP.

Ammunition
----------
Ammunition for weapons can sometimes have extra on-hit effects. In addition,
certain ammunition types are only usable by certain weapons.

.. note::

   Unless specified by the ammunition description, *ammunition level do not
   contribute* to the attack roll dice pool.

   The damage is already determined by the weapon. Ammo merely changes the
   “on-hit” intention.

.. item:def:: Generic Bullets
   :uid: Generic.Ammo.Bullets

   | **Level:** 1
   | **RRP:** 200
   | **Damage:** :node:ref:`Generic.Damage.Ballistic`

   Just generic bullets that will work in most small and medium arms.
   Sold as a pack of 50.

.. todo:: Expand on more ammo types

.. _equipment-generic-armour:

Armour
======
.. item:def:: Tactical Vest
   :uid: Generic.Armour.TacticalVest

   | **Level:** 0
   | **RRP:** 100

   Provides extra pockets to carry more stuff.

   *Note* that the level is zero. This basically means it provides no
   protection. It purely exists for utility.

.. item:def:: Light Armour
   :uid: Generic.Armour.Light

   | **Level:** 1
   | **RRP:** 500

   A basic piece of durable cloth armour worn underneath other clothes. This is
   a standard piece of equipment most adventurers.

   This kind of armour is light enough to wear underneath Medium or Heavy
   armour, providing an extra base layer of protection.

.. item:def:: Medium Armour
   :uid: Generic.Armour.Medium

   | **Level:** 2
   | **RRP:** 1,000
   | **Penalty:**
     While wearing medium armour you permanently have
     :node:ref:`1 rank of Encumbered <Generic.Condition.Encumbered>`

   Strong re-enforced materials make up this flexible armour.

.. item:def:: Heavy Armour
   :uid: Generic.Armour.Heavy

   | **Level:** 3
   | **RRP:** 2,000
   | **Penalty:**
     While wearing medium armour you permanently have
     :node:ref:`2 ranks of Encumbered <Generic.Condition.Encumbered>`

   Strong composite materials make this a tough armour. It does restrict
   movement, but provides good protection.

----

:Author(s):

   * Andre Engelbrecht

.. include:: /content-footer.rst
