.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: Old Rules - Nemron RPG

.. include:: /global-message.rst


#########
Old Rules
#########
This page contain old rules that we decided to replace with something else. In
some cases these rules may still be helpful for different kinds of games or
player groups. For this reason we keep them here for reference.

===============
Old Health Node
===============
Some groups might prefer using the old health system that is slightly more
crunchy. Unlike wounds, health start out with a maximum, and as you take damage
you reduce the health.

.. node:def:: Health
   :uid: Attribute.Body.Health
   :type: E, R

   Represents the overall health of your body. Any physical damage received will
   reduce this resource node.

   The maximum health depends on the ranks for the other **Body** nodes,
   :math:`{MaxHealth} = {Power} + {Agility} + {Constitution} + 5`.

   **When health reach zero**, the character is critically wounded, and will be
   :node:ref:`Generic.Condition.Downed`.

   **Healing and Recovery**

   For every **4 hours of rest**, the character will naturally receive healing
   equal to their **Constitution** rank.

Health Node Damage Changes
==========================
If you decide to use the health node, you will also need to change some
intentions and attack/damage rolls.

Your attack roll also count as your damage roll. If, for example, you roll ``8``
on your attack roll, you can deal up to ``8 points`` of damage.

The **amount of damage dealt**, is the difference between the attack and defence
rolls and, any armour that the character might have.

.. math::

   {DamageTaken} = {AttackRoll} - {DefenceRoll} - {ArmourRank}

|

===============
Old Sanity Node
===============
.. node:def:: Sanity
   :uid: Attribute.Mind.Sanity
   :type: E, R

   .. note:: **Disclaimer**

      Real world mental trauma is serious and, should never be treated jokingly,
      or haphazardly.

      If you decide to use the sanity rules in your games, then make sure your
      players are OK with using these rules. Also make sure that they are aware
      that certain sensitive topics may come up during play.

      **Tools**

      It may be handy to have tools in your game to help deal with sensitive
      topics. I personally like the
      `X-, and O-Card System <https://docs.google.com/document/d/1SB0jsx34bWHZWbnNIVVuMjhDkrdFGo1_hSC2BWPlI3A/edit?usp=sharing>`_.

   Sanity refers to the soundness, rationality, and health of the mind.

   This node is a resource that can heal slowly, with therapy. The maximum
   amount of sanity depends on the other **Mind** nodes.

   :math:`{MaxSanity} = {Intellect} + {Awareness} + {Willpower} + 5`

   **Whenever a character take sanity damage**, they need to write down on the
   character sheet, the source and amount of damage.

   Every subsequent exposure to the same scenario, the character needs to make a
   **Willpower Resistance Roll** vs. their
   :ref:`willpower fixed-medium <fixed-med-max>`.

   **Failing the resistance roll**, the character will receive
   :node:ref:`ranks of Torment <Generic.Condition.Torment>` equal to the amount
   of mental trauma originally suffered for that source (as written down on the
   character sheet).

   **Healing Sanity**

   Sanity cannot instantly recover with items or skills. It takes at least
   **3 successful therapy sessions** with a trusted individual like a friend or
   a qualified therapist.

   Each session will require a different dice check at the end of a therapy
   session, and a character cannot make more than one of these checks per day.

   #. **Awareness** to identify the mental trauma accurately.
   #. **Intellect** to rationalize and work through the trauma.
   #. **Willpower** to utilize the insight gained in an attempt to heal the
      trauma.

   The difficulty for each of the rolls is the
   :ref:`fixed-medium <fixed-med-max>` for the **attribute** used.

   **When one of the rolls fail**, the character can retry that roll again the
   end of the next therapy session.

   **Once all 3 rolls succeeded**, the character will heal **1 Sanity**, and can
   remove the source that caused the trauma from their character sheet.

.. todo:: Describe changes required.
