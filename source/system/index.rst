.. -*- coding: utf-8 -*-

.. title:: Nemron RPG System

.. include:: /global-message.rst

.. image:: /branding/nemron-rpg-logo.png
   :width: 100%

|

#################
Nemron RPG System
#################
.. tip::

   Majority of the game system is just about understanding some basic
   :doc:`rules`, and how to use :doc:`skills`,
   :ref:`skill domains <skills-domains>` and :doc:`intentions`.

   Once you have a basic understanding of this, you can play almost any genre
   and setting with little effort, changes or modifications.

=====
Rules
=====
In this section we discuss character creation and the core rules in the system.
You don’t have to memorize every rule. Just skim through it so that you know
what is there, and have a general idea where to find some bit of information if
you need it.

* :doc:`rules`

=============================
Skills, Traits and Intentions
=============================
In the rules section we had a quick summary of what nodes are, and the core
types of nodes. The following sections go into more detail on nodes, categories
of nodes, and their purpose within the system in general.

* :ref:`rules-character-traits` – These are specific traits available to
  characters, races or frames to make them more unique, they represent natural
  ability for the character.
* :doc:`skills` – Generic skills that all (or most) characters can train, and
  applicable to most world settings and game types.
* :doc:`intentions` – Unique to the Nemron system, *intentions* is a mechanical
  and systematic approach to specify the desired outcome for any character
  action.

=========
Equipment
=========
Here we discuss how equipment are just nodes (like skills) that rounds out the
system. We talk about the different kinds of equipment, and provide a couple of
generic items that GM’s or creators can use as a baseline for their own, custom
settings.

* :doc:`equipment`

======
Frames
======
Frames are our way of defining the “body” or “frame” for a particular character.
The equivalent for frames in other game settings is “race”.

We don’t use the term “race” in Nemron, purely because some game settings don’t
have a concept of “race”. Race doesn’t make sense in a game setting that consist
entirely of robots and mechanical beings (think of the indie game Machinarium).

For your own game settings, you can still use the term “race”, but just be aware
that in the background a “race” is just a “frame”, and throughout the system we
refer to “frame”.

* :doc:`frames`

----

.. toctree::
   :maxdepth: 1

   rules
   intentions
   skills
   equipment
   frames
   extended_rules

----

:Author(s):

   * Andre Engelbecht

.. include:: /content-footer.rst
