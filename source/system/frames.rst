.. -*- coding: utf-8 -*-

.. title:: Frames (Races)

.. include:: /global-message.rst

##############
Frames (Races)
##############
.. include:: _frames_sidebar.rst

This section covers the rules on creating races and frames for your games. We
include a couple of common example races and frames that you can quickly plug
into most game settings.

===================
General Frame Notes
===================
When creating a character, the player will usually start with their frame. Most
other games and settings call this the character “race” but, we prefer the term
“frame”.

We don’t use the term “race” in Nemron, purely because some game settings don’t
have a concept of “race”. Race doesn’t make sense in a game setting that consist
entirely of robots and mechanical beings (think of the indie game Machinarium).

For your own game settings, you can still use the term “race”, but just be aware
that, mechanically a “race” is just a “frame” and, throughout the system we
will use the term “frame”.

Some worlds and settings will share common frame types, like “human” or “dwarf”.
Other game world may have unique frames for that setting.

Frame Attributes
================
Every frame will provide a set of *minimum and maximum* potential values for the
primary attribute nodes of the frame.

Frames are Domains
==================
Every frame counts as it’s own :ref:`domain <skills-domains>`. This means that
depending on the rank, the character can have access to frame-specific features.

Like domains, frames do can have a rank. However, unlike domains, frames
*cannot* be ranked up with **XP**. Frames normally have a fixed rank.

The GM could come up with narrative reasons why a frame might increase in rank,
but this would normally depend on the game world/setting.

Features are Optional
---------------------
When a player create their character, they can ignore the frame features and,
instead pick starting *skills* in their place. Remember that they **cannot** use
XP to purchase features later on. Frame features are *fixed* during character
creation.

That said, the GM always have the option to override this rule for narrative
reasons.

.. _frames-generic:

==============
Frame Examples
==============
The following is a list of generic frames that is commonly used in other game
worlds and settings. Feel free to use them as-is for your game, or modify and
change them to suit your needs.

They also serve as great examples of how to create your own custom frames for
the world/setting you want to run your game in.

.. node:def:: Generic Human
   :uid: Generic.Frame.Human
   :type: E

   Humans are quick to adapt to change and quickly pick up new skills.

   | Mundane every-day NPC’s will be have a ``Rank 0`` frame.
   | Trained NPC’s like guards usually have ``Rank 1`` frames.
   | Heroes and **player characters** are usually ``Rank 2``.

   It’s rare for humans (even heroes and PC’s) to have a frame higher than
   ``Rank 2``.

   Attributes [Min/Max]
      | **Body:** Power [0/3], Agility [0/3], Constitution [0/3]
      | **Mind:** Intellect [0/3], Awareness [0/3], Willpower [0/3]
      | **Spirit:** Control [0/3], Sense [0/3], Focus [0/3]

   Abilities
      :node:ref:`Generic.Ability.Shout`

   Traits
      :node:ref:`Generic.Boon.Ambidex`,
      :node:ref:`Generic.Boon.Charming`,
      :node:ref:`Generic.Boon.Empath`,
      :node:ref:`Generic.Boon.InnerMonologue`,
      :node:ref:`Generic.Boon.Luck`,
      :node:ref:`Generic.Boon.Resolve`,
      :node:ref:`Generic.Boon.Talented`,
      :node:ref:`Generic.Boon.Valour`

.. node:def:: Generic Dwarf
   :uid: Generic.Frame.Dwarf
   :type: E

   | Mundane every-day NPC’s will be have a ``Rank 0`` frame.
   | Trained NPC’s like guards usually have ``Rank 1`` frames.
   | Heroes and **player characters** are usually ``Rank 2``.

   Attributes [Min/Max]
      | **Body:** Power [0/4], Agility [0/3], Constitution [0/5]
      | **Mind:** Intellect [0/3], Awareness [0/2], Willpower [0/2]
      | **Spirit:** Control [0/3], Sense [0/3], Focus [0/3]

   Abilities
      :node:ref:`Generic.Ability.Shout`

   Traits
      :node:ref:`Generic.Boon.CombatAwareness`,
      :node:ref:`Generic.Boon.DarkSight`,
      :node:ref:`Generic.Boon.Empowered`,
      :node:ref:`Generic.Boon.Endurance`,
      :node:ref:`Generic.Boon.Fury`,
      :node:ref:`Generic.Boon.Immunity`,
      :node:ref:`Generic.Boon.Resilience`

.. node:def:: Generic Elf
   :uid: Generic.Frame.Elf

   | Mundane every-day NPC’s will be have a ``Rank 0`` frame.
   | Trained NPC’s like guards usually have ``Rank 1`` frames.
   | Heroes and **player characters** are usually ``Rank 2``.

   Attributes [Min/Max]
      | **Body:** Power [0/1], Agility [0/4+2], Constitution [0/2]
      | **Mind:** Intellect [0/3], Awareness [0/4+1], Willpower [0/3]
      | **Spirit:** Control [0/3], Sense [0/4], Focus [0/3]

   Abilities
      :node:ref:`Generic.Ability.EvasiveMovement`

   Traits
      :node:ref:`Generic.Boon.AgileStriker`,
      :node:ref:`Generic.Boon.Alert`,
      :node:ref:`Generic.Boon.Ambidex`,
      :node:ref:`Generic.Boon.PointBlank`,
      :node:ref:`Generic.Boon.Farsight`,
      :node:ref:`Generic.Boon.FleetFoot`,
      :node:ref:`Generic.Boon.KeenSense`,
      :node:ref:`Generic.Boon.Mobility`

.. _frames-generic-constructs:

Construct
=========
Construct cover robots, androids and any game setting where a creature was
“constructed”.

Construct Attribute Min/Max
   We cannot really define min/max values for constructs. This is because it
   entirely depends on who created the construct, and what its primary purpose
   is for.

   *Non-organic constructs* (like androids) **cannot** rank up their
   :ref:`character-attributes` using skill points. In order to upgrade their
   attributes, they have to spend money to buy and install upgrades. Of course,
   this could change based on the narrative and GM discretion.

Construct Health and Healing
----------------------------
*Non-organic* constructs cannot heal naturally. They need to heal wounds by
making repairs with the appropriate tools and parts.

In some game settings, special construct rules may apply, like using
nano-technology to repair the *non-organic construct*. This would effectively be
a healing mechanic for constructs in those settings.

.. include:: /content-footer.rst
