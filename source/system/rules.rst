.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. title:: Rules - Nemron RPG

.. include:: /global-message.rst

.. _rules:

#####
Rules
#####
The following chapters discuss the general rules of the system. Once you
understand a couple of core concepts, it will quickly become clear how you can
adapt the system to different genre’s and settings, and how you can use it in
interesting ways to tell your stories.

.. tip::

   Before you start reading through the rules, check that you have the
   following.

   * **Rank Dice** are the dice you will use for the different attributes,
     domains and skills in the game. These are **d4, d6, d8, d10 and d12**.
     It’s best to have two or more of each of these dice (except maybe the
     **d4**), since it is common to roll multiple of the same type during skill
     checks.
   * **Extra Paper, pencils and eraser** to make notes and keep track of things.
   * *(Optional)* A single twenty-sided die (1d20). We call this
     **the Drama Dice** and it’s purpose is to inject drama into the scene.

===================
Creating Characters
===================
To start out with download and print a
:download:`character sheet </Downloads/character-sheet.pdf>`. Once you have
that, follow the steps below to create your first character.

1. Choose a Frame (Race)
========================
.. include:: _frames_sidebar.rst

Pick your frame from the list of :ref:`frames-generic`. Different genres and
worlds may have different rules for frames and races, but in general every frame
will have certain strengths, weaknesses and core skills or traits specific to
the frame in question.

See :doc:`frames` for more details on frames.

2. Choose your Starter Skills and Domains
=========================================
Every starting character will get **7 skills** on **rank 1**. These starting
skills represent the experience a character gained up to this point.

.. _characters-level-up:

3. Levelling Up
===============
Starting characters get **5 experience points (XP)**. Characters use
**experience points (XP)** to rank up **Attributes, Skills and Domains**.

To rank up any existing node by **1 rank**, you need to spend a number of
**XP equal to the current rank** of that node.

.. tip:: *Levelling Cool Down*

   All nodes (attributes, skills and abilities) should have a **one month** cool
   down period after ranking up. This means that a character cannot rank the
   same skill more than **one level per in-game month**.

   The GM can always override this if they think a sufficient amount of time
   have passed. We just need to enforce some kind of limit on how fast one
   specific attribute, skill or ability can upgrade.

Leaning new Skills
------------------
Existing characters can learn new skills, but it takes a long time. To learn a
new skill a character must first tell the GM what skill they want to train.

They must then train **2 hours, for 30 days (in-game)**.

At the end of the training period, the character must spend **1 XP** to finish
their training. They will now have the skill on ``Rank 1``.

Upgrade a Skill into a Domain
-----------------------------
Characters can **upgrade** an existing skill of theirs, into a domain (If a
domain is available for that skill in the game setting), by spending **2 XP**.

Now that the skill is a domain, they can start buying **Domain Features**,
within the following limitations:

* You cannot have more domain features than the **domain rank**.
* You can only purchase **one feature per month (in-game)**.
* Every domain feature will cost **2XP** to train.

Earning XP
----------
Experience points is the standard reward that goes out to characters that
overcome great challenges and make good use of their skills.

Minor milestones can yield ``1 to 2 XP``, while major milestones can yield a
further ``2 - 3 XP``.

.. tip::

   The GM is free to choose when he wants to award skill points. Our personal
   preference is to reward players more for exploration and clever solutions to
   problems, over difficult combat.

   The more you reward players for combat, the more they will gravitate to using
   combat as a solution to most problems.

   Even though we prefer not rewarding points for combat, we do reward players
   if they make creative use of their skills during combat, we want to encourage
   players to use their skills and abilities in interesting ways.

   *These are just our suggestions.* You can still award points in your own way
   if you prefer.

.. _rules-node-types:

==========
Node types
==========
The Node types form one of the core pillars of the system. They represent
Attributes, Skills, Abilities, Effects, Items or anything that can have a rank
or level in the game world.

.. _node-type-attribute:

Attribute Nodes
===============
Attribute nodes represent the natural ability that all characters in the world
will have.

Attribute nodes *always* add a dice to the dice pool. Any action that the
character takes will *always* depend on at least one attribute node.

.. _node-type-skill:

Skill (S)
=========
This is the most common type of node and represents a skill that the character
learned or trained.

**Skill nodes add dice to the dice pool.**

Only add these nodes to the dice pool when the skill can contribute to the
action.

Some skills can provide extra features as the skill level up. We call these
nodes **Skill Domains**, and other than the extra features they provide, they
function mostly the same as basic skill nodes.

.. _node-type-ability:

Ability (AB)
============
Ability nodes are special skills that augment your actions in some way. They
also represent abilities that are magical or special and usually require
:node:ref:`Generic.Trait.Energy` to use.

Abilities can also exist within items as an enchantment for example.

Ability Nodes **do not** add dice to the dice pool for their level. But the
level of the ability may have different effects, at different levels.

.. _node-type-effect:

Effect (E)
==========
Effect Nodes represent passive effects that influence the character either
positively or negatively. These can be conditions like *Blind* or *Stunned*, or
it could be a boon like *Fury*.

.. _node-type-trait:

Trait (T)
=========
Traits are usually :ref:`node-type-effect` nodes that is permanently available
for the character based on their frame, background or other special
circumstances.

They will always have this at minimum on ``rank 1``.

Traits **cannot** rank up with **XP**. But if the trait is a boon or condition,
and the character gains more of the same boon/condition, then the rank will
temporarily increase, after which it will decay back down to ``rank 1``.

.. _node-type-resource:

Resource (R)
============
Resource Nodes represent expendable resources or abilities. Each rank represents
a single available “charge” or “use”.

Resource nodes are *almost always* used in combination with another node type.

An example of a node-type that is also a *resource*, is the boon
:node:ref:`Generic.Boon.Inspired`.

================
Characters Stats
================
In Nemron, we define characters by their nine **Attributes** and any
combination of **Traits**, **Skills** and **Domains**.

.. _character-attributes:

Attribute Nodes
===============
Attributes embody the raw, natural capabilities and health of your character.
We group them into three attribute lines, **Body**, **Mind** and **Spirit**.

.. _attribute-line-body:

Body Line
---------
Power
   Any action that relies on physical power will use this attribute. It includes
   things like melee combat, certain athletic tasks, like lifting heavy objects,
   etc.

Agility
   This attribute is for any action that relies on a certain amount of finesse
   and accuracy in movement. It includes tasks that require the entire body or,
   tasks that require finer motor skills like the dexterity to play a piano or
   guitar.

Constitution
   This is the attribute that describe your physical fitness and resistance to
   physical ailments.

   Constitution is the **upkeep attribute** for any long-lasting physical
   activity.

.. _attribute-line-mind:

Mind Line
---------
Intellect
   This attribute describes your intellectual prowess, how quickly you can think
   on your feet and how well you learn and retain information.

Awareness
   Your awareness of your surroundings, objects or people.

   This attribute also informs how well you are capable of reading the emotional
   state of an individual and how well you can read the social mood.

   For this reason awareness is often used for rolls that involve accurate
   interpretation during social interactions.

Willpower
   This attribute describes how well you can resist any mental distractions or
   temptations, as well as your ability to rationalize difficult situations, so
   they don’t lead to extra distress.

   Willpower is the **upkeep attribute** for any long-lasting mental activity.

.. _attribute-line-spirit:

Spirit Line
-----------
Control
   This attribute informs how well you are capable of controlling your own
   nervous system, fine motor movement, emotional state, and social interactions
   etc.

   Depending on game settings, *Control* also describe your connection to
   magic or cybernetic implants, and your ability to activate and “control”
   those domains.

Sense
   This attribute informs how well you are able to sense or read minute changes
   within your nervous system and sensory organs/devices.

   Depending on the game setting, *Sense* also describe your connection to magic
   or cybernetic implants, and your ability to detect or “sense” changes within
   those domains.

   A cybernetic implant that give you night vision for example, will use this
   attribute instead of **Awareness**.

Focus
   This attribute informs how well you can maintain focus on actions that
   involve the nervous system or *spirit line*.

   Focus is the **upkeep attribute** for any long-lasting actions using the
   spirit line.

Energy
======
Energy is a resource that every character has. Characters spend energy for
special abilities or any activity that requires high amount of effort.

.. node:def:: Energy
   :uid: Generic.Trait.Energy
   :type: E,R

   The energy pool is the total amount of energy you have when rested and
   healthy. Any activity that require high amounts of exertion, will spend this
   resource.

   The maximum amount for this resource node depends on the character
   **Spririt** line.

   :math:`{MaxEnergy} = {Control} + {Sense} + {Focus} + 5`

   Spending Energy:
      Spend **1 Energy** for every hour of hard physical effort, like jogging,
      hiking up a mountain etc.

      Or, spend **2 Energy** once per round, to get **one** bonus *reaction*.

      Or, by using a skill that has energy requirements (magic, technomancy etc.)

   Recovering Energy:
      When resting, energy will recover **1 point** for every hour of rest.

.. tip::

   You can have more than one kind of energy node in your game setting. I ran
   a custom setting in the past that had **three** energy pools (one for each
   line; Body, Mind, Spirit), and each type of skill had to use energy from a
   specific pool.

   We eventually scrapped it for a single pool, since it makes for better
   resource management and less complexity, but you should feel free to do what
   suits your setting and players best.

Limbs and Health
================
Ever character, PC or NPC, has one or more *major* and *minor* limbs.

**Lesser limbs** are limbs that are not critical for survival, like an arm.
Wounds received on this limb can disable, or sometimes dismember the limb, but
it won’t necessarily lead to character death.

**Major limbs** are critcal for survival, like your torso or head. Significant
wounds on these limbs would likely lead to character incapacitation or death.

Humanoid characters, for example, have two major limbs and 4 minor limbs, which
we can represent on a **6-sided dice**. We call this the **Location Dice**.

.. csv-table:: Limb Assignments
   :header: Dice Number, Limb

   1, Right Arm
   2, Left Arm
   3, Right Leg
   4, Left Leg
   5, Torso
   6, Head

.. tip::

   Other entities might exist that have different amounts of limbs. In this case
   you can use different location dice for those creatures.

   In my own games, I still use a D6 for creatures with extra limbs, but we rule
   that only a subset of 6 limbs are valid targets, depending on the positioning
   of the character.

   If, for example, a creature has the 6 standard limbs plus a tail, and the
   PC is attacking from in the front, then I would rule that the tail cannot be
   targeted.

.. node:def:: Health
   :uid: Generic.Attribute.HP
   :type: E,R

   Every limb on the character will have a maximum amount of “health points”.
   The *HP* for each limb is, :math:`{HP} = 5 + {Pow} + {Agi} + {Con}`.

   **If the limb is a minor limb,** and you have *zero* hit-points, the limb is
   no longer usable. In severe cases, limbs can be dismembered or broken beyond
   repair or healing.

   **If the limb is a major limb,** like a torso or head, and you have
   **zero hp**, then the character will be downed and start dying.

   See :node:ref:`the downed condition <Generic.Condition.Downed>` for details
   on this.

Healing HP
----------
**Every eight hours of rest,** a injured character may roll ``Constitution`` to
determine the amount of healing they will recover during the rest period.

The amount rolled is the amount of healing that every damaged limb will receive.

Targeted Healing
----------------
If you are using first aid or some healing skill to heal a specific limb, then
the character can only use the healing points on that specific limb.

Movement Speed
==============
Movement speed is the distance that a character can cover during their turn. We
measure this in meters.

.. math::

   {MoveSpeed} = {Agility} \times 2

.. note::

   Generally, we don’t ask a player to roll dice for movement, but there
   is still an **implied roll** happening in the background, it's just that the
   difficulty for normal movement is too easy to bother with rolls.

   The implied roll for normal movement is ``Agility``.

   If you wish to move even faster by running or dashing, you need to add an
   extra boost of power. The **implied roll** for this is ``Agility › Power``,
   and so you move twice the range.

Skill and Domain Nodes
======================
All characters can have any number of skill and item nodes, which represent the
background, training and experience for the character.

In Nemron, skills do not exist in isolation. We allow characters to use multiple
skills in combination, so long as the player and GM agrees that the skill
combination is relevant to the dice check.

This means that a character wanting to jump over a chasm may add both their
Athletics and Acrobatics skills to their dice pool (assuming they have both),
since their experience in these disciplines have an chance to affect the
outcome.

See our :doc:`skills add-on <skills>` for a list of starter skills you can adapt
to your game settings.

.. _rules-character-traits:

Character Traits
================
Some characters can have permanent traits based on their frame (species/race).

Character traits are usually :ref:`intentions-boons` or
:ref:`intentions-conditions` that a character permanently has on ``rank 1``.

If a character has boons or conditions as a trait, then that boon or condition
cannot *decay* below ``rank 1``. This does not apply to *resource nodes* (see
below).

Resource Nodes as Character Traits
----------------------------------
If the trait is a :ref:`resource node <node-type-resource>`, then the resource
can drop below ``rank 1`` when spent. However, the trait will recharge back up
to ``rank 1`` after **8 hours of rest**.

.. _rules-skill-checks:

============
Skill Checks
============
Whenever a character attempts a task that might fail or have some side-effect,
the GM can ask them to make a “skill check”.

Rolling for Checks
==================
When making a skill check, the player will build up a **dice pool** by combining
at least one :ref:`Attribute Node <character-attributes>`, with any number of
skill or ability nodes. Every node added to the pool must relate to check in
some way.

Every node has a rank (or level), and each rank has an equivalent dice.

.. csv-table::
   :header: Rank/Level, Dice

   0, 1d4
   1, 1d6
   2, 1d8
   3, 1d10
   4, 1d12

Here is a common set of steps for a typical skill check:

1. **The GM chooses a difficulty for the check.**
   The normal difficulty range is ``1-10``. For especially difficult tasks, the
   DC can go to ``11-12`` (or higher if you are using the advanced power scaling
   rules).

   .. csv-table::
      :header: Difficulty, Description
      :widths: 1,3

      1, "A simple every day task, like walking up the stairs."
      2,
      3, "Task requires a bit more focus but still easy on average."
      4,
      5, "Hard, but not unattainable for those with novice training"
      6,
      7, "Hard, but not unattainable for those with adept training"
      8,
      9, "Hard, but not unattainable for those with master training"
      10,
      11, "Hard, but not unattainable for grand-masters"
      12, "Even grand-masters struggle to attain this level consistently"

2. **The player tell the GM which attributes and skills they want to add to
   their dice pool.** GM and player need to agree that the skill choices
   contribute to the check in some way.

3. **The player rolls the entire dice pool, and keeps the highest result.** If
   the player rolls exactly the target difficulty, it will count as a success.

4. **(Optional) The player rolls a separate 1d20 (Drama Dice).** This roll will
   determine the drama outcome for the skill check. Drama only happen when the
   player rolls either ``1 or 20`` on Drama. See :ref:`rules-drama` for details.

   .. tip::

      You can roll the drama dice together with your main dice pool to save
      time, but make sure you don’t accidentally use it as part of your dice
      pool result.

5. **The GM describe the result,** based on the roll success or failure, and
   the outcome of good or bad drama.

.. _rules-boons-penalties:

Boons and Penalties
===================
.. todo:: *“Boons” are pending a re-name*

   Currently, the term “Boon” and it’s intended use here, is confused with the
   actual boons that exist as intentions.

   We need to rename either this mechanic here, or use a different name for the
   boons listed in the :doc:`intentions <intentions>` document.

Boons & penalties affect a character’s dice pool in either a positive or
negative way.

* Lesser Boon = Add a ``1d8`` to your dice pool – **Keep Highest Result**
* Greater Boon = Add a ``1d12`` to your dice pool – **Keep Highest Result**
* Lesser Penalty = Add a ``1d8`` to your dice pool – **Keep Lowest Result**
* Greater Penalty = Add a ``1d4`` to your dice pool – **Keep Lowest Result**

**Boons and penalties do not stack,** but they do cancel each other out.
So you can never have more than one boon or penalty per roll.

**Greater** boons or penalties always cancel out **lesser** boons and penalties.

.. _min-difficulty:

Minimum Difficulty
==================
Mundane tasks like walking or running don’t really have a minimum difficulty,
and PC’s will generally succeed in these tasks, provided there aren’t obstacles
making the action harder.

However, with specialized skills (skills that require special training), there
is always a minimum difficulty of ``DC3``.

For example, a PC uses *Marksmanship* skill to shoot at an opponent. Lets assume
the target don’t have any actions to dodge or evade the attack. The player rolls
``Awareness › Marksmanship`` with a result of ``2``. This attack will still
miss, because the PC failed to roll the minimum difficulty of ``3``.

.. _fixed-med-max:

Fixed Rolls & Difficulties
==========================
The following are special terms used to indicate that a roll has a
**fixed difficulty** based on the **maximum roll potential** for a skill,
action or dice pool.

**Fixed-Medium** refers to exactly *half* of the maximum number possible for the
skill/action dice pool.

So if the dice pool is ``1d6 › 1d10 › 1d8``, then the **fixed-medium** for the
pool is ``5``. Exactly half of the maximum roll potential.

**Fixed-Maximum** refers to the *maximum* number possible for the skill/action
dice pool.

**For example,** if the skill pool is ``1d6 › 1d10 › 1d8``, then the
**fixed-maximum** for the pool is ``10``, the maximum of your roll potential.

.. _rules-intentions:

Intentions
==========
Characters perform actions with a specific goal, outcome or intent in mind. In
the Nemron system we express this with our “intentions” mechanic.

Intentions can be one of the following types;

* :ref:`Use Ability <intentions-abilities>`
* :ref:`Do Damage <intentions-damage>`
* :ref:`Apply Boons <intentions-boons>`
* :ref:`Apply Conditions <intentions-conditions>`

Different skills, weapons and items can grant the user access to different
intentions.

See the :doc:`intentions add-on <intentions>` for more information on how to
use intentions in your games.

.. _rules-action-sequence:

================
Action Sequences
================
An action sequence is a sequence of events where activities happen in a
pre-determined order.

An example action sequence is a combat sequence.

Action Sequence Steps:

#. Roll to determine initiative.
#. Character with highest initiative may take their turn first.
#. If a PC and NPC has the same initiative, the PC will always go first.
#. Once all PC’s and NPC’s took their turns, a new round will start.

.. _rules-initiative:

Initiative
==========
At the start of an action sequence, every character rolls initiative.

This is a **dice pool** consisting of ``Agility › Awareness › Sense``.
The character with the highest result goes first.

If PC’s have the same initiative as a NPC, the PC’s always go first.

.. tip::

   Drama is optional when rolling initiative.

.. _rules-actions:

Actions, Reactions and Bonus Actions
====================================
Every character has only **one action**, **one reaction** and
**one movement action** during their turn.

Characters may only use their **action** on their turn or, later in the
round if they previously :ref:`held their action <rules-held-actions>`.

Characters can only use **reactions** to something happening to them-self, or
something happening within their :ref:`melee range <rules-range-indicators>`.

Characters may only use their **movement actions** during their turn. Unlike
other game systems, you cannot use half your move, do something else, and then
use the rest of your movement. Once you stop your movement action to do
something else, that movement action is now spent.

.. Design NOTE: Why separate actions and reactions?

   The choice to have two kinds of actions was deliberate. It allows finer
   control for boons and conditions to affect either just actions or reactions.

Bonus Actions and Reactions
---------------------------
Sometimes characters can have a *bonus* action or reaction.

This is usually provided by a boon, trait, skill or
:ref:`when dual wielding <combat-dual-wielding>`.

Note that the distinction between *action and reaction* is important here.

.. _rules-held-actions:

Held Actions
------------
On their turn, an actor can choose to hold their action. Doing this, they can
use their action at any point in the event sequence.

.. _rules-action-intervals:

Action Sequence Intervals
==========================
During an action sequence, some abilities or events may repeat at a specific
interval or cycle. Intervals can repeat in the following ways.

Turn:
   Happens at the *end of every turn*. This happens regardless if the character
   actually does anything during their turn, even if they choose to hold their
   action.

Round:
   Happens every round, usually at the same initiative as when the
   action, ability or event started.

.. _rules-combat:

======
Combat
======
Combat is an :ref:`action sequence <rules-action-sequence>`, so all participants
need to roll initiative at the start of combat. Make sure you understand the
:ref:`action sequence <rules-action-sequence>` basics before going on with this
section.

Attack, Defense and Damage
==========================
When a character makes an attack roll, they add any relevant dice to their pool
for the attack. This includes attribute(s), related skill(s), and weapon rank.

   *For Example:*

   Brandon attempts to shoot an enemy with his pistol.
   He’s dice pool might look something like this:

   ``Awareness › Marksmanship › Pistol``.

**Your attack roll is also the actual amount of damage you will do.** If your
opponent took a defensive action against your attack roll, then the damage is
reduced by their defensive rice roll.

   Brandon rolls ``6`` for his attack, so he will do ``6`` damage.

   The enemy attempted to dodge behind a boulder though, and they rolled ``3``
   for their action.

   So the total damage they receive while attempting to dive behind the boulder
   is ``3``.

As you can see above, we use an “active defence” mechanic, meaning a character
must actively spend an action or reaction to make defensive dice rolls against
an attack.

Defensive actions can include evasion, parrying, blocking etc. Characters make
these rolls just like any other roll.

Damage to limbs
---------------
Damage dealt must go to one or more limbs.

**If the attacker doesn’t specify which limb they are targeting,** then they
will roll a **location dice** to determine which random limb will receive
damage.

**An attacker can target a specific limb.** To do this, they have to tell the GM
*before making their attack roll* because the attack roll will have a
*lesser penalty*. In this case damage will go to the targeted limb without
having to roll the location dice.

Area of Effect (AOE) Damage
---------------------------
AOE damage will damage more than one location for each target inside the *AOE*.

Roll a number of location dice equal to the amount of AOE damage received. Each
dice represent **one damage** to a specific location.

*For example:*

* An enemy attacks Darren with a fireball.
* They roll ``7`` for their attack.
* Darren attempts to evade with a roll of ``3``
* He will now receive ``4 AOE Damage``.
* The GM rolls ``4d6`` (one dice for each point of damage), and gets the
  following results: ``5, 5, 3, 2``

This translates into ``2`` damage to the torso, ``1`` damage to the right leg
and, ``1`` wound to the right arm.

Minimum Weapon DC
-----------------
When using a weapon there is always a base difficulty for the attack roll. This
base difficulty is equal to the ``weapon rank + 1``.

So a ``Rank 2 Pistol`` for example will have a minimum difficulty of ``DC3``. So
you have to roll ``3 or higher`` for the attack to be successful.

Failing the base difficulty, will result in an automatic miss or failed attack.

Flanking Opponents
==================
When a character is flanked by opponents, they will find it harder to defend
against those opponents. The flanked character will have the condition,
:node:ref:`Generic.Condition.Flanked` until they are no longer flanked.


.. _rules-range-indicators:

================
Range Indicators
================
Many actions, abilities and skills will have a range, distance or reach. We
identify these ranges as, Melee, Near, Far and Distant.

.. csv-table::
   :header: Range, Classifier, Description
   :widths: 20, 10, 70

   < 3 meters, Melee, Within a single lunge or swing of your weapon
   3 to 8 meters, Near, Within movement range
   8 to 30 meters, Far, Outside of movement speed
   30 to 100 meters, Distant

.. _rules-falling-damage:

==============
Falling Damage
==============
Falling damage count as damage, where;

**The damage type** depend on the surface that you collide with. The following
damage types are just guidelines. The GM should feel free to bend these rules as
fit the scenario.

* Water, sand dunes, grass or flat ground: :node:ref:`Generic.Damage.Kinetic`
* Concrete or smooth rock: :node:ref:`Generic.Damage.Crushing`

**The damage amount** depend on how far the character fell before they
collide with a surface. The damage is 1 point for every 3 meters fallen, and
counts as AOE damage.

So a 15-metre fall onto flat ground will result in **5 damage** and could
potentially also apply :node:ref:`Generic.Condition.Dazed` or
:node:ref:`Generic.Condition.Stagger` conditions (the extra conditions up to the
GM based narrative flair).

Falling damage ignores armour, unless the armour specifically protects against
it.

.. note::

   We intentionally use a linear scale for falling damage. The reason for this
   is that we want to favour ease-of-use, over super realism.

.. _rules-drama:

==============
The Drama Dice
==============
This is an *optional mechanic*, but when used, it can make for more interesting
games.

   Imagine your character is falling off a cliff and your belt hooks on a tree
   branch, saving you from the inevitable fall to death.

   Or, you have to shoot a charging bull. It’s an easy shot, and it will land the
   final blow to bring the beast down; But your gun misfire.

The drama dice introduce this kind of randomness to all rolls.

With *every roll*, players also roll an extra ``1d20``. This is their “drama
dice”.

This extra dice is not part of the dice pool, and does not contribute to
your roll. **It only determines the outcome of drama**.

Drama only happens when the players roll ``1`` or ``20`` on their drama dice.

**1 on Drama (Bad Drama):** The action automatically fails or, the GM may decide
on something dramatic that has some negative outcome.

**20 on Drama (Good Drama):** The character gain :node:ref:`1 point of Luck
<Generic.Boon.Luck>` or, the action result in an automatic success or, the GM
may decide on something dramatic that has a positive outcome.

.. tip::

   Drama outcomes are ultimately in the hands of the GM, but we would caution
   *not* to make things feel unfair.

   The purpose of drama is to inject random, high impact, and *memorable events*
   into the scene. Something that players will talk about weeks later.

   *Drama should never make players feel punished for something they have no
   control over.*

.. _rules-drama-example:

Drama Dice Example
==================
Orrian tries to shoot the opponent standing next to her friend Rhalf. She rolls
really high for her attack, a good shot; But she rolled **bad drama**.

The GM realize there are a couple of options that can add a dramatic flair to
this scene:

#. Orrian hits Rhalf by accident. A perfectly acceptable outcome, but maybe
   not dramatic or unique enough. *Moreover*, it doesn’t reflect the fact
   that she had a good attack roll to begin with.
#. Since Orrian had a good attack roll against the opponent, the GM factors this
   into his drama decision.

   GM decides that Orrian will hit the intended target as normal, but that the
   shot hit a grenade attached to the opponents’ belt. The grenade explodes,
   damaging everyone around him as well.

   This is a dramatic outcome and can be devastating to Rhalf and the team.
#. The second example may be too devastating for the team, so GM may decide that
   the shot hit the enemy, but that the blood from the wound splatter into
   Rhalf’s eyes, temporarily giving him
   :node:ref:`obscured vision <Generic.Condition.Dull>`.

.. include:: /content-footer.rst
