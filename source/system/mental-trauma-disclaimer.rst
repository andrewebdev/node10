:orphan:

.. note:: **Disclaimer**

   Real world mental trauma is serious and, should never be treated jokingly,
   or haphazardly.

   If you decide to use the sanity rules in your games, then make sure your
   players are OK with using these rules. Also make sure that they are aware
   that certain sensitive topics may come up during play.

   **Tools**

   It may be handy to have tools in your game to help deal with sensitive
   topics. I personally like the
   `X-, and O-Card System <https://docs.google.com/document/d/1SB0jsx34bWHZWbnNIVVuMjhDkrdFGo1_hSC2BWPlI3A/edit?usp=sharing>`_.
