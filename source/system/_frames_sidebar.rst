.. sidebar:: Frame, Race or Species?

   Whenever we use the word “Frame” you can substitute it for “Race” or
   “Species”.

   We prefer to emphasize that the body or “frame” is just a vessel for a
   character’s mind (or soul, depending on the game setting).

   Many fantasy worlds have a concept of a soul that can change or inhabit
   different bodies or “frames”.

   Mechanically, races and frames are the same thing and the principle on how
   they interact with the game system is the same.

   This is the reason we use the term “frame” over race.
