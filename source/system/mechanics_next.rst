.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: |nem| Mechanics Next

.. include:: /global-message.rst

==============
Mechanics Next
==============
.. warning::

   **You can ignore this section**

   This section is just a small test-bed for new mechanics or potential changes.
   Sometimes we will write some preliminary notes for some edits we will be
   making in future.

   In general you can ignore this section, but feel free to skim through it to
   see if anything inspires some creative ideas.

   This is also a good time to remind you that you can
   :ref:`contribute to the system and rules <contribute>`, so if you see
   anything of worth in this section, please let us know!

.. warning::

   **For testing only**

   This page contains mechanics that should be considered alpha. They are not
   yet stable and consist of new rules that we are in the process of testing.

   This document might be a bit more technical than the core mechanics,
   because we have to discuss and think about the technical implication for
   these rules.

   Due to this we often write a lot of technical jargon, and sometimes just
   quickly jot down ideas as we evolve the rules.

New Roll Mechanics
==================
Players from most systems may feel comfortable with a system where everyone,
including GM's, roll for every skill, counter and resistance rolls
(saving-throws). The |nem| system do however have a unique advantage in that we
can use it in 3 different ways.

#. Legacy rolls (everyone rolls for all characters).
#. Player only rolls.
#. Hybrid rolls.

The hybrid system is the best middle ground to speed up combat.

.. note::

   **After review** hybrid felt weak. The NPC's for which we didn't roll, felt
   very weak and they didn't accomplish much. This may be due to the older AP
   based actions, and players taking advantage of quick simple actions.

Breath Hold and Suffocation
===========================
We need to review the rules on breath-hold and suffocation when we have some
more time to properly test it.

Zero G Movement
===============
.. todo:: **Coming Soon**

   We will expand on these rules at a later stage (When we start work on Book 2
   of the tutorial.)

   For the time being GM’s will decide this till we have proper rules in place.
   Use the text below as a guide to decide how they want to deal with this in
   the meantime.

When in zero G, your momentum will keep carrying you in the direction you
first started the movement. This kind of movement doesn’t cost initiative.

You can take actions while moving like this, but depending on the speed you are
moving at you may have a *lesser/greater penalty* to actions.

Certain items can assist you when moving in Zero G, like Hazardous Environment
Suits which have its own propellant.

.. todo::

   **Zero G Items to be created**

   We have some special items in mind that is fit within our sci-fi universe and
   story.

Skill Range
===========
Can we create a single rule that will make skill range act consistently for
all skills? We need to reduce the need to look up skill rules for things like
range for example.

Generic Skill Recipes and Templates WIP
=======================================
These are any generic recipes and skill templates that we are not yet happy
with. They have some basic rules but may be un-balanced, badly described/worded,
or just not simple enough to use etc.

|

.. node:def:: Teleport
   :uid: Generic.Ability.Teleport
   :type: Ab

   | **Energy Cost:** 3
   | **Restrictions:** Melee attacks only.
   | **Attribute(s):** Control
   | **Usage:** Control

   Teleport to the target location.

   Tether your energy to the target location. Once the link is active, you
   instantly teleport to the target.

   The teleport range (in meters) depend on the ability rank,
   :math:`{Range} = {Rank} \times 5` (this is essentially the same as the
   tether template).

      **Recipe:**

      ``Tether Template (3 E) › Magic/Quantum Physics/etc. › Intent: Teleport``

|

.. node:def:: Multiply
   :uid: Generic.Ability.Multiply
   :type: Ab

   :Energy Cost: … todo …

   .. todo:: *We need multiplication rules*

      We need either another template, ability or special rule that will allow
      the channel to be extend to minutes or hours, rather than just *rounds*.

      This will allow for a more systematic way to create recipes or abilities
      that can last for longer periods, or have larger ranges.

      The current thought is that you can *multiply* the duration by pouring
      more energy into the skill.

      The following is a *rough* idea of what we need:

      * Use ``10 Energy`` to change the duration from *rounds to minutes*
      * Use ``50 Energy`` to change the duration to *hours*
      * Use ``1000 Energy`` to change the duration to *days*

      The above is just for duration, we need something similar for range also.

|

.. node:def:: Automata
   :uid: Generic.Ability.Automata
   :type: AB

   .. todo:: **Work in Progress**

      Create an automaton like a elemental.

      We still need to expand on these rules because there is a lot happening
      here. But don't worry about it till we actually need it for some in-game
      requirement or module etc.

      We had some old rules in a older version of the system, but it was
      cumbersome and, incompatible with the new system.

      See, :node:ref:`Old.Ability.FireElemental` for an old example of this.

|

.. node:def:: Seismic Wave
   :uid: Generic.Recipe.SeismicWave

   :Energy Cost: 3 + *Seismic Wave Rank*
   :Decay: See the wave decay on :node:ref:`Generic.Ability.Wave`
   :Uses:
      :node:ref:`Generic.Ability.Shape`,
      :node:ref:`Generic.Ability.Wave`

   This skill will create a :node:ref:`waves <Generic.Ability.Wave>` of
   shaking and unstable ground in the target direction.

   For every wave ripple that hits a character (includes allies), that character
   needs to make a *agility resistance roll* against the
   :ref:`fixed-medium <fixed-med-max>` of the wave rank.

   Remember that as per the :node:ref:`Generic.Ability.Wave` template rules,
   the wave rank decays by ``1 rank`` *per action*.

   Failing any one of the resistance rolls will give the target the
   :node:ref:`Generic.Condition.Stagger` condition.

   **Recipe**

   .. mermaid::
      :align: center

      flowchart LR
         Control --> WT[/Wave/] --> CT[/Cone/]
         CT --> |using| Geology
         CT --> |or using| ED[Earth Domain]
         Geology & ED --> Stagger{{Condition: Stagger}}

   |


.. _nemron-interface-pre-loading:

Pre-Loading A Interface
=======================
.. todo:: *Review Required*

   Review this mechanic and refine and optimize. The mechanics below are relics
   from older version of the system and is quite complicated.

   Now that we have more consistent and balanced core mechanics, we should
   review the rules below.

An example of how this be used, is for weapons or items that create a specific
effect on hit.

#. The interface must have at least *2 Energy* left in the battery.
#. A codex that contains the skill being loaded must be plugged in to the
   interface.
#. User must have the minimum Technomancy requirements (see the skill
   requirements above).
#. User rolls for the skill as normal, and spend their own energy as normal.
#. Now also spend 1 Energy from the interface battery.

Skill is now pre-loaded into the interface. Every invocation from this point
on will spend the required skill Energy from the interface battery, instead
of the user.

Once the interface is loaded with a skill, it will always use the same skill
when invoked, *without* needing to roll or pre-load the skill again.

An interface *will reset and forget* the pre-loaded skill when;

* The battery drops to zero.
* The codex containing the skill is removed.
* A technomancer or engineer resets the interface.

Specialized Technomancy Abilities
=================================
.. todo::

   These are skills from an earlier version of the system. They need some rule
   review, but we will keep them here for reference, so be aware that the rules
   for these skills have not yet been revised.

.. node:def:: Conduit
   :uid: Generic.Ability.Conduit
   :type: Passive, Effect

   Any item or actor with this ability, can function as a conduit, meaning
   that anyone with permission, can channel energy through the owner of this
   effect.

      **Example:**

      Darren Govan channels *flame strike* through his **sentry drone** (which
      has the conduit effect). This would mean that the pulse originates from
      the drone, rather than Darren.

   There maximum range that a conduit can be from the operator is the
   :ref:`fixed maximum <fixed-med-max>` (in meters) of this ability.

.. node:def:: Energy Transfer
   :uid: Generic.Ability.EnergyTransfer
   :type: Active

   :Type: Channel
   :Range: 5m

   Grants the user the ability to channel energy to and from other individuals.
   This skill has offensive and utility use.

   .. todo:: **Rework/Review Required**

      I really like this ability, and some of the mechanics behind it. But we
      need to see if we can simplify some of the rules a bit so that we can get
      the same results, with fewer rolls required.

      Ideally, we would only want this skill to be rolled once, and the target
      to make a resistance roll once.

   **Offensive Use**

   Offensively this skill can drain energy from the target, to replenish the
   user’s own energy. This requires 2 Action rolls.

   :Action 1 (Sense) — 1 Energy:

      Detect weaknesses within the target’s mind. Target needs to make a
      *willpower resistance roll*. If the target succeeds their resistance roll,
      this connection will fail and you cannot proceed to “Action 2” below.

   :Action 2 (Focus) — Free Cost:

      This roll determines the amount of energy that you can channel through the
      connection. You cannot make this roll if the connection failed in the
      first Action.

      The target can make a *focus resistance roll*.

      The amount of *energy drained* from the target is the amount left over
      after the resistance roll.

      .. math::

         {TargetEnergyDamage} = {EnergyTransferRoll - ResistanceRoll}

         \\

         {UserEnergyConsumed} = \lceil{\frac{TargetEnergyDamage}{2}}\rceil

   **Utility Use**

   As a utility this skill can transfer energy to or from willing participants.
   No resistance rolls needed for willing participants.

   :Action 1:

      * Roll: :roll:`Focus Technomancy.EnergyTransfer InterfaceMod`
      * Energy Use: **Energy 1**

      You focus and open the connection for participants to contribute towards.
      The level rolled is the maximum number of participants that can contribute
      to the connection.

   :Action 2:

      * Roll: :roll:`Control Technomancy.EnergyTransfer InterfaceMod`
      * Energy Use: **None**

      Channel the energy from the targets to yourself. The level rolled is the
      maximum energy that the character can receive per round per participant,
      while the connection is open.

   :Participant Action:

      * Participant Roll: :roll:`Control`

      Participant makes a straight control roll to determine the maximum amount
      of energy they can send to the receiving user. The amount they want to
      share is still up to them, but may not exceed this total.

   :Interrupting the Connection:

      Damage and some skills can interrupt the connection. Skills will have
      their own rules regarding this, but in general a character would need to
      make a focus resistance roll to keep the connection open and transfer
      energy.

.. node:def:: Fire Elemental
   :uid: Old.Ability.FireElemental
   :type: Active

   :Type: :node:ref:`Generic.Ability.Automata`
   :Duration: 5 minutes

   .. todo:: **Rule Change Required**

      Review and update the skill mechanics now that the system changed.
      Everything below is temporary

   Create a automated construct forged from the material related to the energy
   module used in your technomancy interface. A Fire module will create a fire
   construct.

   Two actions are required to create the construct and will cost a minimum of 5
   energy to invoke this program.

   * The Control roll determines the Body level of the construct.
   * The Sense roll determines the Mind level of the construct.
   * The construct can only ever have 1 action, no matter the level
   * The construct is under your control, and you need to spend ``1 initiative``
     to command the construct.
   * The construct has its own initiative in combat.

   There are some special circumstances where a user might lose control over
   their construct. To resist losing control the user needs to roll Focus vs the
   difficulty set by the GM.

----

.. include:: /content-footer.rst
