.. -*- coding: utf-8 -*-

.. |nem| replace:: Nemron RPG

.. title:: Extended Rules - Nemron RPG

.. include:: /global-message.rst

.. _extended-rules:

##############
Extended Rules
##############

.. todo::

   Write about the rules for speeding up combat by using mediums for simple
   enemies. Note however that we would need for those enemies to account somehow
   for *lesser and greater boons and penalties*.

=======================
Extended Core Mechanics
=======================

.. _scaling-node-ranks:

Scaling Node Ranks
==================
While not immediately obvious, nodes can rank up higher than level 4. The way
this works, is that every additional rank gives a +1 modifier to that node.

So if someone has **Power** on **rank 4+1**, it means that the player can roll
``1d12`` and then have a +1 modifier to the roll.

A couple of things to note here:

* DC’s for rolls can now go higher than 12.
* **Modifiers do not stack**. Only the highest modifier can add to the final
  result.

Tweaking Drama Chance
=====================
By default we use a ``d20`` for the drama dice, but you can opt to use a ``d12``
or a ``d10``. Using a lower dice will make high and low drama rolls happen more
often.

If you decide to tweak the drama system in this way, make sure that all the
players understand and don’t mind the implications.

Since drama *does not count toward your roll result*, it would be good to use a
different colour dice. This avoids confusing your Drama Dice with your other
dice in your pool.

Energy Use
==========
If a character lose or use all their energy, they will get
:node:ref:`1 stack of exhaustion <Generic.Condition.Exhaustion>` until they
recovered some energy again.

.. _rules-casting-time:

Casting Time
============
.. warning:: **Notice!**

   Casting time rules will soon be re-visited for a rework. See,
   `this issue <https://gitlab.com/andrewebdev/node10/-/issues/44>`_ for more
   details and status on this.

The amount of energy used for an action will determine how long it takes to
activate the ability or action.

It takes ``1 Turn`` (in an event sequence) for every point of energy used,
regardless of the AP Cost of the action.

So if Darren uses **Flame Strike** (2 Energy), it will have a casting time of
``2 Turns``. Activating the skill counts as the first turn.

When the *next character* in the event sequence finish their turn, Darren’s
flame strike will activate (provided that Darren isn’t interrupted before it
activates).

.. _rules-skill-upkeep:

Concentration and Skill Upkeep
==============================
Some actions may require constant concentration to keep it active. A player can,
for example, use the :node:ref:`Generic.Ability.Amplify` to keep a
“Frost Ray” active, so long as they spend the required *energy* to do so.

Whenever a skill’s duration is about to end, the character can choose to
:ref:`refresh <rules-refresh-ability>` it.

The total number of effects that a character can maintain this way, depends on
the upkeep attributes (Constitution, Willpower or Focus).

.. _rules-refresh-ability:

Refreshing a Skill or Ability
=============================
At the end of a skill or ability’s duration, you can choose to refresh it. When
doing this, the following rules apply:

* To refresh a skill you have to use an action ``1 AP``.
* **You do not** have to roll again.
* You have to spend the *same amount of energy*, as when the skill was first
  activated.

.. _rules-breaking-concentration:

Breaking Concentration
----------------------
When taking any direct damage (or some other event determined by GM), you have
a chance of losing your concentration, and thus one or more of the abilities
you are concentrating on.

The concentration difficulty is the :ref:`fixed-medium <fixed-med-max>` for the
*upkeep attribute* of the skill in question.

Failing the difficulty, you will lose concentration on the last activated upkeep
skill.

:ref:`Bad drama <rules-drama>` will cause you to lose concentration on *all*
upkeep abilities.

Intentions
==========

.. _intentions-opposing-effects:

Opposing Effects
----------------
Some intentions may potentially counter others.

For example the :node:ref:`Generic.Condition.Crippled` condition has an opposing
effect, :node:ref:`Generic.Boon.Mobility`. This means that if the PC has **3
Stacks of Crippled**, giving them **2 Stacks of Mobility** can potentially
remove two of the crippled stacks.

Remember the GM can still change the ruling regarding the opposing effect, based
on the narrative.

=====================
Extended Combat Rules
=====================

.. _rules-extended-distraction:

Distraction
===========
Players can create distractions to impose a penalty on an attacker, but there
are a couple of rules that need satisfied:

* During an event sequence, the distraction can only happen as a *reaction*.
* Player rolls **Control** as the base attribute and may add any other relevant
  nodes based on how they are attempting to distract the target.
* The roll difficulty is the :ref:`fixed-medium <fixed-med-max>` for the
  target **Focus** resistance pool.
* If successful, the target get a *lesser penalty* to their attack roll.

Some items could also assist with the distraction, like the flash of a camera,
fireworks etc. Using an item in this way, the player will get a *lesser boon* to
their distraction roll.

.. _simultaneous-attack-rolls:

Simultaneous Attack Rolls
=========================
When a character is reacting to an attack against them, they can choose to
counter-attack instead of using a defensive action.

In this case both characters make contested attack rolls. The character with the
higher roll wins the contest.

.. _untrained-skill-rolls:

Untrained Skill Rolls
=====================
Characters can use skills without training, but the following rules apply:

* If the skill doesn’t require special training (like evading an attack, or
  running up stairs), then the player just roll the most applicable attribute
  node.
* If the skill requires specialized training, then the player rolls the
  most applicable attribute node, with a *greater penalty*.

.. _contested-rolls:

Contested Rolls
===============
If two or more contested rolls occur, and characters roll the same amount,
then follow these rules:

#. When both of the actors have the same boons, the player characters will
   *always win* vs. NPC’s.

#. Actors with a *greater boon* will win the contest over an actor that has a
   *lesser boon* or no boons.

#. Actors with a *lesser boon* will win the contest over an actor that has no
   boons.

#. Actor that used more nodes in their pool will win.

#. When none of the actors have any boons, and they have the same number of dice
   nodes in the pool, then the player characters will *always win* vs. NPC’s.

**The winning side will always count as succeeding by 1 point**.

.. _rules-grappling:

Grappling Rules
===============
Every character in a grappling contest:

* *Body* rolls that don’t apply to the grapple will have a
  :ref:`greater penalty <rules-boons-penalties>`.
* *Mind and Spirit* rolls will have a
  :ref:`Lesser Penalty <rules-boons-penalties>`.

Players can attempt to break free from a grapple with any valid node
combination, like ``Power › Grappling``. Skills like *Contortion* or
*Acrobatics* may also prove useful.

.. _rules-ranged-into-melee:

Ranged combat into Melee
========================
When a target is within melee range of your allies, the roll will have a
:ref:`lesser penalty <rules-boons-penalties>`.

If the target is actively in a :ref:`grappling <rules-grappling>` contest with
an ally then the roll will have a
:ref:`greater penalty <rules-boons-penalties>`, with a ``50%`` chance to
hit your ally.

.. _rules-ranged-point-blank:

Point Blank shot vs. Melee Target
=================================
When attacking an enemy :ref:`within your melee range <rules-range-indicators>`,
you will get a :ref:`lesser penalty <rules-boons-penalties>` for any ranged
attacks against that target.

:node:ref:`Generic.Boon.PointBlank` can remove this penalty.

.. _combat-dual-wielding:

Dual Wielding
=============
When dual wielding, you get one bonus action during your turn. This bonus action
will have a *lesser penalty*.

Ranged Targets beyond the maximum distance
==========================================
Shooting at targets beyond the maximum effective range, will reduce the attack
roll by ``1 point for every 10 meters`` beyond the maximum weapon range.

==========================
Extended Adventuring Rules
==========================

.. _rules-armour:

Armour
======
The purpose of armour is to absorb damage. It’s not a necessity but can greatly
increase a character’s chances of surviving an attack. At the same time, the
armour can also reduce freedom of movement.

The **armour rank** is the total amount of damage that the armour can absorb.

.. todo::

   Add rules for the requirements to wear higher ranking armour. For example, to
   wear a rank 2 chainmail, the PC would need **Power** on rank 2 as a minimum
   requirement, if not they will get a penalty of some kind.

Suffocation
===========
.. todo::

   Add conditions and/boons related to suffocation?
   Maybe suffocation should be a condition?

A character needs to make a constitution roll to determine the amount of “air
units” they can pack into their lungs and airways.

For every round that the character cannot breathe, subtract 1 unit of air.

When the character has 0 air units, they will start to lose 2 points of energy
per round. When the character’s energy reach ``0``, they will lose consciousness
and get 1 point of spirit damage per round.

If a character prepare by taking a deep breath, they do *not need to make* the
constitution roll. Just use the maximum amount that he can possibly roll.

Example 1 — Preparing a breath-hold
-----------------------------------
Darren prepares to swim under water. He takes a deep breath. He has a
constitution of ``2 (1d8)`` which has a maximum roll of ``8``.

So he can swim underwater for *8 rounds (~30 seconds)* before starting to lose
energy.
