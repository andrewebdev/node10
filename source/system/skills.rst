.. -*- coding: utf-8 -*-

.. title:: Skills

.. include:: /global-message.rst

##################
Skills and Domains
##################
This section covers the basic and advanced skill rules, and provides a list of
generic skill nodes that should work as-is in most game settings and genres.

**When making skill checks,** you can add multiple skill nodes to your dice
pool so long as you can justify to the GM the reason for the combination of
skills you are adding to a dice pool.

.. _skills-common-list:

==========================
List of Common Skill Nodes
==========================
The skills listed in this section are some examples that you can use in your
game settings.

Academic Skills
===============
* Accounting
* Architecture
* Astronomy
* Biology
* Botany
* Chemistry
* Cryptography
* Economics
* Engineering
* Geology
* History
* Law
* Literature
* Medicine
* Physics
* Kinetics
* Psychiatry
* Structural Engineering
* Systems Engineering

Acumen
======
The ability to make good judgements and take quick decisions.

* Investigation
* Command
* Insight
* Persuasion
* Inspiration
* Intimidation
* Tactics

Art and Creativity
==================
* Crafting
* Dance
* Drama
* Music
* Painting
* Graphic Design
* Creative Writing

Construction and related fields
===============================
* Carpentry
* Metalworking
* Construction
* Plumbing
* Electrician

Athletics
=========
* Acrobatics
* Balance
* Climbing
* Contortion
* Running
* Swimming
* Wrestling

Subterfuge
==========
* Deception
* Disguise
* Lipreading
* Slight Of Hand
* Stealth

Survival
========
* First Aid
* Foraging
* Survival
* Tracking
* Trapping

Environmental Biomes
====================
* Desert Biome
* Forest Biome
* Jungle Biome
* Tundra Biome
* Marsh Biome
* Mountain Biome
* Plains Biome
* Wasteland Biome
* Asteroid Biome

Miscelaneous
============
* Animal Handling
* Drone Operations
* Driving
* Piloting
* Sensors

.. _skills-domains:

===========================
Domains and Specialisations
===========================
Domains are special skill domains that give characters access to special
abilities and feats. Characters have access to these domains via special
training, specialisation or as a favour granted by a supernatural entity or
force.

Mechanically domains work like skills, you add it to your dice pool when you are
using the domain.

Characters can have access to more than one domain, so long as this makes sense
for the character background and the game setting.

Think of the domains listed here *not* as fixed rules, but more as a *guide* or
blueprint, something you can change to suit your game worlds, genre and
settings.

.. _skills-domain-features:

Domain Features
===============
Every domain will give the character access to new *Abilities*, *Damage Types*,
*Boons* and/or *Conditions*. We will call these **Domain Features**.

Characters can buy **Domain Features** with **XP**, within the following
limitations:

* You cannot have more domain features than the **domain rank**.
* You can only purchase **one feature per month (in-game)**.
* Every domain feature will cost **2XP** to train.

.. _skills-domain-features-activate:

Activating or Using Domain Features
-----------------------------------
The following common rules apply to activating or using domain features for any
domain.

Damage Types
   To use a specific damage type in the domain, you can just channel the damage
   type through **touch** or, use any domain ability that can apply the damage
   in some other way.

Abilities
   Every ability will has it’s own energy cost, rules and roll requirements.
   Read the Ability description to find out how the ability works.

   See :node:ref:`Generic.Ability.Shout` or :node:ref:`Generic.Ability.Pulse` as
   examples.

Boons and/or Conditions
   **Apply a boon to yourself** by rolling ``Focus › (Domain) » 1 Energy``,
   where ``(Domain)`` is the governing domain for the boon.

   **Apply a boon or condition to others** by rolling
   ``Control › (Domain) » 1 Energy``, where ``(Domain)`` is the governing domain
   for the boon.

   When granting boons or conditions to others, the character *must* have a
   mechanism to do so, usually through an ability.

   A Brawler for example, can use
   :node:ref:`a battle cry <Generic.Ability.Shout>` to grant all allies within
   earshot :node:ref:`Generic.Boon.Fury` or, they can use the battle cry to
   :node:ref:`Daze <Generic.Condition.Dazed>` opponents.

Traits
   Some Domains can have traits as features. The are just boons or conditions
   that are permanently available to the character. They *never decay*, and will
   always be on ``Rank 1``.

   If for some reason a trait is boosted with higher ranks/stacks from another
   source, then those extra stacks will decay as normal back down to ``Rank 1``.

Custom Domains
==============
**Creating your own custom domains** for your settings is simple.

Characters can pick any :doc:`intention <intentions>` as a domain feature, so
long as they can justify a narrative or rational reason as to *how/why* they
have access to that intention within the domain.

For example, it may be difficult to rationalise how you have
:node:ref:`Cold Damage <Generic.Damage.Cold>` as a *Fire Domain* feature.

.. tip::

   You can also convert plain :doc:`skills <skills>` into a domain of it’s own.
   This is between the GM and the player to figure out the details.

   The system is flexible in this way by design. This way, it’s easy to create
   something unique for players.

.. _skills-domains-list:

List of Generic Domains
=======================
.. node:def:: Brawler
   :uid: Generic.Domain.Brawler

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities
      :node:ref:`Generic.Ability.Shift`,
      :node:ref:`Generic.Ability.EvasiveMovement`,
      :node:ref:`Battle Cry/Shout <Generic.Ability.Shout>`

   Conditions
      :node:ref:`Generic.Condition.Dazed`,
      :node:ref:`Generic.Condition.Stunned`

   Boons
      :node:ref:`Generic.Boon.Empowered`,
      :node:ref:`Generic.Boon.Fury`

.. node:def:: Unarmed Combat
   :uid: Generic.Domain.UnarmedCombat

   .. tip:: Unarmed combat count as :ref:`combat-dual-wielding`.

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities
      :node:ref:`Generic.Ability.Shift`,
      :node:ref:`Generic.Ability.EvasiveMovement`

   Boons
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.FleetFoot`

   Conditions
      :node:ref:`Generic.Condition.Crippled`,
      :node:ref:`Generic.Condition.Dazed`

   Traits
      :node:ref:`Generic.Boon.CombatAwareness`

.. node:def:: Marksman
   :uid: Generic.Domain.Marksmanship

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities:
      :node:ref:`Generic.Ability.Disarm`

   Conditions:
      :node:ref:`Generic.Condition.Crippled`,
      :node:ref:`Generic.Condition.Bleeding`

   Traits
      :node:ref:`Generic.Boon.Deadeye`,
      :node:ref:`Generic.Boon.PointBlank`,
      :node:ref:`Generic.Boon.CombatAwareness`

.. node:def:: Melee Arms
   :uid: Generic.Specialist.MeleeWeapons

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities
      :node:ref:`Generic.Ability.Disarm`,
      :node:ref:`Generic.Ability.Shift`

   Boons
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.FleetFoot`

   Conditions
      :node:ref:`Generic.Condition.Crippled`,
      :node:ref:`Generic.Condition.Dazed`

   Traits
      :node:ref:`Generic.Boon.CombatAwareness`

.. node:def:: Guardian
   :uid: Generic.Domain.Guardian

   **Requires:** A handheld shield or buckler, or any defensive weapon that can
   act as a shield.

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities
      :node:ref:`Generic.Ability.Shift`,
      :node:ref:`Generic.Ability.Aura`,

   Boons
      :node:ref:`Generic.Boon.NaturalDefender`,
      :node:ref:`Generic.Boon.Resilience`

   Conditions
      :node:ref:`Generic.Condition.Dazed`,
      :node:ref:`Generic.Condition.Stagger`,
      :node:ref:`Generic.Condition.Stunned`

.. node:def:: Cantrips
   :uid: Generic.Domain.Cantrips

   Create temporary illusions and sounds.

   The distance at which you can create these effects depend on the amount of
   energy you use with this ability.

   After upgrading this skill into a domain, the following domain features
   become available for purchase.

   Abilities
      :node:ref:`Generic.Ability.Amplify`,
      :node:ref:`Generic.Ability.Illusion`,
      :node:ref:`Generic.Ability.Shape`,
      :node:ref:`Generic.Ability.Shift`

   Boons
      :node:ref:`Generic.Boon.Blur`

   Conditions
      :node:ref:`Generic.Condition.Confusion`,
      :node:ref:`Generic.Condition.Marked`

.. node:def:: Death Domain
   :uid: Generic.Domain.Death

   You have control over the death domain.

   Damage Types
      :node:ref:`Generic.Damage.Cold`,
      :node:ref:`Generic.Damage.Corrosive`
      :node:ref:`Generic.Damage.Toxic`,

   Abilities
      :node:ref:`Generic.Ability.Beam`,
      :node:ref:`Generic.Ability.Pulse`,
      :node:ref:`Generic.Ability.Shape`

   Conditions
      :node:ref:`Generic.Condition.Fear`,
      :node:ref:`Generic.Condition.Frozen`,
      :node:ref:`Generic.Condition.Poisoned`,
      :node:ref:`Generic.Condition.Weakness`

.. node:def:: Earth Domain
   :uid: Generic.Domain.Earth

   You can manipulate geology and earth. You can damage them by making rocks fly
   at them or smash into them from below. In the same way you can cripple or
   stagger them by making the ground shift break beneath your opponents.

   Defensively you can use this to create armour of rock and stone around
   yourself or allies.

   Damage Types
      :node:ref:`Generic.Damage.Crushing`,
      :node:ref:`Generic.Damage.Kinetic`

   Abilities
      :node:ref:`Generic.Ability.Manifest`,
      :node:ref:`Generic.Ability.Pulse`,
      :node:ref:`Generic.Ability.Shape`,
      :node:ref:`Generic.Ability.Wave`

   Boons
      :node:ref:`Generic.Boon.Resilience`

   Conditions
      :node:ref:`Generic.Condition.Crippled`,
      :node:ref:`Generic.Condition.Stagger`

.. node:def:: Fire Domain
   :uid: Generic.Domain.Fire

   You have the ability to create and manipulate fire or plasma.

   Damage Types
      :node:ref:`Generic.Damage.Heat`

   Abilities
      :node:ref:`Generic.Ability.Beam`,
      :node:ref:`Generic.Ability.Explode`,
      :node:ref:`Generic.Ability.Pulse`,
      :node:ref:`Generic.Ability.Manifest`

   Conditions
      :node:ref:`Generic.Condition.Burning`

.. node:def:: Glamour Domain
   :uid: Generic.Domain.Glamour

   Damage Types
      :node:ref:`Generic.Damage.Psychic`

   Abilities
      :node:ref:`Generic.Ability.Illusion`,
      :node:ref:`Generic.Ability.Manifest`

   Boons
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.Camouflage`,
      :node:ref:`Generic.Boon.Invisible`

   Conditions
      :node:ref:`Generic.Condition.Charmed`,
      :node:ref:`Generic.Condition.Confusion`,

.. node:def:: Protection Domain
   :uid: Generic.Domain.Protection
   :type: A

   Abilities
      :node:ref:`Generic.Ability.Amplify`,
      :node:ref:`Generic.Ability.Manifest`

   Boons
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.Resilience`,
      :node:ref:`Generic.Boon.Immunity`

   Conditions
      :node:ref:`Generic.Condition.Dazed`,

.. node:def:: Life Domain
   :uid: Generic.Domain.Life

   Abilities
      :node:ref:`Generic.Ability.Aura`,
      :node:ref:`Generic.Ability.Beam`,
      :node:ref:`Generic.Ability.Cleanse`,
      :node:ref:`Generic.Ability.Heal`

   Boons
      :node:ref:`Generic.Boon.Regeneration`

   Conditions
      :node:ref:`Generic.Condition.Exhaustion`,
      :node:ref:`Generic.Condition.Marked`

.. node:def:: Light Domain
   :uid: Generic.Domain.Light
   :type: A

   Abilities
      :node:ref:`Generic.Ability.Beam`,
      :node:ref:`Generic.Ability.Shape`,
      :node:ref:`Generic.Ability.Manifest`

   Damage Types
      :node:ref:`Generic.Damage.Holy`

   Boons
      :node:ref:`Generic.Boon.Valour`

   Conditions
      :node:ref:`Blind <Generic.Condition.Dull>`,
      :node:ref:`Generic.Condition.Marked`

.. node:def:: Shadow
   :uid: Generic.Domain.Shadow

   You have knowledge and control over darkness and shadow.

   Abilities
      :node:ref:`Generic.Ability.Aura`,
      :node:ref:`Generic.Ability.Manifest`,
      :node:ref:`Generic.Ability.Pulse`,
      :node:ref:`Generic.Ability.Shape`

   Boons
      :node:ref:`Generic.Boon.Camouflage`,
      :node:ref:`Generic.Boon.Invisible`,
      :node:ref:`Generic.Boon.DarkSight`

   Conditions
      :node:ref:`Blind <Generic.Condition.Dull>`,
      :node:ref:`Generic.Condition.Marked`

.. _skills-stances:

Combat Stances
==============
Combat stances are domain specialisations that are difficult to obtain
initially, but once you have them they can become a game changer on the
battlefield.

Stances are difficult to obtain for two reasons.

1. They always require that a character have a *prerequisite* skill on at least
   ``Rank 2``.
2. The character needs training and guidance from someone that mastered the
   combat form in question.

**Unlike other domains,** characters do not have access to all the stance
features all the time. Characters must first **use an action** “enter” or
“activate” the stance before the stance features become available.

If a player have more than one stance trained, they can only use one stance at
a time.

When looking at the stance examples below, notice that the majority of the
stance features are :ref:`node-type-trait`. This is the reason why stances are
so powerful, since they can instantly give the character a number of traits.

.. note::

   The stances below are some quick examples. We will add more over time, but in
   the meantime these will serve as examples on how to create your own.

.. _skills-stances-list:

List of Combat Forms and Stances
--------------------------------
.. node:def:: Aggressive Stance
   :uid: Generic.Stance.AggressiveStance

   | **Requirements:**
     Melee combat only. You must have a melee combat skill on at least
     ``Rank 2`` and, you must have the **Intimidation** skill on at least
     ``Rank 1``.
   | **Activation Required:** Stance features are not available until the
     character **uses an action** to activate the stance.

   When using this combat stance, your body language and movements are of such a
   nature that enemies within your melee range feel threatened, even when you
   don’t intend to attack them directly.

   Abilities
      :node:ref:`Generic.Ability.Shout`

   Boons
      :node:ref:`Generic.Boon.Resilience`

   Conditions
      :node:ref:`Generic.Condition.Fear`,

   Traits
      :node:ref:`Generic.Boon.CombatAwareness`,
      :node:ref:`Generic.Boon.Empowered`,
      :node:ref:`Generic.Boon.Ferocity`,
      :node:ref:`Generic.Boon.Fury`

.. node:def:: Butterfly Dance
   :uid: Generic.Stance.ButterflyDance

   | **Requirements:**
     Short Bladed Weapons only. Must have a related combat skill on at least
     ``Rank 2`` and, dancing, acrobatics or any other similar skill on
     ``Rank 1`` at minimum.
   | **Activation Required:** Stance features are not available until the
     character **uses an action** to activate the stance.

   You are light-footed and flow unpredictably between your opponents while
   taking deceptive and opportunistic strikes at your enemies.

   Abilities
      :node:ref:`Generic.Ability.OpportuneStriker`

   Traits:
      :node:ref:`Generic.Boon.AgileStriker`,
      :node:ref:`Generic.Boon.Ambidex`,
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.CombatAwareness`,
      :node:ref:`Generic.Boon.Mobility`,
      :node:ref:`Generic.Boon.Vigilant`

.. node:def:: Defensive Stance
   :uid: Generic.Stance.DefensiveStance

   | **Requirements:** When using this stance the character *cannot* make any
     offensive actions. This stance will end the moment you make any attack or
     offensive action.
   | **Activation Required:** Stance features are not available until the
     character **uses an action** to activate the stance.

   Focus your attention and awareness on your opponents to anticipate their
   attacks.

   Abilities
      :node:ref:`Generic.Ability.Aura`,
      :node:ref:`Generic.Ability.EvasiveMovement`

   Conditions
      :node:ref:`Generic.Condition.Taunted`

   Taits
      :node:ref:`Generic.Boon.Alert`,
      :node:ref:`Generic.Boon.Vigilant`,
      :node:ref:`Generic.Boon.Endurance`,
      :node:ref:`Generic.Boon.KeenSense`,
      :node:ref:`Generic.Boon.NaturalDefender`,
      :node:ref:`Generic.Boon.Resilience`,
      :node:ref:`Generic.Boon.Valour`,
      :node:ref:`Generic.Boon.Vigilant`

.. node:def:: Gun Kata
   :uid: Generic.Stance.GunKata

   | **Requirements:** Only one-handed pistol(s) (single or dual wielded)
   | **Activation Required:** Stance features are not available until the
     character **uses an action** to activate the stance.

   **Note:** While in the stance you have a *lesser penalty* to all melee
   actions.

   Your stance and movement while in this form have been carefully developed to
   make it harder for *ranged attackers* to hit you, while also increasing your
   own accuracy with pistols.

   When using this stance you get the following *accumulating* effects,
   based on the stance rank:

   Abilities
      :node:ref:`Generic.Ability.EvasiveMovement`,

   Conditions
      :node:ref:`Generic.Condition.Marked`

   Traits
      :node:ref:`Generic.Boon.Ambidex`,
      :node:ref:`Generic.Boon.Blur`,
      :node:ref:`Generic.Boon.CombatAwareness`,
      :node:ref:`Generic.Boon.Deadeye`,
      :node:ref:`Generic.Boon.PointBlank`,
      :node:ref:`Generic.Boon.Mobility`,
      :node:ref:`Generic.Boon.Vigilant`

.. node:def:: Shield Warden
   :uid: Generic.Stance.ShieldWarden

   | **Requirements:** Only usable with handheld shields or shield-like items
   | **Activation Required:** Stance features are not available until the
     character **uses an action** to activate the stance.

   | **Note:** While in this stance, you have a *lesser penalty* to any
     *offensive action* that doesn’t use your shield.

   Developed for those wielding shields to defend themselves or allies, this
   form allows for a strong but mobile stance, that can quickly adapt to using
   even the largest shields.

   Abilities
      :node:ref:`Generic.Ability.Aura`,
      :node:ref:`Generic.Ability.Shift`

   Conditions
      :node:ref:`Generic.Condition.Stagger`

   Traits
      :node:ref:`Generic.Boon.Alert`,
      :node:ref:`Generic.Boon.CombatAwareness`,
      :node:ref:`Generic.Boon.Empowered`,
      :node:ref:`Generic.Boon.Endurance`,
      :node:ref:`Generic.Boon.Fury`,
      :node:ref:`Generic.Boon.FleetFoot`,
      :node:ref:`Generic.Boon.NaturalDefender`,
      :node:ref:`Generic.Boon.Valour`,
      :node:ref:`Generic.Boon.Vigilant`

----

.. include:: /content-footer.rst
