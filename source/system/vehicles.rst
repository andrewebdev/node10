.. -*- coding: utf-8 -*-

:orphan:

.. |nem| replace:: Nemron RPG

.. title:: |nem| Vehicle Rules

.. include:: /global-message.rst

.. _vehicles:

=============
Vehicle Rules
=============
This page describe how Vehicle and Ship mechanics work in the |nem| System. We
designed the core system so that it could translate to larger power levels,
without needing to learn new mechanics.

The best way to show how this works is to just use a couple of examples. Each
section below will give players and game masters a good idea on how to deal with
vehicles of different sizes etc.


* Different Systems require different skills
* Each system has it’s power level (dice to roll)
* Each system also has Health
* AI can take over some systems. Depending on type of AI has max levels etc.


----

:Author(s):

   * Andre Engelbrecht

.. include:: /content-footer.rst
