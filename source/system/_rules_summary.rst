:orphan:

======================
Rules Summary and Tips
======================

Dice Checks
===========
When making a dice check (**DC**), players roll a pool of dice, and **keeps the
highest result**.

In the documentation we like to use a special syntax to show dice pools for
dice checks.

A dice pool can look like this;

   ``Awareness › Investigation``

… which means the PC may add both **Awareness** and **Investigation** dice to
their dice pool. They roll both dice, and **keeps the highest result**.

We sometimes include the target **DC** in the roll:

   ``Awareness › Investigation » DC4``

This means that the dice pool can include both **Awareness** and
**Investigation** skill, and the target number is **four** or higher.

Sometimes, when we ask for dice checks, we may specify only the *base attribute*
for the roll. It will look something like this:

   ``Awareness » DC4``

Even though we only mention the attribute, it *does not* mean that this is all
the player can add to their dice pool; If for example the check is to
investigate a corpse, then the player can add both **Investigation** and
**Biology** skills to their dice pool, even though we only asked for
**Awareness**.

This is 100% allowed in the Nemron system and the GM should
**actively encourage** players to blend their skills together in this way, so
long as the combination makes sense for the skill check.

**Lastly,** when we specify a roll like ``Awareness › Investigation » DC4``, it
does not mean that you **must** include ``Investigation``. If the player do not
have the ``Investigation`` skill, they can still make the roll by just rolling
the ``Awareness`` by itself.
