.. -*- coding: utf-8 -*-

.. title:: Intentions

.. include:: /global-message.rst

##########
Intentions
##########
When a PC uses a sword, we know that the player can use that sword to either
pierce, slash or bash (with the pommel) the target with the sword. They could
also use their combat knowledge to disarm their opponent.

These are examples of intentions within the Nemron system. The system allows
players to come up with interesting ways to use their skills, without needing to
“bend the rules”.

.. _intentions-damage:

======
Damage
======
.. node:def:: Acoustic Damage
   :uid: Generic.Damage.Acoustic
   :type: E

   Damage caused by sound waves.

.. node:def:: Ballistic Damage
   :uid: Generic.Damage.Ballistic
   :type: E

   Anything fired at extreme speeds.

.. node:def:: Cold Damage
   :uid: Generic.Damage.Cold
   :type: E

   Extreme cold damage.

.. node:def:: Corrosive Damage
   :uid: Generic.Damage.Corrosive
   :type: E

   Damage caused by corrosive elements.

.. node:def:: Crushing Damage
   :uid: Generic.Damage.Crushing
   :type: E

   Physical crushing damage.

.. node:def:: Holy Damage
   :uid: Generic.Damage.Holy
   :type: E

   Holy damage from some kind of divine source.

.. node:def:: Kinetic Damage
   :uid: Generic.Damage.Kinetic
   :type: E

   Deal kinetic, force, or bludgeoning damage to the target.

.. node:def:: Neural Damage
   :uid: Generic.Damage.Neural
   :type: E

   Inflict neural or energy damage to the target. Target loses
   :node:ref:`Generic.Trait.Energy` equal to the damage dealt.

.. node:def:: Piercing Damage
   :uid: Generic.Damage.Piercing
   :type: E

   Arrows, bullets, daggers, rapiers.

.. node:def:: Psychic Damage
   :uid: Generic.Damage.Psychic
   :type: E

   Inflict :node:ref:`Generic.Condition.Stress` damage to the target.

.. node:def:: Toxic Damage
   :uid: Generic.Damage.Toxic
   :type: E

   Damage caused by toxins or poisons.

.. node:def:: Slashing Damage
   :uid: Generic.Damage.Slashing
   :type: E

   This is a cut across the target from weapons such as swords, and axes.

.. node:def:: Heat Damage
   :uid: Generic.Damage.Heat
   :type: E

   Heat or fire damage to the target.

.. node:def:: Electric Damage
   :uid: Generic.Damage.Electric
   :type: E

   Electric or lightning damage to the target.

.. _intentions-boons:

=====
Boons
=====
Boons are :ref:`effect nodes <node-type-effect>`, that can affect characters,
skills, objects or areas in a positive way. These effects can be temporary or
permanent, depending on narrative or setting.

**To gain or apply a boon,** the character applying the boon will makes the
roll for applying the boon. Unless otherwise specified, the number of boon ranks
gained is equal to the resulting roll.

**Boons decay** by one rank per round. This could change based on the narrative
or special rules set by the GM.

**If you have a boon as a character trait**, that boon will be ``rank 1`` and
will not decay below this rank.

List of Boons
=============
.. node:def:: Agile Striker
   :uid: Generic.Boon.AgileStriker
   :type: E

   | **Requirements:** Unarmed combat and one handed melee weapons only

   You may use **Agility** instead of **Power** when making unarmed and
   one-handed attack rolls.

.. node:def:: Alert
   :uid: Generic.Boon.Alert
   :type: E

   **Effect:** *Lesser boon* to Awareness and Sense rolls.

.. node:def:: Ambidexterous
   :uid: Generic.Boon.Ambidex
   :type: E

   | **Effect:** Remove the *lesser penalty* when using your off-hand item while
     :ref:`combat-dual-wielding`.

   You favour no limb over the other and can use items left or right handed
   without incurring any penalty.

.. node:def:: Blur
   :uid: Generic.Boon.Blur
   :type: E

   | **Effect:** All attack rolls against you have a *lesser penalty*.

   You are difficult to target and hit with any attack. This can be because you
   have a magical effect on you blurring visual boudary, or it could be more
   mundane and physical, like using irregular movement or steps to make it hard
   to predict how you will move next.

      **Sci-Fi example:** Light particles around you vibrate and distort
      rapidly, making it unclear what your precise position is.

      **Modern day:** Your movement is irregular or erratic making it hard to
      predict how you will move at any one moment.

.. node:def:: Camouflage
   :uid: Generic.Boon.Camouflage
   :type: E

   | **Effect:** *Lesser boon* to all stealth rolls

.. node:def:: Charming
   :uid: Generic.Boon.Charming
   :type: E

   | **Effect:** *Lesser boon* to any roll that involve social interactions.

.. node:def:: Combat Awareness
   :uid: Generic.Boon.CombatAwareness
   :type: E

   **When this boon is applied,** pick a number of opponents equal to your
   ``Focus`` rank. While this boon is active, you have a *lesser boon* to all
   *attack and defence* rolls against those opponents.

   **If this boon is chosen as a trait,** then the character may choose the
   opponents the moment they enter combat.

   Note that the focus can be shifted to new opponents during combat, but the
   character **must use an action** to “re-focus” on the new opponent.

.. node:def:: Combat Ready
   :uid: Generic.Boon.CombatReady
   :type: E

   | **Effect:** *Lesser boon* to your **initiative rolls**

   You have a keen sense of danger and quickly to respond to combat scenarios.

.. node:def:: Dark Sight
   :uid: Generic.Boon.DarkSight
   :type: E

   | **Effect:** You no longer get penalties in dark and un-lit areas.

   You can see in the dark. You are unable to make out fine detail or colour,
   but your vision is good enough to navigate dark spaces and distinguish
   obstacles, objects or other actors.

.. node:def:: Deadeye
   :uid: Generic.Boon.Deadeye
   :type: E

   | **Effect:** *Lesser boon* to all ranged attack rolls.

   You have exceptional aim with all ranged weapons.

.. node:def:: Empath
   :uid: Generic.Boon.Empath
   :type: E

   | **Effect:** *Lesser boon* to any social roll related to the emotional state
     of others.

   You are naturally able to understand how someone else feel. You find it easy
   to think of yourself in their position, and you know how hardships can affect
   people emotionally.

.. node:def:: Empowered
   :uid: Generic.Boon.Empowered
   :type: E

   | **Effect:** *Lesser boon* to all **Power** rolls.

.. node:def:: Endurance
   :uid: Generic.Boon.Endurance
   :type: E

   | **Effect:** *Lesser boon* to any **Constitution** roll related to weather
     or environmental effects. This includes resistance rolls to conditions like
     :node:ref:`Generic.Condition.Frozen` or
     :node:ref:`Generic.Condition.Burning`

   You have trained your body tirelessly to have strong endurance and higher
   resistance to some extreme weather patterns.

.. node:def:: Point Blank
   :uid: Generic.Boon.PointBlank
   :type: E

   | **Effect:** You no longer suffer the :ref:`rules-ranged-point-blank`
     penalty when making ranged attacks to opponents within your melee range.

.. node:def:: Farsight
   :uid: Generic.Boon.Farsight
   :type: E

   | **Effect:** *Lesser boon* to any **Awareness** roll related to sight over
     long distances.

   You have good vision and can see longer distances.

.. node:def:: Ferocity
   :uid: Generic.Boon.Ferocity
   :type: E

   | **Effect:** You have **+1 damage** per rank in this condition for any of
     your attacks

.. node:def:: Fury
   :uid: Generic.Boon.Fury
   :type: E

   | **Effect:** *Lesser boon* to all **Power** and **Constitution** rolls.

.. node:def:: Fleet Foot
   :uid: Generic.Boon.FleetFoot
   :type: E

   *Lesser boon* to all **agility based** attack, defence and resistance rolls.

   Additionally, whenever you use an action like dodge, block or attack, you can
   move up to ``3 meters`` as part of your action.

.. node:def:: Immunity
   :uid: Generic.Boon.Immunity
   :type: E

   | **Effect:** *Lesser boon* to any resistance roll vs. venoms, poisons and
     other toxins

.. node:def:: Inner Monologue
   :uid: Generic.Boon.InnerMonologue
   :type: E

   | **Effect:** *Lesser boon* to any investigation or research related rolls
   | **Negative Effect:** *Lesser penalty* to any Awareness or Sense rolls
     that are not related to the research or investigation roll.

   You rapidly mumble or discuss with yourself, either silently or out loud, a
   problem, how it manifests, and potential solutions.

.. node:def:: Inspired
   :uid: Generic.Boon.Inspired
   :type: E

   | **Effect:** *Lesser boon* to all rolls while you have this boon

   You are temporarily inspired.

.. node:def:: Invisible
   :uid: Generic.Boon.Invisible
   :type: E

   | **Effect:** You are invisible.

   While invisible you cannot be seen. But you still make noise, leave
   footprints and emit smell etc.

.. node:def:: Keen Sense
   :uid: Generic.Boon.KeenSense
   :type: E

   | **Effect:** *Lesser boon* to any **Awareness** or **Sense** roll relating
     to a *single sense*

.. node:def:: Luck
   :uid: Generic.Boon.Luck
   :type: E,R

   **Decay:** Never

   **Consume** a number of ranks in this resource, to gain the following
   benefits, based on the number of ranks you consumed:

   | **1 Rank:** Get a *lesser boon* to your next roll.
   | **2 Ranks:** Get a *greater boon* to your next roll.
   | **3 Ranks:** Re-roll any skill, adding a *lesser boon* to the roll.
   | **4 Ranks:** Re-roll any skill, adding a *greater boon* to the roll.

   *Unlike* :node:ref:`Generic.Boon.Motivated`, this condition does not
   have a duration, so it wont diminish over time. It only diminishes when you
   actually spend the resource.

   **If this boon is chosen as a Trait,** and you consume the final stack of
   this boon, then it will recharge back up to ``Rank 1`` after a long rest.

   .. note::

      This node serves as a generic example of how you can implement a special
      luck-based mechanic. Use the same method to add your own custom boon for
      your world or setting.

.. node:def:: Mobility
   :uid: Generic.Boon.Mobility
   :type: E

   **Effect:** *Lesser boon* to all **Agility** rolls.

.. node:def:: Motivated
   :uid: Generic.Boon.Motivated
   :type: E,R

   Motivated to do your best no matter what.

   **Consume** a number of ranks in this resource, to gain the following
   benefits, based on the number of ranks you consumed:

   | **1 Rank:** Get a *lesser boon* to your next roll.
   | **2 Ranks:** Get a *greater boon* to your next roll.
   | **3 Ranks:** Re-roll any skill, adding a *lesser boon* to the roll.
   | **4 Ranks:** Re-roll any skill, adding a *greater boon* to the roll.

.. node:def:: Natural Defender
   :uid: Generic.Boon.NaturalDefender
   :type: E

   **Effect:** *Lesser boon* to any defensive action

.. node:def:: Regeneration
   :uid: Generic.Boon.Regeneration
   :type: E

   | **Effect:** Every limb will heal :node:ref:`1 HP <Generic.Attribute.HP>`
     *per round*.

   .. note::

      While the regen boon affect entire character, the healing effect will only
      happen on a single limb per round. The PC may choose which limb to heal.

.. node:def:: Resilience
   :uid: Generic.Boon.Resilience
   :type: E

   You get temporary bonus armour to one target limb, while this boon is active.

   The armour rank is equal to the highest dice rank in your dice pool using
   this intention.

   The source of this boon can specify that the armour spread over more than one
   limb, but you have to divide the total up between those limbs.

   For example, if you get ``3 ranks`` of this boon, you can spread them so
   that you have ``2 armour`` on your head, and ``1 armour`` on your torso.

   .. todo:: What if this is chosen as a trait? Add notes for this case.

.. node:def:: Resolve
   :uid: Generic.Boon.Resolve
   :type: E

   | **Effect:** *Lesser boon* to **Focus** rolls

.. node:def:: Talented
   :uid: Generic.Boon.Talented
   :type: E

   | **Effect:** *Lesser boon* to any roll for the *skill* you have talent for

   You have natural talent for a specific skill. Choose the skill when you gain
   this boon/trait.

.. node:def:: Valour
   :uid: Generic.Boon.Valour
   :type: E

   | **Effect:** *Lesser boon* to **Willpower**, **Control** and **Focus**
     resistance rolls.

   Strength of mind in regard to danger; that quality which enables a person to
   encounter danger with firmness.

.. node:def:: Vigilant
   :uid: Generic.Boon.Vigilant
   :type: E

   You are hyper alert to everything happening around you, and you can rapidly
   observe and react to actions targeting your awareness.

   You have *lesser boon* to **Agility**, **Awareness** or **Sense** resistance
   rolls.

   The condition :node:ref:`Generic.Condition.Flanked` has no effect on you.

.. _intentions-conditions:

==========
Conditions
==========
Conditions are :ref:`effect nodes <node-type-effect>`, that can affect
characters, skills, objects or areas in a *negative* way. These effects can be
temporary or permanent, depending on narrative or setting.

**Every condition has a resistance pool.** This is the opposing dice pool that
PC’s can roll to resist the effects of a specific condition applied to them.

The **DC** for the resistance roll can be a set difficulty or, as is the case in
most scenarios, the **DC** is against an opposing roll.

The number of “stacks/ranks” of the condition that the player receives, is the
difference between the resistance roll and the **DC**.

If for example, a character needs to make a ``Constitution » DC6`` resistance
roll against poison, and they roll ``4``, then they will only receive
``2 stacks`` of poison.

**Conditions decay one rank every five minutes,** unless otherwise specified.
This could change based on the narrative or special rules set by the GM.

**Conditions can affect individual limbs.** A leg or arm can be poisoned for
example. In some cases (based on the narrative or GM ruling), the condition can
spread to adjacent limbs.

All this means that it is possible to “stun” or “paralyse” a single limb.

List of Conditions
==================
.. node:def:: Bleeding
   :uid: Generic.Condition.Bleeding
   :type: E

   | **Resistance Pool:** ``Constitution``
   | **Effect:** ``1 Damage`` per round

   If a limb is bleeding but already have 4 wounds, then the remaining bleeding
   ranks will pass up to the next limb, in the direction of the nearest
   *major limb*.

.. node:def:: Burning
   :uid: Generic.Condition.Burning
   :type: E

   | **Resistance Pool:** ``Constitution``
   | **Effect:** ``1 Damage`` per round

.. node:def:: Charmed
   :uid: Generic.Condition.Charmed
   :type: E

   | **Resistance Pool:** ``Control › Willpower``
   | **Effect:** *Lesser penalty* for any negative actions towards the
     source that charmed the character

   Charmed by someone, you find them interesting and alluring in ways that you
   cannot describe. It becomes hard to take any negative actions towards the
   charmer.

   .. note::

      If the charmer takes any action towards the charmed individual that could
      be interpreted as “betrayal”, then the charmed individual will immediately
      lose 2 ranks of Charm, and get the condition
      :node:ref:`Generic.Condition.Confusion`, as they attempt to mentally
      struggle why the charmer is betraying them.

.. node:def:: Confusion
   :uid: Generic.Condition.Confusion
   :type: E

   | **Resistance Pool:** ``Focus``
   | **Effect:** *Lesser penalty* to any ref:`Mind <attribute-line-mind>` rolls
     (Intellect, Awareness and Willpower)

.. node:def:: Corrosion
   :uid: Generic.Condition.Corrosion
   :type: E

   | **Resistance Pool:** ``Constitution``

   *Every round,* all affected limbs will receive ``1 damage``, as well as
   ``1 point of damage`` to any armour on the affected limbs.

   Remove this condition by washing the corrosive substance off or, removing
   the armour or clothing.

.. node:def:: Crippled
   :uid: Generic.Condition.Crippled
   :type: E

   | **Resistance Pool:** ``Agility › Constitution``
   | **Effect:** *Lesser penalty* to all **Agility** rolls and, movement speed
     is *halved*

.. node:def:: Downed
   :uid: Generic.Condition.Downed
   :type: E

   | **Resistance Pool:** ``Control``

   This condition is a indicator that the character received critical damage,
   and is on the verge of death.

   **Once per round,** the character must make a ``Control`` resistance roll in
   an attempt to stabilise. The difficulty is the
   :ref:`fixed-medium <fixed-med-max>` for the resistance roll.

   **Succeeding** the roll, the character will stabilise, and the character no
   longer need to make resistance rolls every round. (Unless they take more
   damage again)

   **Failing** the roll, this condition will increase by **one rank**.

   **If this condition reaches rank 4**, then the character will be dead.

   .. tip::

      The First-Aid skill can also help stabilise a dying character.

.. node:def:: Encumbered
   :uid: Generic.Condition.Encumbered
   :type: E

   | **Resistance Pool:** ``Power``

   You have a *greater penalty* to all **Agility** rolls and, your movement
   speed is *halved*.

.. node:def:: Flanked
   :uid: Generic.Condition.Flanked
   :type: E

   | **Decays:** Instantly as soon as you are no longer flanked by opponents.

   While flanked you have a *lesser penalty* to any **defensive** action against
   the flanking opponents.

.. node:def:: Dazed
   :uid: Generic.Condition.Dazed
   :type: E

   | **Resistance Pool:** ``Focus``

   While dazed you can only take simple actions.

   You may only use a single **attribute dice** in your dice pool. *No other
   dice may* contribute to your dice pool while you are dazed.

.. node:def:: Demoralised
   :uid: Generic.Condition.Demoralised
   :type: E

   | **Resistance Pool:** ``Willpower``.
   | **Effect:** *Lesser penalty* to all rolls.

.. node:def:: Dull
   :uid: Generic.Condition.Dull
   :type: E

   .. todo:: Not sure about the name “Dull” here, rename?

   | **Resistance Pool:** ``Awareness › Sense``.
   | **Effect:** *Lesser penalty* to all **Awareness** and **Sense** rolls,
     related to the affected sense.

   This condition will target a specific sense, sight, sound, touch etc.

.. node:def:: Exhaustion
   :uid: Generic.Condition.Exhaustion
   :type: E

   | **Resistance Pool:** ``Constitution``.
   | **Effect:** On *ranks 1 to 2* you have a *lesser penalty* to all rolls. On
     *ranks 3 to 4* you have a *greater penalty* to all rolls.
   | **Decay:** 1 rank every 4 hours of rest

   While a character has this condition, any further actions *can* potentially
   increase the rank of this condition. It’s up to the GM to determine if the
   action warrants this increase.

.. node:def:: Fear
   :uid: Generic.Condition.Fear
   :type: E

   | **Resistance Pool:** ``Willpower``

   Overcome with fear, you get a *lesser penalty* to all actions, unless the
   action is an attempt to get away from the source of fear.

.. node:def:: Frozen
   :uid: Generic.Condition.Frozen
   :type: E

   | **Resistance Pool:** ``Constitution``
   | **Effect:** *Lesser penalty* to all rolls and, you get ``1 damage``
     *per round* to the affected limb

   Your body suffer from severe cold. Muscles cramp up, blood struggle to flow
   and your skin cracks.

.. node:def:: Marked
   :uid: Generic.Condition.Marked
   :type: E

   | **Resistance Pool:** ``Control``

   When marked, there is a invisible “mark” on the target. Only the character
   that applied the mark is able to sense or see the mark.

   In most cases only the individual that applied this condition will have the
   bonuses below.

   All skill and ability rolls have a *lesser boon* against the marked target.

   This condition can also mark the target to later ``Sense`` where they are, or
   keep track of their movement etc.

.. node:def:: Paralysis
   :uid: Generic.Condition.Paralysis
   :type: E

   | **Resistance Pool:** ``Control``

   While you have this condition, you cannot make any actions that use the body.
   You can still hear and see, but you cannot feel touch or move your body in
   any way.

.. node:def:: Poisoned
   :uid: Generic.Condition.Poisoned
   :type: E

   | **Resistance Pool:** ``Constitution › Control``

   Different poisons can have different effects depending on settings, game,
   enemies etc. What-ever the effect the following rules apply.

   :ref:`Every round <rules-action-intervals>`, the character must make a
   resistance roll to prevent the poison spreading. The attribute depends on the
   type of poison.

   Failing the resistance roll, the poison effect will spread to the limb(s)
   immediately adjacent to the effected limb.

.. node:def:: Prone
   :uid: Generic.Condition.Prone
   :type: E

   | **Resistance Pool:** ``Power``.

   You are prone. This condition ends if you stand up, or an ally helps you up.

   While prone, you have a *lesser penalty* to all **Agility** rolls and,
   **ranged attacks** against you also have a *lesser penalty*.

.. node:def:: Restrained
   :uid: Generic.Condition.Restrained
   :type: E

   | **Resistance Pool:** ``Power`` or ``Agility``, depending on how the
     condition source.
   | **Effect:** You are restrained and cannot move.

.. node:def:: Slow
   :uid: Generic.Condition.Slow
   :type: E

   | **Resistance Pool:** ``Power``.
   | **Effect:** You can only move half your normal distance.

.. node:def:: Staggered
   :uid: Generic.Condition.Stagger
   :type: E

   | **Resistance Pool:** ``Agility``

   You don’t have good footing or balance.

   You have a *lesser penalty* to **Power** and **Agility** rolls, due to the
   imbalance, frustration and distraction caused by this condition.

.. node:def:: Stress
   :uid: Generic.Condition.Stress
   :type: E, R

   .. note:: **Disclaimer**

      Real world mental trauma is serious and, should never be treated jokingly,
      or haphazardly.

      If you decide to use the stress rules in your games, then make sure your
      players are OK with using these rules. Also make sure that they are aware
      that certain sensitive topics may come up during play.

      **Tools**

      It may be handy to have tools in your game to help deal with sensitive
      topics. I personally like the
      `X-, and O-Card System <https://docs.google.com/document/d/1SB0jsx34bWHZWbnNIVVuMjhDkrdFGo1_hSC2BWPlI3A/edit?usp=sharing>`_.

   Stress is the feeling of being overwhelmed or unable to cope with mental or
   emotional pressure.

   **Whenever a character take stress**, they need to write down on the
   character sheet, the source that caused that stress.

   Every subsequent exposure to the same scenario, the character need to make a
   **Willpower Resistance Roll** vs. their
   :ref:`willpower fixed-medium <fixed-med-max>`.

   **Failing the resistance roll**, the character will receive
   :node:ref:`ranks of Torment <Generic.Condition.Torment>` equal to the amount
   of stress originally suffered for that source (as written down on the
   character sheet).

   **Healing Stress**

   Stress cannot instantly recover with items or skills. It takes at least
   **3 successful counselling sessions** with a qualified therapist.

   Each session will require a different dice check at the end of a counselling
   session, and a character cannot make more than one of these checks per day.

   #. **Awareness** to identify the mental trauma accurately.
   #. **Intellect** to rationalize and work through the trauma.
   #. **Willpower** to utilize the insight gained in an attempt to heal the
      trauma.

   The difficulty for each of the rolls is the
   :ref:`fixed-medium <fixed-med-max>` for the **attribute** used.

   **When one of the rolls fail**, the character can retry that roll again the
   end of the *next* therapy session.

   **Once the final roll succeeds,** the character will heal
   **1 point of stress**.

.. node:def:: Stunned
   :uid: Generic.Condition.Stunned
   :type: E

   | **Resistance Pool:** ``Control``
   | **Decay:** 1 Rank per round

   While stunned, you **cannot take any action(s)**.

.. node:def:: Taunted
   :uid: Generic.Condition.Taunted
   :type: E

   | **Resistance Pool:** ``Willpower``

   Taunted by someone (or something), you have a *lesser penalty* to any actions
   against targets other than the source taunting you.

.. node:def:: Torment
   :uid: Generic.Condition.Torment
   :type: E

   | **Resistance Pool:** ``Willpower``

   Nightmarish memories plague and torment your every thought, causing you to
   second-guess every action.

   While tormented, you cannot take any actions. You may only make a single
   re-action per round, and this action will have a *lesser penalty*.

.. node:def:: Weakness
   :uid: Generic.Condition.Weakness
   :type: E

   | **Resistance Pool:** ``Constitution``

   Your muscles weaken and is unable to produce it’s full capacity. You get a
   *lesser penalty* to all **Power** rolls.

.. _intentions-abilities:

=========
Abilities
=========

Using Abilities
===============
**When activating or using an ability,** you do not add the ability rank dice to
the dice pool unless the ability explicitly asks for it. This is because
abilities change the *behaviour* of another skill, domain, ability or intention.

For example; ``Control › Fire Domain › Pulse (AB)`` can create a pulse of fire
energy to do fire damage, while ``Control › Water Domain › Pulse (AB)`` can
create a pulse of water energy to do water damage.

The examples above use the :node:ref:`Pulse Ability <Generic.Ability.Pulse>`,
but with different elemental domains. The first will result in a classic “fire
bolt”, while the second could create the same effect, but with water.

**In both examples above,** the player only adds the ``Control`` and
``Fire domain`` dice to their dice pool.

Combo Abilities
===============
.. todo::

   Explain how abilities can combine with skills, and intentions to change the
   intended outcome.

   Use Aura as an example:
      This template is actually an example of how to create a custom ability by
      combining other abilities into a new “recipe”. This ability was created
      with the following recipe:

      ``Amplify (2) › Shape (2) » 4 Energy``

      It uses :node:ref:`Generic.Ability.Shape` (sphere) on rank 2 so that a
      sphere can covers the area around the user.

      Shape is :node:ref:`amplified <Generic.Ability.Amplify>` to last 2 rounds
      longer than normal.

List of Abilities
=================
.. node:def:: Amplify
   :uid: Generic.Ability.Amplify
   :type: AB

   **Energy Cost:** Depends on the kind of amplification. See below.

   When using this ability, you need to specify which skill, ability, boon or
   condition you’d like to amplify. The amplified node **must** be part of the
   current action that you use amplify on.

   **Skill or Ability (1 Energy per Rank):** Increase the rank of the skill or
   ability. You have to use the amplified skill immediately, or you will lose
   the amplification. This will only last for a single use of the skill.

   **Boon or Condition (1 Energy per Rank):** Increase the rank of the boon or
   condition that you apply to allies or opponents.

   **Upgrade (4 Energy):** Change a *lesser boon* into a *greater boon*.

   **Downgrade (4 Energy):** Change a *lesser penalty* into a *greater penalty*.

   **Other:** You can always negotiate a different kind of amplification with
   the GM. Like, for example, increasing the duration that another ability or
   it’s behaviour, shape or size.

.. node:def:: Aura
   :uid: Generic.Ability.Aura
   :type: AB

   | **Energy Cost:** 4
   | **Duration:** :math:`{Rank} + 2` in rounds

   Channel energy into an area around you. The aura will affect either all enemy
   or allied characters that enter
   :ref:`your melee zone <rules-range-indicators>`.

.. node:def:: Beam
   :uid: Generic.Ability.Beam
   :type: AB

   | **Energy:** 2
   | **Max Range:** :ref:`Far <rules-range-indicators>`
   | **Duration:** 1 Round

   A beam of energy stream out from the source to the target (or direction).
   Affects anything the beam touches.

   During the round that the beam is active, the user can choose to use their
   reaction to redirect the beam. This change in direction can pass through
   other targets. *The difficulty to evade or dodge* the beam during this time
   is the same as the original roll for the beam.

.. node:def:: Cleanse
   :uid: Generic.Ability.Cleanse
   :type: AB

   | **Energy:** 1
   | **Range:** Touch

   Cure a number of condition stacks/ranks equal to the amount rolled.

   The kind of conditions cleansed may depend on the domain, item or energy
   source used with this intention.

   **NOTE:** This ability cannot be used to cleanse the
   :node:ref:`Generic.Condition.Stress` condition.

.. node:def:: Detection
   :uid: Generic.Ability.Detection
   :type: AB

   Find or detect something. The nature of what you can detect with this ability
   depend on the combination of skills, domains or other abilities used with
   this ability.

.. node:def:: Disarm
   :uid: Generic.Ability.Disarm
   :type: AB

   | **Energy Cost:** 1 Energy
   | **Resistance Pool:** ``Power › Agility``

   Failing the resistance roll, the target will drop or lose their weapon.

.. node:def:: Enchant
   :uid: Generic.Ability.Enchant
   :type: AB

   **Energy Cost:** 1 per target

   Enchant the target with a boon, condition or ability. You need access in some
   way to the intention that you want to enchant the character or object with.

   .. todo:: *Expand on this ability.*

      * What is the enchantment duration for Boons
      * What is the enchantment duration for Conditions
      * If the enchantment is an ability, then the energy requirement is based
        on the ability?
      * The “enchanter” should be able to “imbue” Energy directly into the item.
        In this way some items have their own energy, or can function as a
        “battery” for other abilities.

      Lastly, we should also consider renaming this ability. “Enchant” works for
      a magical or supernatural setting, but doesn’t work in settings that
      doesn’t have any notion of the supernatural or magic. We should use a name
      for this ability that is more universal, no matter the setting.

.. node:def:: Evasive Movement
   :uid: Generic.Ability.EvasiveMovement
   :type: AB

   **Energy Cost:** 1

   You disengage from your current position and move as far as your movement
   speed allows, in a direction of your choosing.

   This counts as an evasive action, so any opponents using reactions to
   strike you while doing this movement, need to beat your disengage roll.

.. node:def:: Explode
   :uid: Generic.Ability.Explode
   :type: AB

   **Energy Cost:** 3

   The energy source explode outwards. This can create effects where things need
   to expand and scatter in random directions in a manner that is slow, fast,
   gentle or violent.

   The radius of this explosion depend on the ability rank:
   :math:`{Rank} \times 2`

   Using this template as-is will result in the **point of origin** on the
   “caster”. If you want to have this effect happen a distance away from you,
   then you can combine it with :node:ref:`Generic.Ability.Pulse` (or another
   ability) to shoot a pulse to where you want the epicentre to be.

.. node:def:: Feign
   :uid: Generic.Ability.Feign
   :type: AB

   **Attribute:** ``Control``

   Create a false impression related in some way to the skills, domain,
   abilities used together with this ability.

   The total roll determine the difficulty for an opponent to notice that you
   are attempting to create a false impression.

.. node:def:: Heal
   :uid: Generic.Ability.Heal
   :type: AB

   **Energy Cost:** ``1 per wound``

   Instantly heal :node:ref:`HP <Generic.Attribute.HP>` equal to the amount of
   energy spent with this skill.

   The target can choose how to distribute the healing for the different limbs.

.. node:def:: Illusion
   :uid: Generic.Ability.Illusion
   :type: AB

   | **Energy Cost:** 2
   | **Duration:** Equal to the amount rolled (in rounds)

   Create a illusion of some sort. The amount rolled when creating the illusion
   determine how difficult it is to see through the illusion with either
   **Sense** or **Awareness**.

.. node:def:: Implode
   :uid: Generic.Ability.Implode
   :type: AB

   **Energy Cost:** 3

   The reverse of :node:ref:`Generic.Ability.Explode`. All other rules remain
   the same.

.. node:def:: Manifest
   :uid: Generic.Ability.Manifest
   :type: AB

   **Note:** You need to use this with another template like
   :node:ref:`Generic.Ability.Shape`. On it’s own this cannot do much.

   Manifest the energy in a way that resemble a specific shape or form used for
   this intention.

   If the domain and purpose is to create something solid, then the density
   (armour) of the manifestation is equal highest rank in your dice pool.

   For example, if you use :node:ref:`Generic.Domain.Earth` earth domain with a
   :node:ref:`Generic.Ability.Shape` template, to create a wall of earth.

.. node:def:: Opportune Striker
   :uid: Generic.Ability.OpportuneStriker

   **Energy Cost:** 1

   When moving past an opponent, you get **one free attack action** against that
   opponent.

   This is the case *for any kind of movement*. So if someone shoves you
   past another opponent, you can take a free attack on that opponent.

.. node:def:: Pulse
   :uid: Generic.Ability.Pulse
   :type: AB

   **Energy Cost:** 2

   A single pulse of energy shoot towards the target.

   The **maximum distance** the pulse can travel depend on the template rank.

   | **Rank 1:** :ref:`Near <rules-range-indicators>`
   | **Rank 2:** :ref:`Far <rules-range-indicators>`
   | **Rank 3:** :ref:`Far <rules-range-indicators>`
   | **Rank 4:** :ref:`Distant <rules-range-indicators>`

.. node:def:: Shape
   :uid: Generic.Ability.Shape
   :type: AB

   | **Energy Cost:** 1 to 4, depending on the kind of shape (see below)
   | **Shape size/area:** :math:`{NodeRank} \times 2`, in cubic meters
   | **Duration:** 1 Round (or instant if requested)

   Shape the energy into a specific form like a Sphere, Wall, Cone etc.
   The shape complexity and size depend on the ability rank.

   Using this template as-is will result in the **centre-point** being on the
   “caster”. To change the center point location, you will have to combine this
   template with another like :node:ref:`Generic.Ability.Pulse`.

   The kind of shape created depends on the amount of energy you use.

   **1 Energy:**
   You can create simple 2 dimensional shapes like a circle, triangle,
   rectangle etc.)

   **2 Energy:**
   You can now create simple 3 dimensional geometric shapes like a sphere,
   cube, cone, cylinder etc.

   **3 Energy:**
   You can now also have more mathematically complex shapes that can have
   multiple angles, curves and corners. These will still look geometric to
   the observer.

   **4 Energy:**
   You can now create shapes complex enough to mimic the randomness in nature
   etc. You can for example shape energy to look like a human statue etc.

.. node:def:: Shape-Shift
   :uid: Generic.Ability.ShapeShift
   :type: AB

   | **Energy Cost:** 6

   Use this to shape-shift into a different form.

   When using this ability, transform into a new frame. Depending on what you
   are transforming into, your :ref:`Body <attribute-line-body>` and
   :ref:`Mind <attribute-line-mind>` attributes values will change to those of
   the shape-shifted frame.

   Additionally, you get **two predetermined traits** related to the new frame
   you transformed into.

   All attributes and traits will revert back once you transform back into your
   original frame.

   **Note however,** transforming takes a toll on the body. When you transform
   back into your original frame, you must make a ``Constitution`` resistance
   roll vs. the :ref:`fixed-medium <fixed-med-max>` of your ``Control``
   attribute.

   **Failing the roll,** you will get
   :node:ref:`1 rank of Exhaustion <Generic.Condition.Exhaustion>` due to the
   strain on the body.

.. node:def:: Shift
   :uid: Generic.Ability.Shift
   :type: AB

   **Energy Cost:** 1 Energy
   **Resistance Pool:** ``Power › Agility``

   Push, pull or shift energy or matter ``1 meters per rank``, based on the
   total roll for this intention.

   This can be in any direction of your choosing, so long as the game setting
   and abilities allow for the outcome.

      **For example:**

      Using this with a unarmed combat (push kick), you can only shift them in
      the direction of the kick.

      If you are using telekinetic shove, then the shift can happen in any
      direction of your choosing, since you can mentally control where the shove
      is coming from.

.. node:def:: Shout
   :uid: Generic.Ability.Shout
   :type: AB

   | **Energy Cost:** 2
   | **Distance:** :ref:`Near <rules-range-indicators>`
   | **Maximum Targets:** ``Rank + 1`` targets

   Infuse energy into a battle cry or shout.

   The maximum number of targets that you can affect, is
   :math:`{Domain/Item Rank} + 1`.

   This ability should combine with other domain features, or skills. So the
   effect for the shout depend on the skill/ability combination.

   For example, use it with ``Control › Brawler (Shout) › Intimidation``
   would create a shout meant to intimidate everyone around you.

.. node:def:: Split
   :uid: Generic.Ability.Split
   :type: AB

   | **Energy Cost:** ``1 Energy`` per split
   | **Maximum Splits:** ``Rank + 1``

   You can split energy output into multiple parts.

   A :node:ref:`Generic.Ability.Pulse` can, for example, become 2 or more
   separate pulses heading to different targets. In a similar way, a
   :node:ref:`Generic.Ability.Beam` can split into multiple beams.

.. node:def:: Tether
   :uid: Generic.Ability.Tether
   :type: AB

   .. todo:: *Revision/Refactor Required*

      I'm not 100% happy with the rules for this ability. I may re-visit and
      refactor it in the near future. Maybe the rules are actually ok, it’s
      mainly the description that is a bit ambiguous.

   | **Energy Cost:** 3
   | **Duration:** 2 Rounds
   | **Range:** Depends on ability rank

   Shoot an energy tether from yourself towards your target. Energy can flow
   back and forth between yourself and the tethered target, so long as the
   target is within the tether range.

   The tether range depend on the tether rank:

   | **Rank 1:** :ref:`Near <rules-range-indicators>`
   | **Rank 3:** :ref:`Far <rules-range-indicators>`
   | **Rank 4:** :ref:`Distant <rules-range-indicators>`

.. node:def:: Timer
   :uid: Generic.Ability.Timer
   :type: AB

   **Energy Cost:** 1 or more

   Use this ability to either extend the duration of a skill so that it lasts
   longer or, set a timer that will only activate once the time ran out.

   The duration of the timer depend on the amount of energy and rank used with
   this ability.

   | **Rank 1:** ``5 Rounds`` per energy used
   | **Rank 2:** ``1 Minute`` per energy used
   | **Rank 3:** ``5 Minutes`` per energy used
   | **Rank 4:** ``10 Minutes`` per energy used

   .. tip::

      You don’t have to use the exact timings above. You can for example spend
      only ``1 energy`` and set the timer for ``1 round`` instead of ``5
      rounds``.

      Or you can spend ``3 energy`` to set it for ``4 minutes``.

.. node:def:: Wave
   :uid: Generic.Ability.Wave
   :type: AB

   | **Energy Cost:** Equal to the rank used.
   | **Decay:** :ref:`1 Rank per TURN <rules-action-intervals>`.
   | **Cycles:** :ref:`End of every TURN <rules-action-intervals>`.

   This template will cause **multiple waves** of energy to emanate from the
   source.

   The amount of energy used will determine the number of waves that will affect
   the target or area.

   At the end of :ref:`every TURN <rules-action-intervals>`, a wave of energy
   will affect the target or area.

   Every ripple will be ``1 rank`` lower than the previous. This ability ends
   when the rank reaches zero.

.. include:: /content-footer.rst
