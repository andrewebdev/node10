"""
Classes and directives for various layout directives.
"""
from docutils import nodes
from docutils.parsers.rst import Directive, directives


class FlavourTextDirective(Directive):
    """
    This directive will highlight a piece of text as flavour text that should be
    read out to the players.
    """
    pass
