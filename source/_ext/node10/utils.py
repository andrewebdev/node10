from docutils import nodes


def create_row(cells):
    """ Helper that creates a table row """
    row = nodes.row()
    for cell in cells:
        entry = nodes.entry()
        row += entry
        entry += nodes.paragraph(text=cell)
    return row


def field_def(name, body):
    field = nodes.field()
    fname = nodes.field_name(name, name)
    fbody = nodes.field_body()
    fbody += nodes.Text(body)
    field += [fname, fbody]
    return field


def term_def(term: str, definition: str):
    """ Helper to create a definition item """
    dl_item = nodes.definition_list_item()
    dt = nodes.term(text=term)
    dd = nodes.definition()
    dd += nodes.Text(definition)
    dl_item += [dt, dd]
    return dl_item


def dice_by_rank(rank, keepval="kh"):
    dScale = [
        "1d4",
        "1d6",
        "1d8",
        "1d10",
        "1d12",
        "1d12,1d4",  # Rank 5
        "1d12,1d6",
        "1d12,1d8",
        "1d12,1d10",
        "2d12{kv}",
        "2d12{kv},1d4",  # Rank 10
        "2d12{kv},1d6",
        "2d12{kv},1d8",
        "2d12{kv},1d10",
        "3d12{kv}",
    ]
    return list(map(lambda s: s.format(kv=keepval), dScale))[rank]


# Helper Classes
class NemronObject(object):

    def __init__(self, name, **opts):
        self._name = name
        self.uid = opts.get('uid', name.replace(' ', ''))
        self.docname = opts.get('docname', None)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def __lt__(self, other):
        return self._name < other.name

    def __gt__(self, other):
        return self._name > other.name

    def __iter__(self):
        return iter([
            self.name,
            self.name,
            'Node',
            self.docname,
            self.uid,
            0,
        ])

    def refnode_content(self):
        """
        Returns a custom reference node, allowing us to choose a custom content
        for the link text.
        """
        lit = nodes.literal()
        lit += nodes.Text(self.name)
        return lit
