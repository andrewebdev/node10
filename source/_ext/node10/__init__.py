from typing import Dict, Any
from sphinx.application import Sphinx

from .nodes import NemNodeDomain
from .items import ItemDomain
from .actors import ActorDomain
from .rolls import roll_role
# from .builders import FoundryJournalBuilder


def setup(app: Sphinx) -> Dict[str, Any]:
    app.add_domain(NemNodeDomain)
    app.add_domain(ItemDomain)
    app.add_domain(ActorDomain)
    app.add_role('roll', roll_role)

    # Custom Builders
    # app.add_builder(CompendiumBuilder)
    # app.add_builder(FoundryJournalBuilder)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
