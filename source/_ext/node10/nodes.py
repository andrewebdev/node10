import re
from collections import defaultdict

from docutils import nodes
from docutils.parsers.rst import directives

from sphinx import addnodes
from sphinx.directives import ObjectDescription
from sphinx.domains import Domain, Index
from sphinx.roles import XRefRole
from sphinx.util.nodes import make_refnode

from .utils import create_row, term_def, NemronObject


# NOTE:
#
# We use the term node because our system consists of a graph of nodes,
# but it may seem confusing because docutils and sphinx also uses the term
# nodes.
#
# Just make sure not to confuse them, when you see any reference to "node".
# Use types where possible so that linters and interpreters know what to
# expect.
#

class NemronNode(NemronObject):
    NODE_TYPES = (
        ('a', 'Active'),
        ('p', 'Passive'),
        ('ab', 'Ability'),
        ('e', 'Effect'),
        ('r', 'Resource'),
        ('f', 'Fixed'),
    )

    def __init__(self, name, **opts):
        super().__init__(name, **opts)
        self.node_type = opts.get('type', 'a')

    @property
    def name(self):
        return f"{self._name} ({self.node_type.upper()})"

    @property
    def node_type_display(self):
        node_types = []
        for key in self.node_type.split(','):
            try:
                node_types.append(dict(self.NODE_TYPES)[key.strip().lower()])
            except KeyError:
                node_types.append('!FIXME')
        return ', '.join(node_types)


class NemNodeDirective(ObjectDescription):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = True
    option_spec = {
        'uid': directives.unchanged_required,
        'type': directives.unchanged,
    }

    def handle_signature(self, sig, signode):
        signode += addnodes.desc_name(text=sig)
        return sig

    def add_target_and_index(self, name_cls, sig, signode):
        signode['ids'].append(f"node-{self.nemnode.uid}")
        if 'noindex' not in self.options:
            items = self.env.get_domain('node')
            items.add_node(self.nemnode)

    def run(self):
        self.nemnode = NemronNode(self.arguments[0], **self.options)
        super().run()

        section = nodes.section(
            ids=[self.nemnode.uid],
            classes=['nemron-node'],
        )

        title = nodes.title(self.nemnode.name, self.nemnode.name)
        section.append(title)

        if self.content:
            description_node = nodes.paragraph()
            self.state.nested_parse(
                self.content,
                self.content_offset,
                description_node)
            section += description_node

        return [section]


class NemNodeIndex(Index):
    name = 'node'
    localname = 'Node Index'
    shortname = 'Nodes'

    def generate(self, docnames=None):
        content = defaultdict(list)
        items = sorted(self.domain.get_objects())

        for nemnode in self.domain.get_objects():
            content[nemnode.name[0]].append((
                nemnode.name,
                0,
                nemnode.docname,
                nemnode.uid,  # Anchor
                nemnode.uid,
                '',
                'Node',  # Type
            ))
        content = sorted(content.items())
        return content, True


class NemNodeDomain(Domain):
    name = 'node'
    label = 'Node Reference'
    roles = {
        'ref': XRefRole(),
    }
    directives = {
        'def': NemNodeDirective,
    }
    indices = (NemNodeIndex,)
    initial_data = {
        'nem_nodes': [],
    }

    def get_full_qualified_name(self, item):
        return f"node.{item.uid}"

    def get_objects(self):
        for obj in self.data['nem_nodes']:
            yield(obj)

    def resolve_xref(self, env, fromdoc, builder, typ, target, node, contnode):
        for nemnode in self.get_objects():
            if nemnode.uid == target:
                if not node.attributes['refexplicit']:
                    contnode = nemnode.refnode_content()
                return make_refnode(
                    builder,
                    fromdoc,
                    nemnode.docname,
                    nemnode.uid,
                    contnode,
                    nemnode.name)

        print(f"Node, {target}, does not exist.")
        return None

    def add_node(self, nemnode):
        """
        Adds the node to the domain
        """
        nemnode.docname = self.env.docname
        self.data['nem_nodes'].append(nemnode)
