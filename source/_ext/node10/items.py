import re
from collections import defaultdict

from docutils.parsers.rst import directives
from docutils import nodes

from sphinx import addnodes
from sphinx.domains import Domain, Index
from sphinx.roles import XRefRole
from sphinx.directives import ObjectDescription
from sphinx.util.nodes import make_refnode

from .utils import create_row, NemronObject


class GameItem(NemronObject):
    def __init__(self, name, **opts):
        super().__init__(name, **opts)


class ItemDirective(ObjectDescription):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {
        'uid': directives.unchanged_required,
    }

    def handle_signature(self, sig, signode):
        signode += addnodes.desc_name(text=sig)
        return sig

    def add_target_and_index(self, name_cls, sig, signode):
        signode['ids'].append(f"items-{self.item.uid}")
        if 'noindex' not in self.options:
            items = self.env.get_domain('item')
            items.add_item(self.item)

    def run(self):
        self.item = GameItem(self.arguments[0], **self.options)
        super().run()

        section = nodes.section(
            ids=[self.item.uid],
            classes=['nemron-item'],
        )

        title = nodes.title(self.item.name, self.item.name)
        section.append(title)

        if self.content:
            description_node = nodes.paragraph()
            self.state.nested_parse(
                self.content,
                self.content_offset,
                description_node)
            section += description_node

        return [section]


class ItemIndex(Index):
    name = 'item'
    localname = 'Item Index'
    shortname = 'Items'

    def generate(self, docnames=None):
        content = defaultdict(list)
        items = sorted(self.domain.get_objects())

        # Now Generate the index output
        for item in items:
            content[item.name[0]].append((
                item.name,
                0,
                item.docname,
                item.uid,
                item.uid,
                '',
                'Item'
            ))

        content = sorted(content.items())
        return content, True


class ItemDomain(Domain):
    name = 'item'
    label = 'Item Reference'
    roles = {
        'ref': XRefRole(),
    }
    directives = {
        'def': ItemDirective,
    }
    indices = (ItemIndex,)
    initial_data = {
        'items': [],
        'item_list': [],
    }

    def get_full_qualified_name(self, item):
        return f"item.{item.uid}"

    def get_objects(self):
        for obj in self.data['item_list']:
            yield(obj)

    def resolve_xref(self, env, fromdoc, builder, typ, target, node, contnode):
        for item in self.get_objects():
            if item.uid == target:
                refexplicit = node.attributes.get('refexplicit', False)
                if not refexplicit:
                    contnode = item.refnode_content()
                return make_refnode(
                    builder,
                    fromdoc,
                    item.docname,
                    item.uid,
                    contnode,
                    item.name)

        print(f"Item, {target}, does not exist.")
        return None

    def add_item(self, item):
        item.docname = self.env.docname
        self.data['item_list'].append(item)
