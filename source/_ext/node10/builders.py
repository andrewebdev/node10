"""
  These custom builders are used to build JSON files for our system so that
  we can then import those into other VTT systems or any other software.
"""
from typing import Dict, Tuple, Iterator, Set, Any
from docutils.nodes import Node
from sphinx.locale import __
from sphinx.application import Sphinx
from sphinx.builders import Builder


test = True


class SystemBuilder(Builder):
    """
    Builds System files as JSON
    """
    name = "system"


class FoundryJournalBuilder(Builder):
    """
    This will generate plain HTML files that can easilly be copy/pasted into
    the foundry vtt journals without creating weird formatting cause by default
    html themes.
    """
    name = "foundryjournals"
    epilog = __('Foundry journal files are in %(outdir)')
    current_docname = None  # type: str
    allow_parallel = True


class CompendiumBuilder(Builder):
    """
    Builds FoundryVTT Compendium
    """
    name = "compendium"
    epilog = __('Compendium files are in %(outdir)')
    current_docname = None  # type: str
    allow_parallel = True

    def init(self) -> None:
        pass

    def get_outdated_docs(self) -> Set[str]:
        return self.env.found_docs

    def get_target_uri(self, docname: str, typ: str = None) -> str:
        return ''

    def prepare_writing(self, docnames: Set[str]) -> None:
        pass

    def write_doc(self, docname: str, doctree: Node) -> None:
        # Export Skills
        global test
        if test:
            for node in doctree:
                print(node)
            test = False

        # Export Items

        # Export Armor

        # Export Actors

        # print(80*"=")
        # print(docname)
        # print(80*"-")
        # print(doctree)

    def finish(self) -> None:
        pass


class AdventureBuilder(Builder):
    """
    Builds Adventure Files as JSON
    """
    name = "adventure"
    # format = ''
    epilog = __('The adventure files are in %(outdir)s.')
    # default_translator_class = TextTranslator
    current_docname = None  # type: str
    allow_parallel = True

    def init(self) -> None:
        pass

    def get_outdated_docs(self) -> Set[str]:
        return self.env.found_docs

    def get_target_uri(self, docname: str, typ: str = None) -> str:
        return ''

    def prepare_writing(self, docnames: Set[str]) -> None:
        pass

    def write_doc(self, docname: str, doctree: Node) -> None:
        print(80*"=")
        print(docname)
        print(80*"-")
        print(doctree)
        pass

    def finish(self) -> None:
        pass
