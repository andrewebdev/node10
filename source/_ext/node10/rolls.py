import re
from functools import reduce
from docutils import nodes
from docutils.parsers.rst import Directive, directives
from sphinx.domains import Domain


class RollParser(object):
    """
    A special class that will parse a roll definition and separate it into
    meaningful parts. Different methods can then be used to calculate certain
    parts, show or hide them etc.

    In the final formula parts will always be summed.
    """
    raw_parts = []
    mod_re = re.compile(r'(?P<name>[a-zA-Z]+)\((?P<value>[-\d]+)\)')

    def __init__(self, roll_def):
        self.roll_def = roll_def
        self.raw_parts = roll_def.split(" ")
        self.roll_parts = []

        # Parse the raw parts
        for part in self.raw_parts:
            if "d10" in part:
                self.roll_parts.append({
                    "type": "d10",
                    "name": "Dice",
                    "value": int(part.split("d")[0]),
                })
                continue

            mod_m = self.mod_re.match(part)
            if mod_m is not None:
                self.roll_parts.append({
                    "type": "mod",
                    "name": mod_m.group('name'),
                    "value": int(mod_m.group('value')),
                })
                continue

            self.roll_parts.append({
                "type": "other",
                "name": part,
                "value": part,
            })

    def get_mod_sum(self):
        mod_total = 0
        for part in self.roll_parts:
            if part["type"] not in ["d10", "other"]:
                mod_total += part["value"]

    def get_nodes(self):
        parts = []
        for index, part in enumerate(self.roll_parts):
            if part["type"] == "other":
                if index == 0:
                    text = "{} ".format(part["value"])
                else:
                    text = "+ {} ".format(part["value"])
            elif part["type"] == "d10":
                text = "{}d10 ".format(part["value"])
            else:
                operator_prefix = ""
                if part["value"] > 0 and index > 0:
                    operator_prefix = "+"
                text = "{} {} ({}) ".format(
                    operator_prefix,
                    part["value"],
                    part["name"],
                )
            parts.append(nodes.inline(text, text))
        return parts


def roll_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    """
    Defines a consistent manner to show dice rolls required etc.
    This directive will show the rolls inline.

    It also provides a syntax that will allow us to supply ranks and
    values for the different skills, attributes and modifiers.

    **Usage:**

    `Body(1) + Dex(2) + Athletics.Acrobatics(2,1) + Mod(1) + OtherMod(2)`

    To calculate the a part and show the total, wrap it in square brackets, eg:

    `Body(1) + Dex(2) + Athletics.Acrobatics(2,1) + [Mod(1) + OtherMod(2)]`

    """
    # use the dice parser to parse our text.
    roll = RollParser(text)
    roll.get_mod_sum()
    return roll.get_nodes(), []


# class RollDomain(Domain):
#     name = 'roll'
#     label = 'Dice Rolls'
#     roles = {}
#     directives = {
#         'def': InlineRollDirective
#     }
