"""
Classes directives and tools for the actors
"""
import re
from collections import defaultdict
from math import ceil, floor

from docutils import nodes
from docutils.parsers.rst import directives

from sphinx import addnodes
from sphinx.directives import ObjectDescription
from sphinx.domains import Domain, Index
from sphinx.roles import XRefRole
from sphinx.util.nodes import make_refnode

from .utils import (
    create_row,
    term_def,
    field_def,
    dice_by_rank,
    NemronObject)


class Actor(NemronObject):
    frame = None
    movespeed = None

    attr_lines = {
        # [ attr1, attr2, attr3, health_node ]
        'body': [0, 0, 0],
        'mind': [0, 0, 0],
        'spirit': [0, 0, 0],
    }

    line_formats = {
        'body': 'Pow {}, Agi {}, Con {}',
        'mind': 'Int {}, Awa {}, Wil {}',
        'spirit': 'Ctr {}, Sen {}, Foc {}',
    }

    def __init__(self, name, **opts):
        super().__init__(name, **opts)

        self.frame = opts.get('frame')
        self.attr_lines = {
            'body': [int(attr.strip()) for attr in opts.pop('body')],
            'mind': [int(attr.strip()) for attr in opts.pop('mind')],
            'spirit': [int(attr.strip()) for attr in opts.pop('spirit')],
        }
        self.update_stats()

    def update_stats(self):
        self.movespeed = sum(self.attr_lines['body'][0:2])

    def line_attrs_formatted(self, line):
        format_args = [i for i in self.attr_lines[line]]
        return self.line_formats[line].format(*format_args)


class ActorDirective(ObjectDescription):
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {
        'uid': directives.unchanged_required,
        'frame': directives.unchanged,
        'background': directives.unchanged,
        'body': lambda s: s.split(','),  # power, agility, constitution
        'mind': lambda s: s.split(','),  # intellect, awareness, willpower
        'spirit': lambda s: s.split(','),  # control, sense, focus
    }
    has_content = True

    def handle_signature(self, sig, signode):
        signode += addnodes.desc_name(text=sig)
        return sig

    def add_target_and_index(self, name_cls, sig, signode):
        signode['ids'].append(f"actor-{self.actor.uid}")
        if 'noindex' not in self.options:
            actors = self.env.get_domain('actor')
            actors.add_actor(self.actor)

    def run(self):
        self.actor = Actor(self.arguments[0], **self.options)
        super().run()

        title = nodes.title(self.actor.name, self.actor.name)

        table = nodes.table()
        tgroup = nodes.tgroup(cols=2)
        table += tgroup

        tgroup += nodes.colspec(colwidth=1)
        tgroup += nodes.colspec(colwidth=2)

        tbody = nodes.tbody()
        tgroup += tbody

        tbody += create_row(('Name', self.actor.name))

        if self.actor.frame:
            tbody += create_row(('Frame', self.actor.frame))

        tbody += create_row(('Movespeed', f"{self.actor.movespeed} m/ap"))

        for k in self.actor.attr_lines.keys():
            attr_key = f"{k.capitalize()}"
            tbody += create_row((attr_key, self.actor.line_attrs_formatted(k)))

        if self.content:
            description_node = nodes.paragraph()
            self.state.nested_parse(
                self.content,
                self.content_offset,
                description_node)
            return [table, description_node]

        return [table]


class ActorIndex(Index):
    name = 'actor'
    localname = 'Actor Index'
    shortname = 'Actors'

    def generate(self, docnames=None):
        content = defaultdict(list)
        items = sorted(self.domain.get_objects())

        # Generate the output
        for actor in items:
            # group by first letter of name
            content[actor.name[0]].append((
                actor.name,  # dispname,
                0,
                actor.docname,
                actor.uid,  # anchor,
                actor.uid,  # name,
                '',
                'Actor',
            ))
        content = sorted(content.items())
        return content, True


class ActorDomain(Domain):
    name = 'actor'
    label = 'Actor Reference'
    roles = {
        'ref': XRefRole(),
    }
    directives = {
        'def': ActorDirective,
    }
    indices = (ActorIndex,)
    initial_data = {
        'actors': [],
    }

    def get_full_qualified_name(self, item):
        return f"actor.{item.uid}"

    def get_objects(self):
        for actor in self.data['actors']:
            yield(actor)

    def resolve_xref(self, env, fromdoc, builder, typ, target, node, contnode):
        for actor in self.get_objects():
            if actor.uid == target:
                if not node.attributes['refexplicit']:
                    contnode = actor.refnode_content()
                return make_refnode(
                    builder,
                    fromdoc,
                    actor.docname,
                    actor.uid,
                    contnode,
                    actor.name)

        print(f"Actor, {target}, does not exist.")
        return None

    def add_actor(self, actor):
        actor.docname = self.env.docname
        self.data['actors'].append(actor)
